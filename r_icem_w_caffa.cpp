//============================================================================
// Name        : r_icem_w_caffa.cpp
// Author      : Stefan Kneissl & Ulrich Falk
// Version     : Version 1.1
// Description : This program is used to generate input for caffa3d.f code
//               based on .grd and .tbc file out of ICEMCFD!
//               Coordinates and boundary condition files are generated for
//               each block. These files are the input of w_preprocessor.F
//               which generates input for block preprocessor of caffa3d
//               according to specifications in manual!
//               Connectivity informations are also extracted from ICEMCFD
//               in order to perform local block refinement.
// Limitations : Periodic boundary conditions can be specified in ICEMCFD //               These boundary conditions are not supported in the current
//               state by caffa3d.f. Specified periodic boundary conditions
//               are treated as connectivities. Results in error in Block
//               pre-processor.
// Copyright   :
//============================================================================
#define MAXLINE 256
#define MAXBC 13 /* 12 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fstream>



using namespace std;

char file_grd[99];
char file_bcs_t[99];

struct gbs_block{     /* information for block of GBS                        */
  int dim[3];         /* dimensions                                          */
};
struct gbs_patch{     /* information for patch of GBS                        */
  int bc;             /* boundary condition                                  */
  int gbs_iblock;     /* number of geometrical block {0,1,...,gbs_nblock-1}  */
  int iface;          /* face {1,2,3,4,5,6}                                  */
  int index[2][2];    /* index[LID][EDG]                                     */
  int gbs_ipatch_nb;  /* number of connected patch, if connection            */
  int lid_nb[2];      /* LID of connected patch [LID]                        */
  int lid_or_nb[2];   /* orientation of connected patch [LID] {-1,+1}        */
  int per;            /* flag for periodic boundary condition {0,1}          */
  int iface_nb;       /* Face of connected Patch : write_cfview              */
  int orient;         /* Patch orientation (same/Reverse in igg ) of         */
                      /* connected patch : write_cfview                      */
  int corn;           /* Corner position (0/1/2/3 in igg) of connected patch */
                      /* write_cfview                                        */
};

static char *bctype[]={"NUL","INL","OUT","MIR","SOL","SOQ","RAS","RAM","MAR","HTC","PER","CON","MTM"};

void exit_on_error(int);
void endian_convert(unsigned char *, int, int);
void read_grid(double **,struct gbs_block **, int *,int *);
void create_output_file(struct gbs_block *,int,double *);
void read_tbc(struct gbs_patch **,struct gbs_block *, int *,int);
char *fgetsCR(char *,int ,FILE *);
int findstr(FILE *, char *);
int eval_bc(char *, int);
void get_con(char *, char *, struct gbs_patch *, int, int, int, int, int);
int get_face_bc(char *, struct gbs_block *);
int get_face_con(char, char);
int lid_of_gid(int, int);
void cutlf(char *, int* );

int main(int argc, char *argv[]) {
	double *p_grid_d;
	struct gbs_block *p_gbs_block_tab;
	struct gbs_patch *p_gbs_patch_tab;
	int nblock;

	int gbs_npatch;
	int grid_size=0;
	unsigned long *hex_elements;
	int *fixed;
	int nelem=0;
	int iblock;
	int i;
	int err;

	strcpy(file_grd, argv[1]);
	strcpy(file_bcs_t, argv[2]);

	read_grid(&p_grid_d,&p_gbs_block_tab,&nblock,&grid_size);

	for(iblock=0;iblock<nblock;iblock++){
		nelem+=((p_gbs_block_tab[iblock].dim)[0]-1) *	((p_gbs_block_tab[iblock].dim)[1]-1) *	((p_gbs_block_tab[iblock].dim)[2]-1);

	}

	if ((hex_elements = (unsigned long *) malloc(8*grid_size*sizeof(long))) == NULL){
	    printf("\n  ERROR: Not enough memory to allocate %d byte\n ",
	    		(8*grid_size*sizeof(unsigned long)));
	    exit_on_error(1);
	}
	if ((fixed = (int *) malloc(grid_size*sizeof(int))) == NULL){
	    printf("\n  ERROR: Not enough memory to allocate %d byte\n ",
	    		(grid_size*sizeof(int)));
	    exit_on_error(1);
	}

	create_output_file(p_gbs_block_tab,nblock,p_grid_d);

	read_tbc(&p_gbs_patch_tab, p_gbs_block_tab, &gbs_npatch, nblock);


	if (err)
	  {
	    std::cout << err << std::endl;
	    return 1;
	  }

	cout << "!!!All done!!!" << endl;
	return 0;
}



void read_grid(double **pp_grid_d,struct gbs_block **p_gbs_block_tab,int *nblock, int *p_grid_size){

	int offset = 0;  /* Offset, where to start reading */
	int byte_order_id;
	FILE *infile_grid; /* pointer for opening file_grd (MW) */
	int gbs_iblock;
	int isize;
	int jsize;
	int ksize;


	if ((infile_grid = fopen(file_grd, "rb")) == NULL){
		printf("\n  ERROR: Cannot open file \"%s\"\n", file_grd);
		exit_on_error(1);
	}

	offset = 8 * sizeof(char);
	fseek(infile_grid, offset, SEEK_CUR);
	fread(&byte_order_id, sizeof(int), 1, infile_grid);
	fread(nblock, sizeof(*nblock), 1, infile_grid);
	if (byte_order_id!=1) endian_convert((unsigned char *) nblock, sizeof(int), 1);
	if ((*p_gbs_block_tab = (struct gbs_block *) malloc((*nblock)*sizeof(struct gbs_block))) == NULL){
	    printf("\n  ERROR: Not enough memory to allocate %d byte\n for block_tab:%d Bloecke",
	           *nblock*sizeof(struct gbs_block),*nblock);
	    exit_on_error(1);
	}
	for (gbs_iblock = 0; gbs_iblock < *nblock; gbs_iblock++){
			fread(&isize, sizeof(int), 1, infile_grid);
			fread(&jsize, sizeof(int), 1, infile_grid);
			fread(&ksize, sizeof(int), 1, infile_grid);
			if(byte_order_id!=1){
				endian_convert((unsigned char *) &isize, sizeof(int), 1);
				endian_convert((unsigned char *) &jsize, sizeof(int), 1);
				endian_convert((unsigned char *) &ksize, sizeof(int), 1);
			}
			(*p_gbs_block_tab)[gbs_iblock].dim[0] = isize;
			(*p_gbs_block_tab)[gbs_iblock].dim[1] = jsize;
			(*p_gbs_block_tab)[gbs_iblock].dim[2] = ksize;
			*p_grid_size += isize * jsize * ksize;
		}
	if ((*pp_grid_d = (double *) malloc(3 * *p_grid_size * sizeof(double))) == NULL){
		printf("\n  ERROR: Not enough memory to allocate %d byte\n for grid",
				3 * *p_grid_size * sizeof(double));
		exit_on_error(1);
	}

	fread(*pp_grid_d, sizeof(double), 3 * *p_grid_size, infile_grid);
	if (byte_order_id!=1)  endian_convert((unsigned char *) *pp_grid_d, sizeof(double), 3 * *p_grid_size);

	fclose(infile_grid);

}
 /* falk begin */
void create_output_file(struct gbs_block *p_gbs_block_tab,int nblock,double *pp_grid_d){
	int iblock;
	int i,j,k;
	int imax,jmax,kmax;
	int ielem=0;
	int icount=0;
	int lastblocks=0;
	char outblock [15];
	char block [15];
	int n ;


	fstream datei;
	n=sprintf (outblock, "control.txt");
	datei.open(outblock, ios::out);
	datei<<nblock<< endl;
	for (iblock=0;iblock<nblock;iblock++){
	  n=sprintf (block, "block%d.txt", iblock);
	  datei<<block<< endl;
	}
	datei.close();

	for (iblock=0;iblock<nblock;iblock++){
	  fstream datei;
	  n=sprintf (outblock, "block%d.txt", iblock);
	  datei.open(outblock, ios::out);
	  imax=(p_gbs_block_tab[iblock].dim)[0];
	  jmax=(p_gbs_block_tab[iblock].dim)[1];
	  kmax=(p_gbs_block_tab[iblock].dim)[2];
	  ielem=(imax)*(jmax)*(kmax);
	  datei<<imax<<" " <<jmax<<" " <<kmax<<" " <<imax*jmax*kmax<< endl;
	  for(k=0;k<kmax;k++){
	    for(i=0;i<imax;i++){
	      for(j=0;j<jmax;j++){
		icount=(i+j*imax+k*imax*jmax+lastblocks)*3;
		datei << pp_grid_d[icount] <<" "<< pp_grid_d[icount+1] <<" "<< pp_grid_d[icount+2] <<  endl;
	     }
	    }
	  }
	  lastblocks+=imax*jmax*kmax;
	  datei.close();
	}

}
/* falk end */

void read_tbc(struct gbs_patch **pp_gbs_patch_tab,
	      struct gbs_block *p_gbs_block_tab, int *p_gbs_npatch,
	      int gbs_nblock){

/**********************************************************************/
/* This routine reads topology and boundary conditions from a tbc     */
/* (Topology and Boundary Conditions) file generated by ICEM.         */
/* Called for velocity and temperature. Information is stored in      */
/* an array of patch structures.                                      */
/**********************************************************************/
  int iblock;             /* Counter for blocks  */
  int iface;              /* Counter for faces   */
  int ipatch = 0;         /* Counter for patches */
  int iblock_nb;          /* Number of neighbouring block */
  int found;              /* Boolean variabel, true when a string is found */
  int imin;               /* First index in i-direction */
  int imax;               /* Last index in i-direction  */
  int jmin;
  int jmax;
  int kmin;
  int kmax;
  int bctypecaffa;
  char outblock [15];
  int n ;
  int fine;               /* Boolean variable to determin when to stop
			     reading from file                           */
  int ncon   = 0;         /* Number of connectivities found              */
  int nbc    = 0;         /* Number of boundary conditions found         */
  int nper   = 0;         /* Number of periodic boundary conditions      */
  int cons   = 1;         /* Boolean variable for consistency            */
  int pos    = -1;         /* Position in string where BC is described    */

  int con_type;           /* Counter; 0 for con. info, 1 for perio info. */
  char tmpstr[3];         /* Temporary string (neighbouring block number */
  char *cmpstr = "\tf";   /* Comparing string (tabulator + f), from ICEM */
  char line1[MAXLINE];    /* First line read from the input file         */
  char line2[MAXLINE];    /* Second line read from the input file        */
  char s_label[MAXLINE];  /* String label                                */
  char str[MAXLINE];      /* Temporary string, used to manipulate line1  */

  FILE *infile_tbc; /* FILE-pointer for opening file_bcs_t (MW) */

  *p_gbs_npatch = 0;


  pos = 1;


  if ((infile_tbc = fopen(file_bcs_t, "r")) == NULL){
    printf("\n  ERROR: Cannot open file \"%s\"\n", file_bcs_t);
    exit_on_error(1);
  }

  /*  Checking that all blocks are represented and consistency of node numbers */
  fgetsCR(line1, MAXLINE, infile_tbc);

  for (iblock = 0; iblock < gbs_nblock; iblock++){
    fgetsCR(line1, MAXLINE, infile_tbc);
    sprintf(str, "domain.%d", iblock + 1);
    line1[strlen(str)] = '\0';

    if (!(strcmp(line1, str) == 0)){
      printf("\n  ERROR: Leaking information for block %d\n", iblock + 1);
      cons = 0;
    }
    sscanf(&line1[12], "%d%d%d%d%d%d", &imin, &jmin, &kmin, &imax,
	   &jmax, &kmax);
    if (imin != 1 || imax != p_gbs_block_tab[iblock].dim[0] ||
	jmin != 1 || jmax != p_gbs_block_tab[iblock].dim[1] ||
	kmin != 1 || kmax != p_gbs_block_tab[iblock].dim[2]){
      printf("\n  ERROR: Wrong number of grid points for block %d:\n"
	     "         imax = %d, jmax = %d, kmax = %d\n",
	     iblock + 1, imax, jmax, kmax);
      cons = 0;
    }
  }
  if (!cons)
    exit_on_error(1);


  /* Get the number of patches for connecivities and periodic BC...*/
  for (iblock = 0; iblock < gbs_nblock; iblock++){
    for (con_type = 1; con_type <= 2; con_type++){
      if (con_type == 1)
	sprintf(s_label, "# Connectivity for domain.%d", iblock + 1);
      else if (con_type == 2)
	sprintf(s_label, "# Periodic info for domain.%d", iblock + 1);
      found = findstr(infile_tbc, s_label);
      if (found){
	fine = 0;
	while (!fine && !feof(infile_tbc)){
	  fgetsCR(line1, MAXLINE, infile_tbc);
	  if (line1[0] == '#' || line1[0] == '\n')
	    fine = 1;
          else{
	    fgetsCR(line2, MAXLINE, infile_tbc);
	    strncpy(tmpstr, &line2[12], 3);
	    iblock_nb = atoi(tmpstr) - 1;
	    if (iblock_nb >= iblock){
	      if (con_type == 1)
		ncon+=2;
	      else if (con_type == 2)
		nper+=2;
	    }
	  }
	}
      }
    }
  }


  /* ... and BCs */
  for (iblock = 0; iblock < gbs_nblock; iblock++){
    sprintf(s_label, "# Boundary conditions for domain.%d", iblock + 1);
    found = findstr(infile_tbc, s_label);

    if (found){
      fine = 0;
      fgetsCR(line1, MAXLINE, infile_tbc);
      while (!fine && !feof(infile_tbc)){
	if (line1[0] == '#' || line1[0] == '\n')
	  fine = 1;
	else if (line1[0] == '*'){
	  if (eval_bc(line1, pos) != -11) nbc++;
	  fgetsCR(line1, MAXLINE, infile_tbc);
	}
	else{
	  printf("\n  ERROR: Wrong boundary condition format in file\n  %s:\n\n", file_bcs_t);
	  printf("         %s\n", line1);
	  exit_on_error(1);
	}
      }
    }
    else{
      printf("\n  ERROR: No BC information for domain/block %d\n", iblock + 1);
      exit_on_error(1);
    }
  }

  printf("  Number of connectivities: %d", ncon);
  printf("\n  Number of periodic bc's : %d", nper);
  printf("\n  Number of boundary conn.: %d\n", nbc);

  *p_gbs_npatch = ncon + nper + nbc;



  /* Allocate memory for array of patches */
  if ((*pp_gbs_patch_tab = (struct gbs_patch *)
       malloc((*p_gbs_npatch) * sizeof(struct gbs_patch))) == NULL){
    printf("\n  ERROR: Not enough memory to allocate %d byte\n",
	   *p_gbs_npatch * sizeof(struct gbs_patch));
    exit_on_error(1);
  }


  /* Read connectivities (i=0) and periodic information (i=1) */
  for (iblock = 0; iblock < gbs_nblock; iblock++){

    /* falk begin */
    fstream datei;
    n=sprintf (outblock, "block%d.txt", iblock);
    datei.open(outblock, ios::out | ios::app);
    /* falk end */

    for (con_type = 1; con_type <= 2; con_type++){
      if (con_type == 1)
	sprintf(s_label, "# Connectivity for domain.%d", iblock + 1);
      else if (con_type == 2)
	sprintf(s_label, "# Periodic info for domain.%d", iblock + 1);
      found = findstr(infile_tbc, s_label);
      if (found){
	fine = 0;
	while (!fine && !feof(infile_tbc)){
	  fgetsCR(line1, MAXLINE, infile_tbc);
	  if (line1[0] == '#' || line1[0] == '\n')
	    fine = 1;
	  else{
	    fgetsCR(line2, MAXLINE, infile_tbc);
	    strncpy(tmpstr, &line2[12], 3);
	    iblock_nb = atoi(tmpstr) - 1;
            if (iblock_nb >= iblock){
	      get_con(line1, line2, &(*pp_gbs_patch_tab)[ipatch], ipatch,
		      ipatch + 1, iblock, iblock_nb, con_type);
	      ipatch++;
	      get_con(line2, line1, &(*pp_gbs_patch_tab)[ipatch], ipatch,
		      ipatch - 1, iblock_nb, iblock, con_type);
	      ipatch++;
	    }
	  }
	}
      }
    }
  }

  /* ... and BCs information */
  for (iblock = 0; iblock < gbs_nblock; iblock++){
    sprintf(s_label, "# Boundary conditions for domain.%d",iblock + 1);
    found = findstr(infile_tbc, s_label);
    /* falk begin */
    fstream datei;
    n=sprintf (outblock, "block%d.txt", iblock);
    datei.open(outblock, ios::out | ios::app);
    /* falk end */
    if (found){
      fine = 0;
      fgetsCR(line1, MAXLINE, infile_tbc);
      while (!fine && !feof(infile_tbc)){
	if (line1[0] == '#' || line1[0] == '\n')
	  fine = 1;
	else{
	  /* Only read boundary condition if it differs from */
	  /* "CON" (connectivity for internal wall) */
	  if (eval_bc(line1, pos) != -11){
	    strcpy(str, strstr(line1, cmpstr));
	    sscanf(&str[2], "%d %d %d %d %d %d", &imin, &jmin, &kmin,
		   &imax, &jmax, &kmax);
	    iface = get_face_bc(&str[2], &p_gbs_block_tab[iblock]);
	    if (iface == 1 || iface == 6){
	      imin = jmin;
	      imax = jmax;
	      jmin = kmin;
	      jmax = kmax;
	    }
	    if (iface == 2 || iface == 5){
	      jmin = imin;
	      jmax = imax;
	      imin = kmin;
	      imax = kmax;
	    }
	    ((*pp_gbs_patch_tab)[ipatch]).bc            = eval_bc(line1, pos);
	    ((*pp_gbs_patch_tab)[ipatch]).gbs_iblock    = iblock;
	    ((*pp_gbs_patch_tab)[ipatch]).iface         = iface;
	    ((*pp_gbs_patch_tab)[ipatch]).index[0][0]   = imin;
	    ((*pp_gbs_patch_tab)[ipatch]).index[0][1]   = imax;
	    ((*pp_gbs_patch_tab)[ipatch]).index[1][0]   = jmin;
	    ((*pp_gbs_patch_tab)[ipatch]).index[1][1]   = jmax;
	    ((*pp_gbs_patch_tab)[ipatch]).gbs_ipatch_nb =-1;
	    ((*pp_gbs_patch_tab)[ipatch]).lid_nb[0]     = 0;
	    ((*pp_gbs_patch_tab)[ipatch]).lid_nb[0]     = 0;
	    ((*pp_gbs_patch_tab)[ipatch]).lid_or_nb[0]  = 0;
	    ((*pp_gbs_patch_tab)[ipatch]).lid_or_nb[0]  = 0;
	    ((*pp_gbs_patch_tab)[ipatch]).iface_nb      = 0;
	    ((*pp_gbs_patch_tab)[ipatch]).orient        = 0;
	    ((*pp_gbs_patch_tab)[ipatch]).corn          = 0;
	    ((*pp_gbs_patch_tab)[ipatch]).per           = 0;
	    /* falk begin */
	    if(eval_bc(line1, pos) == -1){
	      bctypecaffa=1;
	    }
	    if(eval_bc(line1, pos) == -2){
	      bctypecaffa=2;
	    }
	    if(eval_bc(line1, pos) == -3){
	      bctypecaffa=3;
	    }
	    if(eval_bc(line1, pos) == -4){
	      bctypecaffa=4;
	    }
	    if(eval_bc(line1, pos) == -11){
	      bctypecaffa=10;
	    }
            /* gabel begin */
	    if(eval_bc(line1, pos) == -12){
	      bctypecaffa=11;
	    }
            /* gabel end*/
	    datei <<iface<<" " <<bctypecaffa <<" " <<imin<<" "<<imax<<" " <<jmin<<" "<<jmax << endl;
	    /* falk end */
	    ipatch++;
	  }
	  fgetsCR(line1, MAXLINE, infile_tbc);
	}
      }
    }
    datei.close();
  }
  fclose(infile_tbc);
}
/**********************************************************************/
char *fgetsCR(char *s,int n,FILE *fp){
/**********************************************************************/
/*  This routine reads a line with fgets and replaces all \r (\015,   */
/*  CR, Carriage Return, german Wagenruecklauf with a blankspace      */
/*  fgetsCR can be used as a replacement for fgets.                   */
/**********************************************************************/
  char *ptr;
  if (fgets(s, n, fp) == NULL){
    return NULL;
  } else {
    ptr=s;
    while(strncmp(ptr,"\0",1) ) {
      if ( strncmp(ptr,"\r",1) == 0 ) *ptr=' ';
      ptr++;
    }
  }
  return s;
}


/**********************************************************************/
int findstr(FILE *inputfile, char *str){
/**********************************************************************/
/* This function searches for a string in the .md file. If the string */
/* is found the value 1 is returned, else 0. To ensure that the       */
/* strings are equal in length, the string read is cut.               */
/**********************************************************************/
  int found=0;
  char line[MAXLINE];

  rewind(inputfile);

  while (!feof(inputfile) && !found){
    fgetsCR(line, MAXLINE, inputfile);
    line[strlen(str)] = '\0';
    if (strcmp(line, str) == 0)
      found = 1;
    else
      found = 0;
  }

  return found;
}


/**********************************************************************/
void get_con(char *line1, char *line2, struct gbs_patch *p_gbs_patch,
             int ipatch, int ipatch_nb, int iblock, int iblock_nb, int per){
/**********************************************************************/
/* Get all information for a connectivity. All information is based   */
/* on the input string read from the tbc-file. The information is     */
/* stored in an array of patch structures.                            */
/**********************************************************************/
  int index[3][2];         /* Start and end index for each direction  */
  int indexw[3][2];         /* Start and end index for each direction  */
  int gid_source[2];       /* Global index direction for source       */
  int gid_dest[2];         /* Global index direction for destination  */
  int gid_or_source[2];    /* Global index direction orientation src. */
  int gid_or_dest[2];      /* Global index direction orientation dest.*/
  int gid_edge_source[2];
  char str[MAXLINE];
  char *cmpstr = "\tf";    /* Comparing string (tab. + f), from ICEM */
  int iface;               /* Face # 1=w, 2=s, 3=b, 4=t, 5=n, 6=e    */
  int ifacenb;             /* Number of neighbouring face            */
  char outblock [15];
  int n ;

  strcpy(str, strstr(line1, cmpstr));
  sscanf(&str[2], "%d%d%d%d%d%d", &index[0][0], &index[1][0], &index[2][0],
                                  &index[0][1], &index[1][1], &index[2][1]);
  gid_source[0]           = line1[17] - 'i';
  gid_source[1]           = line1[19] - 'i';
  gid_dest[0]             = line2[17] - 'i';
  gid_dest[1]             = line2[19] - 'i';
  gid_or_source[0]        = line1[16] == '-' ? -1 : 1;
  gid_or_source[1]        = line1[18] == '-' ? -1 : 1;
  gid_edge_source[0]      = line1[16] != '-';
  gid_edge_source[1]      = line1[18] != '-';
  gid_or_dest[0]          = line2[16] == '-' ? -1 : 1;
  gid_or_dest[1]          = line2[18] == '-' ? -1 : 1;
  iface                   = get_face_con(line1[20], line1[21]);
  ifacenb                 = get_face_con(line2[20], line2[21]);
  p_gbs_patch->bc         = iblock_nb;
  p_gbs_patch->gbs_iblock = iblock;
  p_gbs_patch->iface      = iface;

  //printf("position 1: %d   \n",line1[17] );
  //printf("position 2: %d   \n",line1[19] );
  //printf("position 3: %d   \n",line1[21] );
  /* falk begin */
  indexw[0][0]=0;
  indexw[0][1]=0;
  indexw[1][0]=0;
  indexw[1][1]=0;
  indexw[2][0]=0;
  indexw[2][1]=0;
  if(line1[17]==105){
    if(index[0][0]==index[0][1]){
      indexw[0][0]=index[0][0];
      indexw[0][1]=index[0][1];
    }
    if(index[0][0]<index[0][1]){
      indexw[0][0]=index[0][0];
      indexw[0][1]=index[0][1];
    }
    if(index[0][0]>index[0][1]){
      indexw[0][0]=index[0][1];
      indexw[0][1]=index[0][0];
    }
  }
  if(line1[19]==105){
    if(index[1][0]==index[1][1]){
      indexw[0][0]=index[1][0];
      indexw[0][1]=index[1][1];
    }
    if(index[1][0]<index[1][1]){
      indexw[0][0]=index[1][0];
      indexw[0][1]=index[1][1];
    }
    if(index[1][0]>index[1][1]){
      indexw[0][0]=index[1][1];
      indexw[0][1]=index[1][0];
    }
  }
  if(line1[21]==105){
    if(index[2][0]==index[2][1]){
      indexw[0][0]=index[2][0];
      indexw[0][1]=index[2][1];
    }
    if(index[2][0]<index[2][1]){
      indexw[0][0]=index[2][0];
      indexw[0][1]=index[2][1];
    }
    if(index[2][0]>index[2][1]){
      indexw[0][0]=index[2][1];
      indexw[0][1]=index[2][0];
    }

  }
  if(line1[17]==106){
    if(index[0][0]==index[0][1]){
      indexw[1][0]=index[0][0];
      indexw[1][1]=index[0][1];
    }
    if(index[0][0]<index[0][1]){
      indexw[1][0]=index[0][0];
      indexw[1][1]=index[0][1];
    }
    if(index[0][0]>index[0][1]){
      indexw[1][0]=index[0][1];
      indexw[1][1]=index[0][0];
    }
  }
  if(line1[19]==106){
    if(index[1][0]==index[1][1]){
      indexw[1][0]=index[1][0];
      indexw[1][1]=index[1][1];
    }
    if(index[1][0]<index[1][1]){
      indexw[1][0]=index[1][0];
      indexw[1][1]=index[1][1];
    }
    if(index[1][0]>index[1][1]){
      indexw[1][0]=index[1][1];
      indexw[1][1]=index[1][0];
    }
  }
  if(line1[21]==106){
    if(index[2][0]==index[2][1]){
      indexw[1][0]=index[2][0];
      indexw[1][1]=index[2][1];
     }
    if(index[2][0]<index[2][1]){
      indexw[1][0]=index[2][0];
      indexw[1][1]=index[2][1];
     }
    if(index[2][0]>index[2][1]){
      indexw[1][0]=index[2][1];
      indexw[1][1]=index[2][0];
    }
  }
  if(line1[17]==107){
    if(index[0][0]==index[0][1]){
      indexw[2][0]=index[0][0];
      indexw[2][1]=index[0][1];
    }
    if(index[0][0]<index[0][1]){
      indexw[2][0]=index[0][0];
      indexw[2][1]=index[0][1];
    }
    if(index[0][0]>index[0][1]){
      indexw[2][0]=index[0][1];
      indexw[2][1]=index[0][0];
    }
  }
  if(line1[19]==107){
    if(index[1][0]==index[1][1]){
      indexw[2][0]=index[1][0];
      indexw[2][1]=index[1][1];
    }
    if(index[1][0]<index[1][1]){
      indexw[2][0]=index[1][0];
      indexw[2][1]=index[1][1];
    }
    if(index[1][0]>index[1][1]){
      indexw[2][0]=index[1][1];
      indexw[2][1]=index[1][0];
    }
  }
  if(line1[21]==107){
    if(index[2][0]==index[2][1]){
      indexw[2][0]=index[2][0];
      indexw[2][1]=index[2][1];
    }
    if(index[2][0]<index[2][1]){
      indexw[2][0]=index[2][0];
      indexw[2][1]=index[2][1];
    }
    if(index[2][0]>index[2][1]){
      indexw[2][0]=index[2][1];
      indexw[2][1]=index[2][0];
    }
  }


  fstream datei;
  n=sprintf (outblock, "block%d.txt", iblock);
  datei.open(outblock, ios::out | ios::app);
  datei <<iface<<" " <<10<< endl;
  datei <<iblock+1<<" "<<iface<<" "<<indexw[0][0]<<" "<<indexw[1][0]<<" "<<indexw[2][0]<<" "<<indexw[0][1]<<" "<<indexw[1][1]<<" "<<indexw[2][1]<< endl;
  /*
  printf("------------------------------------------------  \n");
  printf("iblock: %d iblock_nb:%d iface:%d ifacenb:%d  \n", iblock+1,iblock_nb+1,iface,ifacenb);
  printf("ss1:%d ss2:%d ss3:%d se1:%d se2:%d se3:%d  \n",index[0][0], index[1][0],index[2][0], index[0][1], index[1][1],index[2][1]);
  */
  /* falk end */

  /* set lower index of first tbc-dir. in il or jl, dependant on face and gid  */
  p_gbs_patch->index[lid_of_gid(iface,gid_source[0])][0] =
    index[0][1-gid_edge_source[0]];
  /* set lower index of second tbc-dir. in il or jl, dependant on face and gid */
  p_gbs_patch->index[lid_of_gid(iface,gid_source[1])][0] =
    index[1][1-gid_edge_source[1]];
  /* set upper index of first tbc-dir. in il or jl, dependant on face and gid  */
  p_gbs_patch->index[lid_of_gid(iface,gid_source[0])][1] =
    index[0][gid_edge_source[0]];
  /* set upper index of second tbc-dir. in il or jl, dependant on face and gid */
  p_gbs_patch->index[lid_of_gid(iface,gid_source[1])][1] =
    index[1][gid_edge_source[1]];
  /* patch number of neighbour */
  p_gbs_patch->gbs_ipatch_nb = ipatch_nb;
  /* set lid_nb of first tbc-dir. in il or jl, dependant on face and gid */
  p_gbs_patch->lid_nb[lid_of_gid(iface,gid_source[0])] =
    lid_of_gid(ifacenb,gid_dest[0]);
  /* set lid_nb of second tbc-dir. in il or jl, dependant on face and gid */
  p_gbs_patch->lid_nb[lid_of_gid(iface,gid_source[1])] =
    lid_of_gid(ifacenb,gid_dest[1]);
  /* set orientation of first tbc-dir. in il or jl, dependant on face and gid */
  p_gbs_patch->lid_or_nb[lid_of_gid(iface,gid_source[0])] =
    gid_or_dest[0] * gid_or_source[0];
  /* set orientation of second tbc-dir. in il or jl, dependant on face and gid */
  p_gbs_patch->lid_or_nb[lid_of_gid(iface,gid_source[1])] =
    gid_or_dest[1] * gid_or_source[1];
  p_gbs_patch->iface_nb = ifacenb;
  p_gbs_patch->orient   = 0;
  p_gbs_patch->corn     = 0;
  p_gbs_patch->per      = per;

  /* falk start */
  strcpy(str, strstr(line2, cmpstr));
  sscanf(&str[2], "%d%d%d%d%d%d", &index[0][0], &index[1][0], &index[2][0],
                                  &index[0][1], &index[1][1], &index[2][1]);

  indexw[0][0]=0;
  indexw[0][1]=0;
  indexw[1][0]=0;
  indexw[1][1]=0;
  indexw[2][0]=0;
  indexw[2][1]=0;
  if(line2[17]==105){
    if(index[0][0]==index[0][1]){
      indexw[0][0]=index[0][0];
      indexw[0][1]=index[0][1];
    }
    if(index[0][0]<index[0][1]){
      indexw[0][0]=index[0][0];
      indexw[0][1]=index[0][1];
    }
    if(index[0][0]>index[0][1]){
      indexw[0][0]=index[0][1];
      indexw[0][1]=index[0][0];
    }
  }
  if(line2[19]==105){
    if(index[1][0]==index[1][1]){
      indexw[0][0]=index[1][0];
      indexw[0][1]=index[1][1];
    }
    if(index[1][0]<index[1][1]){
      indexw[0][0]=index[1][0];
      indexw[0][1]=index[1][1];
    }
    if(index[1][0]>index[1][1]){
      indexw[0][0]=index[1][1];
      indexw[0][1]=index[1][0];
    }
  }
  if(line2[21]==105){
    if(index[2][0]==index[2][1]){
      indexw[0][0]=index[2][0];
      indexw[0][1]=index[2][1];
    }
    if(index[2][0]<index[2][1]){
      indexw[0][0]=index[2][0];
      indexw[0][1]=index[2][1];
    }
    if(index[2][0]>index[2][1]){
      indexw[0][0]=index[2][1];
      indexw[0][1]=index[2][0];
    }

  }
  if(line2[17]==106){
    if(index[0][0]==index[0][1]){
      indexw[1][0]=index[0][0];
      indexw[1][1]=index[0][1];
    }
    if(index[0][0]<index[0][1]){
      indexw[1][0]=index[0][0];
      indexw[1][1]=index[0][1];
    }
    if(index[0][0]>index[0][1]){
      indexw[1][0]=index[0][1];
      indexw[1][1]=index[0][0];
    }
  }
  if(line2[19]==106){
    if(index[1][0]==index[1][1]){
      indexw[1][0]=index[1][0];
      indexw[1][1]=index[1][1];
    }
    if(index[1][0]<index[1][1]){
      indexw[1][0]=index[1][0];
      indexw[1][1]=index[1][1];
    }
    if(index[1][0]>index[1][1]){
      indexw[1][0]=index[1][1];
      indexw[1][1]=index[1][0];
    }
  }
  if(line2[21]==106){
    if(index[2][0]==index[2][1]){
      indexw[1][0]=index[2][0];
      indexw[1][1]=index[2][1];
     }
    if(index[2][0]<index[2][1]){
      indexw[1][0]=index[2][0];
      indexw[1][1]=index[2][1];
     }
    if(index[2][0]>index[2][1]){
      indexw[1][0]=index[2][1];
      indexw[1][1]=index[2][0];
    }
  }
  if(line2[17]==107){
    if(index[0][0]==index[0][1]){
      indexw[2][0]=index[0][0];
      indexw[2][1]=index[0][1];
    }
    if(index[0][0]<index[0][1]){
      indexw[2][0]=index[0][0];
      indexw[2][1]=index[0][1];
    }
    if(index[0][0]>index[0][1]){
      indexw[2][0]=index[0][1];
      indexw[2][1]=index[0][0];
    }
  }
  if(line2[19]==107){
    if(index[1][0]==index[1][1]){
      indexw[2][0]=index[1][0];
      indexw[2][1]=index[1][1];
    }
    if(index[1][0]<index[1][1]){
      indexw[2][0]=index[1][0];
      indexw[2][1]=index[1][1];
    }
    if(index[1][0]>index[1][1]){
      indexw[2][0]=index[1][1];
      indexw[2][1]=index[1][0];
    }
  }
  if(line2[21]==107){
    if(index[2][0]==index[2][1]){
      indexw[2][0]=index[2][0];
      indexw[2][1]=index[2][1];
    }
    if(index[2][0]<index[2][1]){
      indexw[2][0]=index[2][0];
      indexw[2][1]=index[2][1];
    }
    if(index[2][0]>index[2][1]){
      indexw[2][0]=index[2][1];
      indexw[2][1]=index[2][0];
    }
  }
  datei <<iblock_nb+1<<" "<<ifacenb<<" "<<indexw[0][0]<<" "<<indexw[1][0]<<" "<<indexw[2][0]<<" "<<indexw[0][1]<<" "<<indexw[1][1]<<" "<<indexw[2][1]<< endl;
  datei.close();
  /* falk ende */

  // printf("ds1:%d ds2:%d ds3:%d de1:%d de2:%d de3:%d  \n",index[0][0],index[1][0],index[2][0],index[0][1],index[1][1],index[2][1]);

}

/**********************************************************************/
int eval_bc(char *line, int pos){
/**********************************************************************/
/* Return the number of the boundary condition for velocity OR        */
/* temperature based on the character string line, typical value:     */
/* SOL = -4                                                           */
/**********************************************************************/
  char temp[7];     /* contains the BC of VEL and TEMP -> 6 chars + End of String */
  char bc [4];     /* contains the requested BC (VEL or TEMP) -> 3 chars + End of String */
  int ibc = 0;    /* BC, number                 */

  int len;        /* Length of a string         */
  int i;
  int found = 0;

  /*
  strncpy(bc, &line[1], 6);
  cutlf(bc, &len);
  if (strlen(bc) == 3)
    strncat(bc, bc, 3);

  strncpy(bc,  &line[pos], 3); //???????????????????????? here was ERROR !! appends at the end -> overflow!
  bc[3] = '\0';
  */

  strncpy(temp, &line[1],6);
  temp[6] = '\0';
  cutlf(temp, &len);
  if (strlen(temp) == 3){
    strncat(temp,temp,3);
    temp[6]='\0';
  }

  strncpy(bc, &temp[pos-1], 3);
  bc[3] = '\0';

  i = 1;
  // changed invalid pointer
  while (i < MAXBC && !found){
    if (!strcmp(bc, bctype[i])){
      ibc   = -i;
      found =  1;
    }
    i++;
  }

  if (!found){
    printf("\n  ERROR: Unknown boundary condition: \"%s\"\n", bc);
    exit_on_error(1);
  }

  if (ibc == 0){
    printf("\n  ERROR: No result generated in eval_bc!");
    exit_on_error(1);
  }

  return ibc;
}

/**********************************************************************/
int lid_of_gid(int iface, int gid){
/**********************************************************************/
/* Function for finding the local index direction for a given face    */
/* and global index direction.                                        */
/**********************************************************************/

  return ((3 - ((iface > 3) ? 7 - iface : iface) + gid) % 3);

}

/**********************************************************************/
int get_face_con(char sign, char index){
/**********************************************************************/
/* Return the number of the face based on the two characters read     */
/* from the tbc file, '-i' = 1, '-j' = 2, '-k' = 3, ' k' = 4,         */
/* ' j' = 5 and ' i' = 6.                                             */
/**********************************************************************/

  return (sign == '-' ? 7 - (6 - (index - 'i')) : 6 - (index - 'i'));
}


/**********************************************************************/
void cutlf(char *line, int *p_len){
/**********************************************************************/
/* This function takes a string variable and truncates it where it    */
/* finds the first space, tab-char. etc. The length of the string     */
/* is also returned.                                                  */
/* Note: A blank space is any one character from the following list:  */
/*       space, formfeed, newline, carriage return, tab, vertical tab */
/**********************************************************************/
  int i;

  for (i = 0; i <= strlen(line); i++)
    if (isspace(line[i])) line[i] = '\0';

  *p_len = strlen(line);
}

/**********************************************************************/
int get_face_bc(char *line, struct gbs_block *p_gbs_block ){
/**********************************************************************/
/* Return the face number for a boundary condition, depending on      */
/* which of the indices are constant, and if these are max or min     */
/* index.                                                             */
/**********************************************************************/
  int imin, imax, jmin, jmax, kmin, kmax;
  int iface;

  sscanf(line, "%d%d%d%d%d%d", &imin, &jmin, &kmin, &imax, &jmax, &kmax);
  if (imin == imax && imin == 1)
    iface = 1;
  else if (imin == imax && imax == p_gbs_block->dim[0])
    iface = 6;
  else if (jmin == jmax && jmin == 1)
    iface = 2;
  else if (jmin == jmax && jmin == p_gbs_block->dim[1])
    iface = 5;
  else if (kmin == kmax && kmin == 1)
    iface = 3;
  else if (kmin == kmax && kmin == p_gbs_block->dim[2])
    iface = 4;
  else
    iface = 0;

  return iface;
}

/**********************************************************************/
int gid_of_lid(int iface, int lid){
/**********************************************************************/
/* Function for finding the local index direction for a given face    */
/* and global index direction.                                        */
/**********************************************************************/

  return ((((iface > 3) ? 7 - iface : iface) + lid) % 3);
}
/**********************************************************************/
unsigned long  get_index(int block,int i,int j, int k,struct gbs_block *p_gbs_block_tab,int iselem){
/**********************************************************************/
    int iblock;
    int offset=0;

    int imax;
    int jmax;
    int kmax;

    //calc blockoffset in grid array
    for(iblock=0;iblock<block;iblock++){
        offset+=((p_gbs_block_tab[iblock].dim)[0]-iselem)*((p_gbs_block_tab[iblock].dim)[1]-iselem)*((p_gbs_block_tab[iblock].dim)[2]-iselem);

    }

    imax=(p_gbs_block_tab[block].dim)[0]-iselem;
    jmax=(p_gbs_block_tab[block].dim)[1]-iselem;
    kmax=(p_gbs_block_tab[block].dim)[2]-iselem;

    return (i-1) + (j-1)*imax + (k-1)*imax*jmax+offset;
}


void endian_convert(unsigned char *buffer, int varlen, int varnum){
/**********************************************************************/
/* This function endian converts a buffer that contains 'varnum'      */
/* variables of length 'varlen'.                                      */
/**********************************************************************/
  int ptrbegin, ptrend, mean, i, j;
  unsigned char store;

  /* Convert buffer contents */
  mean = varlen / 2;
  for (i = 0; i < varnum; i++)
    {
      ptrbegin = i * varlen;
      ptrend = (i + 1) * varlen - 1;
      for (j = 0; j < mean; j++)
        {
          store = *(buffer + ptrbegin + j);
          *(buffer + ptrbegin + j) = *(buffer + ptrend - j);
          *(buffer + ptrend - j) = store;
        }
    }
}


void exit_on_error(int exitvalue){
/**********************************************************************/
/* Exit on Error                                                      */
/**********************************************************************/

    printf("\n!!! ABNORMAL termination due to ERRORS  !!!\n");
    printf("!!! Please check the output attentively !!!\n\n");
    exit(exitvalue);
}


