C############################################################
C NEW
C     SUBROUTINE SOLVER(FI,IFI)
C END NEW
      SUBROUTINE SOLVER(FIARR,FIII,IFI)
C############################################################
C     This routine incorporates the Stone's SIP solver, based
C     on ILU-decomposition. See Sect. 5.3.4 for details.
C
C     A 3D version of SIPSOL has been incorporated here
C     obtained from 3D cartesian code in /3dc/prog.f
C     
C
C     This version has been adapted for use with block-
C     structured grids.
C============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "coef3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
      
      integer M,K,I,J,IJK,IMJK,IJKM,N,IFI,IJMK
      real*8 P1,P2,P3,RES1,RSM
      real*8 BB(NXYZA),BW(NXYZA),BS(NXYZA),BP(NXYZA),
     *     BN(NXYZA),
     *     BE(NXYZA),BT(NXYZA),RES(NXYZA),FIARR( 1)
C NEW
      PetscOffset FIII
#define FI(IJK) FIARR(FIII+IJK)
#define AP(IJK) APARR(APPI+IJK)
#define SU(IJK) SUARR(SUUI+IJK)
C==============================================================
C END NEW
C  
C
C.....INITIALIZE LU COEFICIENTS AND RESIDUALS
C
      BE=0.
      BN=0.
      BT=0.
      RES=0.
C
C.....CALCULATE COEFFICIENTS OF  L  AND  U  MATRICES
C
      DO M=1,NBLKS
      NKM=NKBK(M)-1
      NI=NIBK(M)
      NJ=NJBK(M)
      KST=KBK(M)
      IST=IBK(M)
      NIM=NI-1
      NJM=NJ-1
      NIJ=NI*NJ
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IMJK=IJK-NJ
        IJKM=IJK-NIJ
        IJMK=IJK-1
        BB(IJK)=+AB(IJK)/(1.+ALFA*(BN(IJKM)+BE(IJKM)))
        BW(IJK)=+AW(IJK)/(1.+ALFA*(BN(IMJK)+BT(IMJK)))
        BS(IJK)=+AS(IJK)/(1.+ALFA*(BE(IJMK)+BT(IJMK)))
        P1=ALFA*(BB(IJK)*BN(IJKM)+BW(IJK)*BN(IMJK))
        P2=ALFA*(BB(IJK)*BE(IJKM)+BS(IJK)*BE(IJMK))
        P3=ALFA*(BW(IJK)*BT(IMJK)+BS(IJK)*BT(IJMK))
        BP(IJK)=1./(AP(IJK)+P1+P2+P3-BB(IJK)*BT(IJKM)-BW(IJK)*BE(IMJK)
     *         -BS(IJK)*BN(IJMK)+SMALL)
        BN(IJK)=(+AN(IJK)-P1)*BP(IJK)
        BE(IJK)=(+AE(IJK)-P2)*BP(IJK)
        BT(IJK)=(+AT(IJK)-P3)*BP(IJK)
      END DO
      END DO
      END DO
C
      END DO
C
C.....INNER ITERATIONS LOOP
C
      DO N=1,NSW(IFI)
        RES1=0.0
C
C.....COMPUTE RESIDUAL VECTOR, SUM OF RESIDUALS AND AUXILIARY VECTOR
C
        DO M=1,NBLKS
        NKM=NKBK(M)-1
        NI=NIBK(M)
        NJ=NJBK(M)
        KST=KBK(M)
        IST=IBK(M)
        NIM=NI-1
        NJM=NJ-1
        NIJ=NI*NJ
C
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          RES(IJK)=SU(IJK)-AP(IJK)*FI(IJK)
     *            -AE(IJK)*FI(IJK+NJ) -AW(IJK)*FI(IJK-NJ)
     *            -AN(IJK)*FI(IJK+1)  -AS(IJK)*FI(IJK-1)
     *            -AT(IJK)*FI(IJK+NIJ)-AB(IJK)*FI(IJK-NIJ)
        END DO
        END DO
        END DO
C
        END DO
C
C.....CONTRIBUTION FROM OC-CUTS TO RESIDUALS
C
        DO I=1,NOCBKAL
          RES(IJLPBK(I))=RES(IJLPBK(I))-AR(I)*FI(IJRPBK(I))
          RES(IJRPBK(I))=RES(IJRPBK(I))-AL(I)*FI(IJLPBK(I))
        END DO
C NEW
C
C.....CONTRIBUTION FROM FACE SEGMENTS TO RESIDUALS
C
        DO I=1,NFSGBKAL
          RES(IJFL(I))=RES(IJFL(I))-AFR(I)*FI(IJFR(I))
          RES(IJFR(I))=RES(IJFR(I))-AFL(I)*FI(IJFL(I))
        END DO
C END NEW
C
C.....IF TESTING COPY OUT RESIDUALS
C
        IF(LTEST.AND.IFI.EQ.1) THEN
          DO IJK=1,NIJKBKAL
            RESMON(IJK)=RES(IJK)
          END DO
        ENDIF
C
C.....START LOOP THROUGH BLOCKS
C
        DO M=1,NBLKS
        NKM=NKBK(M)-1
        NI=NIBK(M)
        NJ=NJBK(M)
        KST=KBK(M)
        IST=IBK(M)
        NIM=NI-1
        NJM=NJ-1
        NIJ=NI*NJ
C
C.....COMPUTE SUM OF RESIDUALS AND AUXILIARY VECTOR
C
        DO K=2,NKM
        DO I=2,NIM
        DO J=2,NJM
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          RES1=RES1+ABS(RES(IJK))
          RES(IJK)=(RES(IJK)-BB(IJK)*RES(IJK-NIJ)
     *                      -BW(IJK)*RES(IJK-NJ)
     *                      -BS(IJK)*RES(IJK-1))*BP(IJK)
        END DO
        END DO
        END DO
C
C.....COMPUTE INCREMENT AND UPDATE VARIABLES (BACKWARD SUBSTITUTION)
C
        DO K=NKM,2,-1
        DO I=NIM,2,-1
        DO J=NJM,2,-1
          IJK=LKBK(K+KST)+LIBK(I+IST)+J
          RES(IJK)=RES(IJK)-BN(IJK)*RES(IJK+1)
     *                     -BE(IJK)*RES(IJK+NJ)
     *                     -BT(IJK)*RES(IJK+NIJ)
          FI(IJK)=FI(IJK)+RES(IJK)
        END DO
        END DO
        END DO
C
        END DO
C
C.....CHECK CONVERGENCE OF INNER ITERATIONS
C
        IF(N.EQ.1) RESOR(IFI)=RES1
        RSM=RES1/(RESOR(IFI)+SMALL)
C
        IF(LTEST) WRITE(6,600) IFI,N,RES1
  600   FORMAT(20X,'FI=',I2,'  SWEEP=',I3,'  RES=',1PE10.3)
        IF(RSM.LT.SOR(IFI)) RETURN
      END DO
C
      RETURN
C NEW
#undef FI
#undef AP
#undef SU
C END NEW
      END
C
C
