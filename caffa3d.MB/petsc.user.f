C#############################################################
      SUBROUTINE DISTRIBUTELOAD
C#############################################################
C     This routine initializes the PETSc Matrix and Vector
C     Objects by allocating space and so distributing the load
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "gradold3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER DNNZ(NXYZA),IJRGL(NOCA+NFA)
      INTEGER ONNZT(NXYZA)
      REAL*8 ONNZARR(1)
      INTEGER I,J,K,M,IJK,IJP,IJN,IJKP
C
      Vec ONNZVEC,ONNZL,ONNZGHOST
      PetscOffset ONNZZI
      PetscInt NGH,RANK
      PetscScalar PZERO
      PetscErrorCode IERR
C
      COMMON /MAPPING/ IJRGL
#include "petsc.user.inc"
C=============================================================
C
C......Initialize variables and arrays
C
      PZERO=0.0D0
      IJKP=0
      DNNZ=1
      NGH=0
      IJRGL=0
C
C.....FIND NUMBER OF VALUES THAT NEED TO BE GHOSTED AND CREATE VECTOR
C
      DO I=1,NOCBKAL
        IJN=IJRPBK(I)
        IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
          NGH=NGH+1
          IJGH(NGH)=IJN-1
        END IF
      END DO
C
      DO I=1,NFSGBKAL
        IJN=IJFR(I)
        IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
          NGH=NGH+1
          IJGH(NGH)=IJN-1
        END IF
      END DO
C
      CALL VecCreateGhost(PETSC_COMM_WORLD,
     *     NIJKBKAL,PETSC_DECIDE,NGH,IJGH,ONNZVEC,IERR)
      CALL VecGhostGetLocalForm(ONNZVEC,ONNZL,IERR)
      CALL VecSet(ONNZL,PZERO,IERR)
      CALL VecGetArray(ONNZL,ONNZARR,ONNZZI,IERR)
      NGH=0
C
      DO M=1,NBLKS
      CALL SETIND(M)
C
C.....Loop through inner faces. East faces
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        DNNZ(IJK)=DNNZ(IJK)+1
        DNNZ(IJK+NJ)=DNNZ(IJK+NJ)+1
      END DO
      END DO
      END DO
C
C.....Loop through inner faces. North faces
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        DNNZ(IJK)=DNNZ(IJK)+1
        DNNZ(IJK+1)=DNNZ(IJK+1)+1
      END DO
      END DO
      END DO
C
C.....Loop through inner faces. Top
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        DNNZ(IJK)=DNNZ(IJK)+1
        DNNZ(IJK+NIJ)=DNNZ(IJK+NIJ)+1
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. West/East bound.
C     
      DO K=1,NK
      DO I=1,NI,NI-1
      DO J=1,NJ
        IJKP=IJKP+1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
        ZEROS(IJKP)=IJK
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. South/North bound.
C     
      DO K=1,NK
      DO I=1,NI
      DO J=1,NJ,NJ-1
        IJKP=IJKP+1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
        ZEROS(IJKP)=IJK
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. Bottom/Top bound.
C     
      DO K=1,NK,NK-1
      DO I=1,NI
      DO J=1,NJ
        IJKP=IJKP+1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
        ZEROS(IJKP)=IJK
      END DO
      END DO
      END DO
      END DO
      NZERO=IJKP
C
C.....Block Boundaries (OC and FSG)
C
      DO I=1,NOCBKAL
C.....SUBTRACT THE PROCESSOR OFFSET
        IJLPBK(I)=IJLPBK(I)-IJKPRC
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
C.....ELEMENT NOT ON PROCESSOR 
        IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
          NGH=NGH+1
          ONNZ(IJP)=ONNZ(IJP)+1.0D0
          ONNZ(NIJKBKAL+NGH)=ONNZ(NIJKBKAL+NGH)+1.0D0
          IJRPBK(I)=NIJKBKAL+NGH
C.....ELEMENT ON PROCESSOR
        ELSE
          DNNZ(IJP)=DNNZ(IJP)+1
C.....SUBTRACT THE PROCESSOR OFFSET
          IJRPBK(I)=IJRPBK(I)-IJKPRC
          DNNZ(IJRPBK(I))=DNNZ(IJRPBK(I))+1
        END IF
C.....SAVE THE GLOBAL INDEX (FOR ASSEMBLY ONLY)
        IJRGL(I)=IJN
      END DO
C
      DO I=1,NFSGBKAL
        IJFL(I)=IJFL(I)-IJKPRC
        IJP=IJFL(I)
        IJN=IJFR(I)
        IF (IJN.GE.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
          NGH=NGH+1
          ONNZ(IJP)=ONNZ(IJP)+1.0D0
          ONNZ(NIJKBKAL+NGH)=ONNZ(NIJKBKAL+NGH)+1.0D0
          IJFR(I)=NIJKBKAL+NGH
        ELSE
          DNNZ(IJP)=DNNZ(IJP)+1
          IJFR(I)=IJFR(I)-IJKPRC
          DNNZ(IJFR(I))=DNNZ(IJFR(I))+1
        END IF
        IJRGL(NOCBKAL+I)=IJN
      END DO
C
C.....SEND NONZEROS IN OFFDIAGONAL PART TO THE RESPECTIVE PROCESSOR
C
      CALL VecRestoreArray(ONNZL,ONNZARR,ONNZZI,IERR)
      CALL VecGhostRestoreLocalForm(ONNZVEC,ONNZL,IERR)
C
      CALL VecGhostUpdateBegin(
     *     ONNZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(ONNZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....CONVERT TO INTEGER
C
      CALL VecGetArray(ONNZVEC,ONNZARR,ONNZZI,IERR)
      DO I=1,NIJKBKAL
        ONNZT(I)=INT(ONNZ(I))
      END DO
      CALL VecRestoreArray(ONNZVEC,ONNZARR,ONNZZI,IERR)
      CALL VecDestroy(ONNZVEC,IERR)
C
C.....Create Matrix
C
      CALL MatCreate(PETSC_COMM_WORLD,AMAT,IERR)
      CALL MatSetSizes(AMAT,NIJKBKAL,NIJKBKAL,
     *                 PETSC_DECIDE,PETSC_DECIDE,IERR)
      CALL MatSetFromOptions(AMAT,IERR)
      CALL MatSeqAIJSetPreallocation(AMAT,PETSC_NULL_INTEGER,DNNZ,IERR)
      CALL MatMPIAIJSetPreallocation(
     *     AMAT,PETSC_NULL_INTEGER,DNNZ,PETSC_NULL_INTEGER,ONNZT,IERR)
C......Vereinheitlichung durch Verwendung dieser Routine
C     CALL MatXAIJSetPreallocation(
C    *     Mat A,PetscInt bs,const PetscInt *dnnz,const PetscInt *onnz,
C    *     const PetscInt *dnnzu,const PetscInt *onnzu)
C......Not necessary because already using preallocation routine
      CALL MatSetUp(AMAT,IERR)
C     CALL MatDuplicate(,MAT_SHARE_NONZERO_PATTERN,A2,IERR)
C
C.....Create ghosted Vectors
C
      CALL VecCreateGhost(
     *     PETSC_COMM_WORLD,NIJKBKAL,PETSC_DECIDE,NGH,IJGH,UVEC,IERR)
      CALL VecSetFromOptions(UVEC,IERR)
C
C.....Duplicate ghosted Vectors
C
      CALL VecDuplicate(UVEC,VVEC,IERR)
      CALL VecDuplicate(UVEC,WVEC,IERR)
C
      CALL VecDuplicate(UVEC,DUXVEC,IERR)
      CALL VecDuplicate(UVEC,DUYVEC,IERR)
      CALL VecDuplicate(UVEC,DUZVEC,IERR)
C
      CALL VecDuplicate(UVEC,DVXVEC,IERR)
      CALL VecDuplicate(UVEC,DVYVEC,IERR)
      CALL VecDuplicate(UVEC,DVZVEC,IERR)
C
      CALL VecDuplicate(UVEC,DWXVEC,IERR)
      CALL VecDuplicate(UVEC,DWYVEC,IERR)
      CALL VecDuplicate(UVEC,DWZVEC,IERR)
C
      CALL VecDuplicate(UVEC,PVEC,IERR)
      CALL VecDuplicate(UVEC,PPVEC,IERR)
      CALL VecDuplicate(UVEC,DPXVEC,IERR)
      CALL VecDuplicate(UVEC,DPYVEC,IERR)
      CALL VecDuplicate(UVEC,DPZVEC,IERR)
C
      CALL VecDuplicate(UVEC,TVEC,IERR)
C
      CALL VecDuplicate(UVEC,DFXOVEC,IERR)
      CALL VecDuplicate(UVEC,DFYOVEC,IERR)
      CALL VecDuplicate(UVEC,DFZOVEC,IERR)
C
      CALL VecDuplicate(UVEC,DENVEC,IERR)
      CALL VecDuplicate(UVEC,VISVEC,IERR)
      CALL VecDuplicate(UVEC,VOLVEC,IERR)
C
      CALL VecDuplicate(UVEC,XCVEC,IERR)
      CALL VecDuplicate(UVEC,YCVEC,IERR)
      CALL VecDuplicate(UVEC,ZCVEC,IERR)
C
      CALL VecDuplicate(UVEC,SUVEC,IERR)
      CALL VecDuplicate(UVEC,SVVEC,IERR)
      CALL VecDuplicate(UVEC,SWVEC,IERR)
      CALL VecDuplicate(UVEC,APVEC,IERR)
      CALL VecDuplicate(UVEC,APRVEC,IERR)
C
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE ASSEMBLESYS(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     system to be solved
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN
      INTEGER IJRGL(NOCA+NFA)
C
      PetscInt       I1,I7,ROW,COL(7),COL1,RANK
      PetscScalar    VAL(7),VAL1,PONE
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
      I1=1
      I7=7
      PONE=1.0D0
      CALL MatZeroEntries(CMAT,IERR)
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
C
      DO M=1,NBLKS
      CALL SETIND(M)
C
C.....INITIALIZE BOUNDARY ENTRIES SOUTH/NORH
C
      DO K=1,NK
      DO I=1,NI
      DO J=1,NJ,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        ROW=IJK-1
        COL1=IJK-1
        CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES WEST/EAST
C
      DO K=1,NK
      DO I=1,NI,NI-1
      DO J=1,NJ
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        ROW=IJK-1
        COL1=IJK-1
        CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES BOTTOM/TOP
C
      DO K=1,NK,NK-1
      DO I=1,NI
      DO J=1,NJ
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        ROW=IJK-1
        COL1=IJK-1
        CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C        
C.....ASSEMBLE MATRIX AND RHS-VECTOR
C
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL(1)=IJKP-NIJ; COL(2)=IJKP-NJ; COL(3)=IJKP-1
        COL(4)=IJKP
        COL(5)=IJKP+1; COL(6)=IJKP+NJ; COL(7)=IJKP+NIJ
C       
        VAL(1)=AB(IJK); VAL(2)=AW(IJK); VAL(3)=AS(IJK)
        VAL(4)=AP(IJK)
        VAL(5)=AN(IJK); VAL(6)=AE(IJK); VAL(7)=AT(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      IF (NJM.EQ.2) NJM=3
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
        IF (AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ; ENDIF
        IF (AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ ; ENDIF
        IF (AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  ; ENDIF
        IF (AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  ; ENDIF
        IF (AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ ; ENDIF
        IF (AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      IF (NIM.EQ.2) NIM=3
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
        IF (AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ; ENDIF
        IF (AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ ; ENDIF
        IF (AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  ; ENDIF
        IF (AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  ; ENDIF
        IF (AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ ; ENDIF
        IF (AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
      IF (NKM.EQ.2) NKM=3
      DO K=2,NKM,NKM-2
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        ROW=IJKP
C       
        COL=(/-1,-1,-1,IJKP,-1,-1,-1/)
C       
        VAL(4)=AP(IJK)
        IF (AB(IJK).NE.0) THEN; VAL(1)=AB(IJK); COL(1)=IJKP-NIJ; ENDIF
        IF (AW(IJK).NE.0) THEN; VAL(2)=AW(IJK); COL(2)=IJKP-NJ ; ENDIF
        IF (AS(IJK).NE.0) THEN; VAL(3)=AS(IJK); COL(3)=IJKP-1  ; ENDIF
        IF (AN(IJK).NE.0) THEN; VAL(5)=AN(IJK); COL(5)=IJKP+1  ; ENDIF
        IF (AE(IJK).NE.0) THEN; VAL(6)=AE(IJK); COL(6)=IJKP+NJ ; ENDIF
        IF (AT(IJK).NE.0) THEN; VAL(7)=AT(IJK); COL(7)=IJKP+NIJ; ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I7,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AFR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AFL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
      END DO
C
C.....FINAL MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FINAL_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FINAL_ASSEMBLY,IERR)
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE SOLVESYS(FIVEC,IFI,CMAT)
C#############################################################
C     This routine solves the linear systems for momentum or
C     pressure correction
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER IFI,LS,LC
C
      Vec FIVEC,VT1,VT2
      KSP KRYLOV,KRYLOVP,KRYLOVPIN
      PC PRECON,PRECONP
      PetscReal RTOL,RINIT
      MatNullSpace NULLSP
      PetscErrorCode IERR
      PetscBool ISNULL
      PetscOffset FIII
      Mat CMAT
      INTEGER IJRGL(NOCA+NFA)
      INTEGER I,J,K,IJK,M
      lOGICAL ISANULL
      COMMON /OUTER/ LS
      COMMON /CORRECTOR/ LC
      COMMON /PTEMP/ VT1,VT2,
     *               KRYLOV,KRYLOVP,PRECON,PRECONP
      COMMON /MAPPING/ IJRGL
      PetscInt       I1,I7,ROW,COL(7),COL1,RANK
      PetscScalar    VAL(7),VAL1,PONE
      CHARACTER(LEN=100) PREFIX
      REAL*8 FIARR(1)
#include "petsc.user.inc"
C     
C=============================================================
C
      IF (INISOL) THEN
        INISOL=.FALSE.
        CALL VecDuplicate(FIVEC,VT1,IERR)
        CALL VecDuplicate(FIVEC,VT2,IERR)
C
        CALL KSPCreate(PETSC_COMM_WORLD,KRYLOV,IERR)
        WRITE(PREFIX,"(A9)") "momentum_"
        CALL KSPSetOptionsPrefix(KRYLOV,PREFIX,IERR)
C
        CALL KSPCreate(PETSC_COMM_WORLD,KRYLOVP,IERR)
        WRITE(PREFIX,"(A9)") "pressure_"
        CALL KSPSetOptionsPrefix(KRYLOVP,PREFIX,IERR)
      END IF
C
      IF (IFI.NE.4) THEN
          CALL KSPSetOperators(KRYLOV,CMAT,CMAT,IERR)
      ELSE
          CALL KSPSetOperators(KRYLOVP,CMAT,CMAT,IERR)
      END IF
C
      RINIT=0.0D0
      CALL MatMult(CMAT,FIVEC,VT1,IERR)
      CALL VecCopy(SUVEC,VT2,IERR)
      CALL VecAXPY(VT2,-1.0D0,VT1,IERR)
      CALL VecNorm(VT2,NORM_2,RINIT,IERR)
      IF (LS.EQ.1) THEN
        IF (.NOT.LREAD) THEN
          RESINI(IFI)=RINIT
          RESFIN(IFI)=RINIT*SORMAX
          RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
        ELSE
          RINIT=RESINI(IFI)
        END IF
        RTOL=SOR(IFI)
      ELSE
        RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
        RTOL=RESOR(IFI)*SOR(IFI)
      END IF
      IF (RINIT/(RESINI(IFI)+SMALL).LT.SORMAX) RETURN
C
C.....SOLVE MOMENTUM BALANCE
C
      IF (IFI.NE.4) THEN
        CALL KSPSetType(KRYLOV,KSPPREONLY,IERR)
        CALL KSPGetPC(KRYLOV,PRECON,IERR)
        CALL PCSetType(PRECON,PCREDISTRIBUTE,IERR)
        CALL KSPSetInitialGuessNonzero(KRYLOV,PETSC_FALSE,ierr)
        CALL PCRedistributeGetKSP(PRECON,KRYLOVPIN,IERR)
C
        CALL KSPSetType(KRYLOVPIN,KSPBCGS,IERR)
        CALL KSPSetFromOptions(KRYLOV,IERR)
        CALL KSPSetTolerances(KRYLOVPIN,
     *                        RTOL, 
     *                        RESFIN(IFI),
     *                        PETSC_DEFAULT_REAL,
     *                        PETSC_DEFAULT_INTEGER,
     *                        IERR)
        CALL KSPSolve(KRYLOV,SUVEC,FIVEC,IERR)
        CALL KSPSetInitialGuessNonzero(KRYLOV,PETSC_TRUE,IERR)
      ELSE
C
C.....SOLVE PRESSURE CORRECTION
C
        CALL KSPSetType(KRYLOVP,KSPPREONLY,IERR)
        CALL KSPGetPC(KRYLOVP,PRECONP,IERR)
        CALL PCSetType(PRECONP,PCREDISTRIBUTE,IERR)
        CALL PCRedistributeGetKSP(PRECONP,KRYLOVPIN,IERR)
C
        CALL KSPSetType(KRYLOVPIN,KSPCG,IERR)
C
C.....CREATE NULLSPACE - PRESSURE CORRECTION ONLY (SINGULAR MATRIX)
C
        CALL MatNullSpaceCreate(
     *      PETSC_COMM_WORLD,PETSC_TRUE,0,PETSC_NULL_OBJECT,NULLSP,IERR)
        CALL KSPSetNullSpace(KRYLOVPIN,NULLSP,IERR)
        CALL MatNullSpaceDestroy(NULLSP,IERR);
        CALL KSPSetInitialGuessNonzero(KRYLOVP,PETSC_FALSE,ierr)
C
        CALL KSPSetFromOptions(KRYLOVP,IERR)
        CALL KSPSetTolerances(KRYLOVPIN,
     *                        RTOL, 
     *                        RESFIN(IFI),
     *                        PETSC_DEFAULT_REAL,
     *                        PETSC_DEFAULT_INTEGER,
     *                        IERR)
        CALL KSPSolve(KRYLOVP,SUVEC,FIVEC,IERR)
      END IF
C
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE DESTROYPETSC
C#############################################################
C     This routine destroys the PETSc Matrix of the linear
C     system to be solved and objects related to SOLVESYS
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "gradold3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      Vec FIVEC,VT1,VT2
      KSP KRYLOV,KRYLOVP
      PC PRECON,PRECONP
      PetscErrorCode IERR
      COMMON /PTEMP/ VT1,VT2,
     *               KRYLOV,KRYLOVP,PRECON,PRECONP
C     
C=============================================================
C
C
C.....DESTROY MATRICES
C
      CALL MatDestroy(AMAT,IERR)
      CALL MatDestroy(A2,IERR)
C
C.....DESTROY KSP
C
      CALL KSPDestroy(KRYLOV,IERR) 
      CALL KSPDestroy(KRYLOVP,IERR) 
C
C.....DESTROY VECTORS
C
      CALL VecDestroy(UVEC,IERR)
      CALL VecDestroy(VVEC,IERR)
      CALL VecDestroy(WVEC,IERR)
C
      CALL VecDestroy(DUXVEC,IERR)
      CALL VecDestroy(DUYVEC,IERR)
      CALL VecDestroy(DUZVEC,IERR)
C
      CALL VecDestroy(DVXVEC,IERR)
      CALL VecDestroy(DVYVEC,IERR)
      CALL VecDestroy(DVZVEC,IERR)
C
      CALL VecDestroy(DWXVEC,IERR)
      CALL VecDestroy(DWYVEC,IERR)
      CALL VecDestroy(DWZVEC,IERR)
C
      CALL VecDestroy(PVEC,IERR)
      CALL VecDestroy(PPVEC,IERR)
      CALL VecDestroy(DPXVEC,IERR)
      CALL VecDestroy(DPYVEC,IERR)
      CALL VecDestroy(DPZVEC,IERR)
C
      CALL VecDestroy(TVEC,IERR)
C
      CALL VecDestroy(DFXOVEC,IERR)
      CALL VecDestroy(DFYOVEC,IERR)
      CALL VecDestroy(DFZOVEC,IERR)
C
      CALL VecDestroy(DENVEC,IERR)
      CALL VecDestroy(VISVEC,IERR)
      CALL VecDestroy(VOLVEC,IERR)
C
      CALL VecDestroy(XCVEC,IERR)
      CALL VecDestroy(YCVEC,IERR)
      CALL VecDestroy(ZCVEC,IERR)
C
      CALL VecDestroy(SUVEC,IERR)
      CALL VecDestroy(SVVEC,IERR)
      CALL VecDestroy(SWVEC,IERR)
      CALL VecDestroy(APVEC,IERR)
      CALL VecDestroy(APRVEC,IERR)
C
      CALL VecDestroy(VT1,IERR)
      CALL VecDestroy(VT2,IERR)
C
      RETURN
      END
C
C
C#############################################################
      SUBROUTINE DEBUGME
C#############################################################
C     This routine serves only for debugging purposes using
C     more than one mpi process
C=============================================================
C
C INCLUDE NECESSARY SYSTEM ROUTINES LIKE GETPIT AND HOSTNM
#ifdef USE_INTEL_COMPILER
        USE IFPORT
#endif
        IMPLICIT NONE
#include "mpif.h"
        CHARACTER*20 HNAME
        INTEGER I,PID,HSTAT
C
        HSTAT = HOSTNM(HNAME)
        PID = GETPID()
        PRINT *,"PID ",PID,"ON HOST ",TRIM(HNAME)," IS READY FOR ATTACH"
        I = 0
500     IF (I == 0) THEN
          CALL SLEEP(5)
          GOTO 500
        END IF
C
        RETURN
        END SUBROUTINE DEBUGME
C
C
