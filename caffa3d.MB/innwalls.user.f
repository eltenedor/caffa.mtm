C#########################################################
      SUBROUTINE INNWALLS
C#########################################################
C     Computes grid data for inner walls treatment
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C
C END NEW
#include "param3d.inc"
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C.....Initialize wall faces counters
C
      NEWA=0
      NNWA=0
      NTWA=0
C
C.....Read from '.cin' number of walls
C
      READ(5,*) NWALLS
C
C.....Loop reading wall specifications and initializing
C
      DO IWALL=1,NWALLS
        READ(5,*) M,ISS,IE,JS,JE,KS,KE
        CALL SETIND(M)
C
C.....Initialize KJ Wall Sections ("East Walls")
C
        IF(ISS.EQ.IE) THEN
          DO K=KS,KE
          DO J=JS,JE
            NEWA=NEWA+1
            IW=NEWA
            IJEW(IW)=LKBK(K+KST)+LIBK(ISS+IST)+J
            IJPEW(IW)= IJEW(IW)-NJ
            IJEW1(IW)= IJEW(IW)-NJ
            IJEW2(IW)=IJEW1(IW)-1
            IJEW3(IW)=IJEW2(IW)-NIJ
            IJEW4(IW)=IJEW3(IW)+1
            CALL CALCFACE(IJEW1(IW),IJEW2(IW),IJEW3(IW),IJEW4(IW),
     *                    XEWC(IW),YEWC(IW),ZEWC(IW),
     *                    XEWR(IW),YEWR(IW),ZEWR(IW))
          END DO
          END DO
        END IF
C
C.....Initialize KI Wall Sections ("North Walls")
C
        IF(JS.EQ.JE) THEN
          DO K=KS,KE
          DO I=ISS,IE
            NNWA=NNWA+1
            IW=NNWA
            IJNW(IW)=LKBK(K+KST)+LIBK(I+IST)+JS
            IJPNW(IW)= IJNW(IW)-1
            IJNW1(IW)= IJNW(IW)-NJ-1
            IJNW2(IW)=IJNW1(IW)+NJ
            IJNW3(IW)=IJNW2(IW)-NIJ
            IJNW4(IW)=IJNW3(IW)-NJ
            CALL CALCFACE(IJNW1(IW),IJNW2(IW),IJNW3(IW),IJNW4(IW),
     *                    XNWC(IW),YNWC(IW),ZNWC(IW),
     *                    XNWR(IW),YNWR(IW),ZNWR(IW))
          END DO
          END DO
        END IF
C
C.....Initialize IJ Wall Sections ("Top Walls")
C
        IF(KS.EQ.KE) THEN
          DO I=ISS,IE
          DO J=JS,JE
            NTWA=NTWA+1
            IW=NTWA
            IJTW(IW)=LKBK(KS+KST)+LIBK(I+IST)+J
            IJPTW(IW)= IJTW(IW)-NIJ
            IJTW1(IW)= IJTW(IW)-NIJ
            IJTW2(IW)=IJTW1(IW)-NJ
            IJTW3(IW)=IJTW2(IW)-1
            IJTW4(IW)=IJTW3(IW)+NJ
            CALL CALCFACE(IJTW1(IW),IJTW2(IW),IJTW3(IW),IJTW4(IW),
     *                    XTWC(IW),YTWC(IW),ZTWC(IW),
     *                    XTWR(IW),YTWR(IW),ZTWR(IW))
          END DO
          END DO
        END IF
C
      END DO
C
C
      RETURN
      END
C
C#########################################################
      SUBROUTINE INNWALLFIX
C#########################################################
C     Force values for U,V,P at inner wall nodes
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C
#define  U(IJK)  UARR(UUI+ IJK)
#define  V(IJK)  VARR(VVI+ IJK)
#define  W(IJK)  WARR(WWI+ IJK)
#define  P(IJK)  PARR(PPI+ IJK)
#define PP(IJK) PPARR(PPPI+IJK)
#define  T(IJK)  TARR(TTI+ IJK)
C
C END NEW
#include "param3d.inc"
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C.....TREATMENT OF EAST WALLS
C
C NEW
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C
C END NEW
      DO IW=1,NEWA
        IJB=IJEW(IW)
        IJP=IJPEW(IW)
        IJV=IJP-(IJB-IJP)
        IJE=IJB+(IJB-IJP)
C
C.....Force values for U,V,W,F1,F2,F3
C
        F1(IJV)=0
        U(IJP)=0
        V(IJP)=0
        W(IJP)=0
        F1(IJP)=0
        F2(IJP)=0
        F3(IJP)=0
        U(IJB)=0
        V(IJB)=0
        W(IJB)=0
        F1(IJB)=0
        F2(IJB)=0
        F3(IJB)=0
C
C.....Extrapolate pressure, pressure correction,
C.....and temperature to nodes IJP and IJB
C
        P(IJP)=P(IJV)
        PP(IJP)=0
        T(IJP)=T(IJV)
C
        P(IJB)=P(IJE)
        PP(IJB)=0
        T(IJB)=T(IJE)
C
      END DO
C
C
C.....TREATMENT OF NORTH WALLS
C
      DO IW=1,NNWA
        IJB=IJNW(IW)
        IJP=IJPNW(IW)
        IJZ=IJP-(IJB-IJP)
        IJN=IJB+(IJB-IJP)
C
C.....Force values for U,V,W,F1,F2,F3
C
        F2(IJZ)=0
        U(IJP)=0
        V(IJP)=0
        W(IJP)=0
        F1(IJP)=0
        F2(IJP)=0
        F3(IJP)=0
        U(IJB)=0
        V(IJB)=0
        W(IJB)=0
        F1(IJB)=0
        F2(IJB)=0
        F3(IJB)=0
C
C.....Extrapolate pressure, pressure correction,
C.....and temperature to nodes IJP and IJB
C
        P(IJP)=P(IJZ)
        PP(IJP)=0
        T(IJP)=T(IJZ)
C
        P(IJB)=P(IJN)
        PP(IJB)=0
        T(IJB)=T(IJN)
C
      END DO
C
C
C.....TREATMENT OF TOP WALLS
C
      DO IW=1,NTWA
        IJB=IJTW(IW)
        IJP=IJPTW(IW)
        IJZ=IJP-(IJB-IJP)
        IJT=IJB+(IJB-IJP)
C
C.....Force values for U,V,W,F1,F2,F3
C
        F3(IJZ)=0
        U(IJP)=0
        V(IJP)=0
        W(IJP)=0
        F1(IJP)=0
        F2(IJP)=0
        F3(IJP)=0
        U(IJB)=0
        V(IJB)=0
        W(IJB)=0
        F1(IJB)=0
        F2(IJB)=0
        F3(IJB)=0
C
C.....Extrapolate pressure, pressure correction,
C.....and temperature to nodes IJP and IJB
C
        P(IJP)=P(IJZ)
        PP(IJP)=0
        T(IJP)=T(IJZ)
C
        P(IJB)=P(IJT)
        PP(IJB)=0
        T(IJB)=T(IJT)
C
      END DO
C NEW
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C END NEW
C
C
      RETURN
C
C
#undef  U(IJK)
#undef  V(IJK)
#undef  W(IJK)
#undef  P(IJK)
#undef PP(IJK)
#undef  T(IJK)
C
      END
C
C
C#########################################################
      SUBROUTINE INNWALLUVW
C#########################################################
C     Modifications to UVW equations for
C     inner walls treatment.
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
#define AP(IJK) APARR(APPI+IJK)
#define SU(IJK) SUARR(SUUI+IJK)
#define SV(IJK) SVARR(SVVI+IJK)
#define SW(IJK) SWARR(SWWI+IJK)
C END NEW
#include "param3d.inc"
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C
      GU=GDS(1)
C
C.....TREATMENT OF EAST WALLS
C
      DO IW=1,NEWA
        IJB=IJEW(IW)
        IJP=IJPEW(IW)
        IJV=IJP-(IJB-IJP)
        IJE=IJB+(IJB-IJP)
C
C.....Modify equations for IJP and IJB
C
        SU(IJP)=0.
        SV(IJP)=0.
        SW(IJP)=0.
        AW(IJP)=0.
        AE(IJP)=0.
        AS(IJP)=0.
        AN(IJP)=0.
        AB(IJP)=0.
        AT(IJP)=0.
        AP(IJP)=1.
C
        SU(IJB)=0.
        SV(IJB)=0.
        SW(IJB)=0.
        AW(IJB)=0.
        AE(IJB)=0.
        AS(IJB)=0.
        AN(IJB)=0.
        AB(IJB)=0.
        AT(IJB)=0.
        AP(IJB)=1.
C
C.....Modify equations for IJE and IJV ( IJW )
C.....Undo Inner EastWard Fluxes Contribution
C
        SUPREM=SU(IJV)
        SVPREM=SV(IJV)
        SWPREM=SW(IJV)
        CALL FLUXUVW(IJV,IJP,XEWC(IJV),YEWC(IJV),ZEWC(IJV),
     *                       XEWR(IJV),YEWR(IJV),ZEWR(IJV),
     *               F1(IJV),CP,CB,FX(IJV),GU)
        SU(IJV)=2*SUPREM-SU(IJV)
        SV(IJV)=2*SVPREM-SV(IJV)
        SW(IJV)=2*SWPREM-SW(IJV)
        AE(IJV)=0.
C
        SUPREM=SU(IJE)
        SVPREM=SV(IJE)
        SWPREM=SW(IJE)
        CALL FLUXUVW(IJB,IJE,XEWC(IJB),YEWC(IJB),ZEWC(IJB),
     *                       XEWR(IJB),YEWR(IJB),ZEWR(IJB),
     *               F1(IJB),CP,CB,FX(IJB),GU)
        SU(IJE)=2*SUPREM-SU(IJE)
        SV(IJE)=2*SVPREM-SV(IJE)
        SW(IJE)=2*SWPREM-SW(IJE)
        AW(IJE)=0.
C
      END DO
C
C.....TREATMENT OF NORTH WALLS
C
      DO IW=1,NNWA
        IJB=IJNW(IW)
        IJP=IJPNW(IW)
        IJZ=IJP-(IJB-IJP)
        IJN=IJB+(IJB-IJP)
C
C.....Modify equations for IJP and IJB
C
        SU(IJP)=0.
        SV(IJP)=0.
        SW(IJP)=0.
        AW(IJP)=0.
        AE(IJP)=0.
        AS(IJP)=0.
        AN(IJP)=0.
        AB(IJP)=0.
        AT(IJP)=0.
        AP(IJP)=1.
C
        SU(IJB)=0.
        SV(IJB)=0.
        SW(IJB)=0.
        AW(IJB)=0.
        AE(IJB)=0.
        AS(IJB)=0.
        AN(IJB)=0.
        AB(IJB)=0.
        AT(IJB)=0.
        AP(IJB)=1
C
C.....Modify equations for IJN and IJZ
C.....Undo Inner NortWard Fluxes Contribution
C
        SUPREM=SU(IJZ)
        SVPREM=SV(IJZ)
        SWPREM=SW(IJZ)
        CALL FLUXUVW(IJZ,IJP,XNWC(IJZ),YNWC(IJZ),ZNWC(IJZ),
     *                       XNWR(IJZ),YNWR(IJZ),ZNWR(IJZ),
     *               F2(IJZ),CP,CB,FY(IJZ),GU)
        SU(IJZ)=2*SUPREM-SU(IJZ)
        SV(IJZ)=2*SVPREM-SV(IJZ)
        SW(IJZ)=2*SWPREM-SW(IJZ)
        AN(IJZ)=0.
C
        SUPREM=SU(IJN)
        SVPREM=SV(IJN)
        SWPREM=SW(IJN)
        CALL FLUXUVW(IJB,IJN,XNWC(IJB),YNWC(IJB),ZNWC(IJB),
     *                       XNWR(IJB),YNWR(IJB),ZNWR(IJB),
     *               F2(IJB),CP,CB,FY(IJB),GU)
        SU(IJN)=2*SUPREM-SU(IJN)
        SV(IJN)=2*SVPREM-SV(IJN)
        SW(IJN)=2*SWPREM-SW(IJN)
        AS(IJN)=0.
C
      END DO
C
C.....TREATMENT OF TOP WALLS
C
      DO IW=1,NTWA
        IJB=IJTW(IW)
        IJP=IJPTW(IW)
        IJZ=IJP-(IJB-IJP)
        IJT=IJB+(IJB-IJP)
C
C.....Modify equations for IJP and IJB
C
        SU(IJP)=0.
        SV(IJP)=0.
        SW(IJP)=0.
        AW(IJP)=0.
        AE(IJP)=0.
        AS(IJP)=0.
        AN(IJP)=0.
        AB(IJP)=0.
        AT(IJP)=0.
        AP(IJP)=1.
C
        SU(IJB)=0.
        SV(IJB)=0.
        SW(IJB)=0.
        AW(IJB)=0.
        AE(IJB)=0.
        AS(IJB)=0.
        AN(IJB)=0.
        AB(IJB)=0.
        AT(IJB)=0.
        AP(IJB)=1
C
C.....Modify equations for IJT and IJZ
C.....Undo Inner NortWard Fluxes Contribution
C
        SUPREM=SU(IJZ)
        SVPREM=SV(IJZ)
        SWPREM=SW(IJZ)
        CALL FLUXUVW(IJZ,IJP,XTWC(IJZ),YTWC(IJZ),ZTWC(IJZ),
     *                       XTWR(IJZ),YTWR(IJZ),ZTWR(IJZ),
     *               F3(IJZ),CP,CB,FZ(IJZ),GU)
        SU(IJZ)=2*SUPREM-SU(IJZ)
        SV(IJZ)=2*SVPREM-SV(IJZ)
        SW(IJZ)=2*SWPREM-SW(IJZ)
        AT(IJZ)=0.
C
        SUPREM=SU(IJT)
        SVPREM=SV(IJT)
        SWPREM=SW(IJT)
        CALL FLUXUVW(IJB,IJT,XTWC(IJB),YTWC(IJB),ZTWC(IJB),
     *                       XTWR(IJB),YTWR(IJB),ZTWR(IJB),
     *               F3(IJB),CP,CB,FZ(IJB),GU)
        SU(IJT)=2*SUPREM-SU(IJT)
        SV(IJT)=2*SVPREM-SV(IJT)
        SW(IJT)=2*SWPREM-SW(IJT)
        AB(IJT)=0.
C
      END DO
C
      RETURN
#undef AP
#undef SU
#undef SV
#undef SW
      END
C
C#########################################################
      SUBROUTINE INNWALLPP
C#########################################################
C     Modifications to P equation for
C     inner walls treatment.
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
#define AP(IJK) APARR(APPI+IJK)
C END NEW
#include "param3d.inc"
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C
C.....TREATMENT OF EAST WALLS
C
      DO IW=1,NEWA
        IJB=IJEW(IW)
        IJP=IJPEW(IW)
        IJV=IJP-(IJB-IJP)
        IJE=IJB+(IJB-IJP)
C
C.....Modify equations for IJP and IJB
C
        AE(IJP)=0.
        AW(IJP)=0.
        AS(IJP)=0.
        AN(IJP)=0.
        AB(IJP)=0.
        AT(IJP)=0.
        AP(IJP)=1.
C
        AE(IJB)=0.
        AW(IJB)=0.
        AS(IJB)=0.
        AN(IJB)=0.
        AB(IJB)=0.
        AT(IJB)=0.
        AP(IJB)=1.
C
C.....Modify equations for IJE and IJV   ( IJW )
C
        F1(IJV)=0
        AE(IJV)=0
        F1(IJB)=0
        AW(IJE)=0
C
      END DO
C
C.....TREATMENT OF NORTH WALLS
C
      DO IW=1,NNWA
        IJB=IJNW(IW)
        IJP=IJPNW(IW)
        IJZ=IJP-(IJB-IJP)
        IJN=IJB+(IJB-IJP)
C
C.....Modify equations for IJP and IJB
C
        AE(IJP)=0.
        AW(IJP)=0.
        AS(IJP)=0.
        AN(IJP)=0.
        AB(IJP)=0.
        AT(IJP)=0.
        AP(IJP)=1.
C
        AE(IJB)=0.
        AW(IJB)=0.
        AS(IJB)=0.
        AN(IJB)=0.
        AB(IJB)=0.
        AT(IJB)=0.
        AP(IJB)=1.
C
C.....Modify equations for IJN and IJZ
C
        F2(IJZ)=0
        AN(IJZ)=0
        F2(IJB)=0
        AS(IJN)=0
C
      END DO
C
C
C.....TREATMENT OF TOP WALLS
C
      DO IW=1,NTWA
        IJB=IJTW(IW)
        IJP=IJPTW(IW)
        IJZ=IJP-(IJB-IJP)
        IJT=IJB+(IJB-IJP)
C
C.....Modify equations for IJP and IJB
C
        AE(IJP)=0.
        AW(IJP)=0.
        AS(IJP)=0.
        AN(IJP)=0.
        AB(IJP)=0.
        AT(IJP)=0.
        AP(IJP)=1.
C
        AE(IJB)=0.
        AW(IJB)=0.
        AS(IJB)=0.
        AN(IJB)=0.
        AB(IJB)=0.
        AT(IJB)=0.
        AP(IJB)=1.
C
C.....Modify equations for IJN and IJZ
C
        F3(IJZ)=0
        AT(IJZ)=0
        F3(IJB)=0
        AB(IJT)=0
C
      END DO
C
      RETURN
#undef AP(IJK)
      END
C
C#########################################################
      SUBROUTINE INNWALLPC
C#########################################################
C     Modifications to P corrections for
C     inner walls treatment.
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#include "param3d.inc"
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE INNWALLTT
C#########################################################
C     Modifications to T equations for
C     inner walls treatment.
C=========================================================
#include "param3d.inc"
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE INNWALLTE
C#########################################################
C     Modifications to T equations for
C     inner walls treatment.
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#include "param3d.inc"
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE INNWALLED
C#########################################################
C     Modifications to T equations for
C     inner walls treatment.
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#include "param3d.inc"
#include "partype3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "parinw3d.inc"
#include "bcinw3d.inc"
C
C
      RETURN
      END
C
C
