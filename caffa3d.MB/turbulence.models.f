C#########################################################
      SUBROUTINE SMAGOR
C#########################################################
C     Smagorinsky model for eddy visocosity
C     "Fluent's" RNG version
C=========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#INCLUDE "param3d.inc"
#INCLUDE "partype3d.inc"
#INCLUDE "parinw3d.inc"
#INCLUDE "indexc3d.inc"
#INCLUDE "logic3d.inc"
#INCLUDE "rcont3d.inc"
#INCLUDE "var3d.inc"
#INCLUDE "geo3d.inc"
#INCLUDE "coef3d.inc"
#INCLUDE "varold3d.inc"
#INCLUDE "grad3d.inc"
#INCLUDE "bound3d.inc"
#INCLUDE "bcinw3d.inc"
#INCLUDE "model3d.inc"
C
C   
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
c       G11=2.*DUX(IJK)**2
c       G12=(DUY(IJK)+DVX(IJK))*DUY(IJK)
c       G13=(DUZ(IJK)+DWX(IJK))*DUZ(IJK)
c       G21=(DVX(IJK)+DUY(IJK))*DVX(IJK)
c       G22=2.*DVY(IJK)**2
c       G23=(DVZ(IJK)+DWY(IJK))*DVZ(IJK)
c       G31=(DWX(IJK)+DUZ(IJK))*DWX(IJK)
c       G32=(DWY(IJK)+DVZ(IJK))*DWY(IJK)
c       G33=2.*DWZ(IJK)**2
C
        G11=4.*DUX(IJK)**2
        G12=(DUY(IJK)+DVX(IJK))**2
        G13=(DUZ(IJK)+DWX(IJK))**2
        G21=(DVX(IJK)+DUY(IJK))**2
        G22=4.*DVY(IJK)**2
        G23=(DVZ(IJK)+DWY(IJK))**2
        G31=(DWX(IJK)+DUZ(IJK))**2
        G32=(DWY(IJK)+DVZ(IJK))**2
        G33=4.*DWZ(IJK)**2
C
        ZFAC=0.1+MIN(0.9,ZC(IJK)/10.0)
        GBAS=((U(IJK)*0.15/0.2)*ZFAC)**2  
C
        GEN(IJK)=.5*(G11+G12+G13+G21+G22+G23
     *          +G31+G32+G33)+GBAS 
        GEN(IJK)=MAX(GEN(IJK),ZERO)
        SGSD=VOL(IJK)**(1/3)
        GEN(IJK)=SGSD**2*GEN(IJK)**0.5
        SGSR=GEN(IJK)*DEN(IJK)/VISC
C.........Ferziger's Dammping
        SGSC=0.100/(1+24.5/(SGSR+SMALL))
        VSL=DEN(IJK)*GEN(IJK)*SGSC**2
        VIS(IJK)=VISC+VSL
      END DO
      END DO
      END DO
      END DO
C
C.....WALL BOUNDARIES
C
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        IJQ=IJB+2*(IJP-IJB)
C
        VIS(IJP)=VIS(IJQ)
C
        ARE=SQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
        VNP=U(IJP)*XNW(IW)+V(IJP)*YNW(IW)+W(IJP)*ZNW(IW)
        XTP=U(IJP)-VNP*XNW(IW)/(ARE+SMALL)
        YTP=V(IJP)-VNP*YNW(IW)/(ARE+SMALL)
        ZTP=W(IJP)-VNP*ZNW(IW)/(ARE+SMALL)
        VTP=SQRT(XTP**2+YTP**2+ZTP**2)
        XTP=XTP/(VTP+SMALL)
        YTP=YTP/(VTP+SMALL)
        ZTP=ZTP/(VTP+SMALL)
C
        UT2=ABS((U(IJB)-U(IJP))*XTP
     *         +(V(IJB)-V(IJP))*YTP
     *         +(W(IJB)-W(IJP))*ZTP)
        UT3=ABS((U(IJB)-U(IJQ))*XTP
     *         +(V(IJB)-V(IJQ))*YTP
     *         +(W(IJB)-W(IJQ))*ZTP)
C
        TAU=VISC*UT2/DN(IW)
        UTAU=SQRT(TAU)/DEN(IJB)
        YPLUS=DEN(IJB)*UTAU*DN(IW)/VISC
        VISCW=0.
        IF (YPLUS.GT.CTRANS) THEN
          UTAU=(UT3-UT2)*CAPPA/LOG(3.)
          YPLUS=DEN(IJB)*UTAU*DN(IW)/VISC
          YPLUS=MAX(YPLUS,ZERO)
          VISCW=YPLUS*VISC*CAPPA/LOG(ELOG*YPLUS+SMALL)
        ENDIF
        VISW(IW)=MAX(VISC,VISCW)
        VIS(IJB)=VISW(IW)
      END DO
C
      RXVISC=0.
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        RXVISC=MAX(RXVISC,VIS(IJK))
      END DO
      END DO
      END DO
      END DO
C
      RETURN
      END
C
C
C############################################################### 
      SUBROUTINE KINE 
C###############################################################
C     This routine assembles the source terms (volume integrals)
C     and applies wall boundary conditions for the turbulent
C     kinetic energy equation.
C===============================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#INCLUDE "param3d.inc"
#INCLUDE "partype3d.inc"
#INCLUDE "parinw3d.inc"
#INCLUDE "indexc3d.inc"
#INCLUDE "logic3d.inc"
#INCLUDE "rcont3d.inc"
#INCLUDE "var3d.inc"
#INCLUDE "geo3d.inc"
#INCLUDE "coef3d.inc"
#INCLUDE "varold3d.inc"
#INCLUDE "grad3d.inc"
#INCLUDE "bound3d.inc"
#INCLUDE "bcinw3d.inc"
#INCLUDE "model3d.inc"
C
C.....Compute temperature gradients for Bouyancy source. 
C
      CALL  GRADFI(T,DPX,DPY,DPZ)
C
C.....VOLUMETRIC SOURCES OF THE MODELLED TURBULENT KINETIC ENERGY
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=1,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        G11=2.*DUX(IJK)**2
        G12=(DUY(IJK)+DVX(IJK))*DUY(IJK)
        G13=(DUZ(IJK)+DWX(IJK))*DUZ(IJK)
        G21=(DVX(IJK)+DUY(IJK))*DVX(IJK)
        G22=2.*DVY(IJK)**2
        G23=(DVZ(IJK)+DWY(IJK))*DVZ(IJK)
        G31=(DWX(IJK)+DUZ(IJK))*DWX(IJK)
        G32=(DWY(IJK)+DVZ(IJK))*DWY(IJK)
        G33=2.*DWZ(IJK)**2
C
        GEN(IJK)=G11+G12+G13+G21+G22+G23+
     *           G31+G32+G33
C       GEN(IJK)=(VIS(IJK)-VISC)*GEN(IJK)
        GEN(IJK)=VIS(IJK)*GEN(IJK)
C
C.....Bouyancy effects on k-e production. 
C
        GENB=BETA*(VIS(IJK)-VISC)/PRANL*
     *       (GRAVX*DPX(IJK)+GRAVY*DPY(IJK)+GRAVZ*DPZ(IJK))
C
        SU(IJK)=SU(IJK)+(GEN(IJK)+GENB*TGEN)*VOL(IJK)
        AP(IJK)=AP(IJK)+DEN(IJK)*VOL(IJK)*ED(IJK)/(TE(IJK)+SMALL)
      END DO
      END DO
      END DO
C
      END DO
C
C.....WALL BOUNDARIES
C
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        IJQ=IJB+2*(IJP-IJB)
C
        VIS(IJP)=VIS(IJQ)
C
        SU(IJP)=SU(IJP)-GEN(IJP)*VOL(IJP)
        VISS=VISC 
        IF(LCAL(ITE).AND.(YPL(IW)).GT.CTRANS) VISS=VISW(IW)
C
        ARE=SQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
        VNP=U(IJP)*XNW(IW)+V(IJP)*YNW(IW)+W(IJP)*ZNW(IW)
        XTP=U(IJP)-VNP*XNW(IW)/(ARE+SMALL)
        YTP=V(IJP)-VNP*YNW(IW)/(ARE+SMALL)
        ZTP=W(IJP)-VNP*ZNW(IW)/(ARE+SMALL)
        VTP=SQRT(XTP**2+YTP**2+ZTP**2)
        XTP=XTP/(VTP+SMALL)
        YTP=YTP/(VTP+SMALL)
        ZTP=ZTP/(VTP+SMALL)
        UT2=ABS((U(IJB)-U(IJP))*XTP
     *         +(V(IJB)-V(IJP))*YTP
     *         +(W(IJB)-W(IJP))*ZTP)
C
        TAU=VISC*UT2/DN(IW)
        GEN(IJP)=ABS(TAU)*CMU25*SQRT(MAX(ZERO,TE(IJP)))/(DN(IW)*CAPPA)
        SU(IJP)=SU(IJP)+GEN(IJP)*VOL(IJP)
      END DO
C
C.....Call to Inner Walls TE modifications routine
C
      CALL INNWALLTE
C
      RETURN
      END
C
C
C############################################################### 
      SUBROUTINE DISE
C###############################################################
C     This routine assembles the source terms (volume integrals)
C     and applies wall boundary conditions for the dissipation
C     rate equation.
C===============================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#INCLUDE "param3d.inc"
#INCLUDE "partype3d.inc"
#INCLUDE "parinw3d.inc"
#INCLUDE "indexc3d.inc"
#INCLUDE "logic3d.inc"
#INCLUDE "rcont3d.inc"
#INCLUDE "var3d.inc"
#INCLUDE "geo3d.inc"
#INCLUDE "coef3d.inc"
#INCLUDE "varold3d.inc"
#INCLUDE "grad3d.inc"
#INCLUDE "bound3d.inc"
#INCLUDE "bcinw3d.inc"
#INCLUDE "model3d.inc"
C
C.....VOLUMETRIC SOURCES FOR THE MODELLED DISSIPATION RATE 
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        TMP=ED(IJK)*VOL(IJK)/(TE(IJK)+SMALL)
        SU(IJK)=SU(IJK)+CE1*TMP*GEN(IJK)
        AP(IJK)=AP(IJK)+CE2*TMP*DEN(IJK)
      END DO
      END DO
      END DO
C
      END DO  
C
C.....WALL BOUNDARIES APPROXIMATED WITH WALL FUNCTIONS
C.....FOR CORRECT VALUES OF DISSIPATION ALL COEFFICIENTS HAVE
C.....TO BE ZERO, SU EQUAL THE DISSIPATION, AND AP = 1
C
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        ED(IJP)=CMU75*(MAX(ZERO,TE(IJP)))**1.5/(CAPPA*DN(IW))
        SU(IJP)=ED(IJP)
        AP(IJP)=1.
        AS(IJP)=0.
        AN(IJP)=0.
        AW(IJP)=0.
        AE(IJP)=0.
        AB(IJP)=0.
        AT(IJP)=0.
      END DO
C
C.....Call to Inner Walls ED modifications routine
C
      CALL INNWALLED
C
      RETURN
      END
C
C
C#############################################################
      SUBROUTINE MODVIS
C#############################################################
C     This routine calculates the eddy viscosity and the
C     dimensionless distance from the wall; also, an effective
C     wall viscosity is calculated so that the shear stress
C     can be computed using the same approximation as for
C     laminar flows (see Eqs. (9.37), (8.74) and (8.73)).
C=============================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#INCLUDE "param3d.inc"
#INCLUDE "partype3d.inc"
#INCLUDE "parinw3d.inc"
#INCLUDE "indexc3d.inc"
#INCLUDE "logic3d.inc"
#INCLUDE "rcont3d.inc"
#INCLUDE "var3d.inc"
#INCLUDE "geo3d.inc"
#INCLUDE "coef3d.inc"
#INCLUDE "varold3d.inc"
#INCLUDE "grad3d.inc"
#INCLUDE "bound3d.inc"
#INCLUDE "bcinw3d.inc"
#INCLUDE "model3d.inc"
C
C.....Maximum turbulent viscosity monitor.
C
      RXVISC=0.
C
C.....EDDY VISCOSITY,DIMENSIONLESS WALL DISTANCE AND LOCAL REYNOLDS-NUMBER
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        VISOLD=VIS(IJK)
        VIS(IJK)=(VISC+CMU*DEN(IJK)*TE(IJK)**2/
     *           (ED(IJK)+SMALL))*URF(IVIS)
     *           +VISOLD*(1.-URF(IVIS))
C
        RXVISC=MAX(RXVISC,VIS(IJK))
C
      END DO
      END DO
      END DO
C
      END DO
C
C.....UPDATE THE DIMENSIONLESS DISTANCE FROM THE WALL
C
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        CK=CMU25*SQRT(MAX(ZERO,TE(IJP)))
        YPL(IW)=DEN(IJB)*CK*DN(IW)/VISC
        VISCW=YPL(IW)*VISC*CAPPA/LOG(ELOG*YPL(IW)+SMALL)
        VISW(IW)=MAX(VISC,VISCW)
        VIS(IJB)=VISW(IW)
      END DO
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE MODDAT
C########################################################
C     In this routine some constants are assigned values.
C========================================================
C
C NEW
#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscmat.h"
#include "finclude/petscpc.h"
#include "finclude/petscksp.h"
C END NEW
#INCLUDE "param3d.inc"
#INCLUDE "partype3d.inc"
#INCLUDE "parinw3d.inc"
#INCLUDE "indexc3d.inc"
#INCLUDE "rcont3d.inc"
#INCLUDE "model3d.inc"
C     
C.....Parameters for Smagorinsky model, eq. N� 6. 
C
      ISMG=6
      SIGT  =0.9
      CAPPA =0.41
      ELOG  =8.342
C
C.....Equation number for K-e model. 
C
      ITE =7
      IED =8
      IVIS=9
C
C.....CONSTANTS OF STANDARD  K-EPSILON MODEL
C
      SIGTE =1.0
      SIGED =1.3
      CE1   =1.44
      CE2   =1.92
      CMU   =0.09
      CMU25=SQRT(SQRT(CMU))
      CMU75=CMU25**3
      CTRANS=11.63
C
C
      RETURN
      END
C
C
