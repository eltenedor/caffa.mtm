C###################################################################
      SUBROUTINE POST(ICOUNT,RANK)
C###################################################################
C    DESCRIPTION
C
C==============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
#include "param3d.inc"
#include "geo3d.inc"
#include "indexc3d.inc"
#include "var3d.inc"

      CHARACTER(LEN=28) VTKFILE
      INTEGER M,I,J,K,NKMT,NIMT,NJMT,KSTT,ISTT,IJK
      INTEGER ICOUNT,RANK
      PetscErrorCode IERR
#include "petsc.user.inc"
C=========================================================

C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
C
      DO M=1,NBLKS
C
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
C
        write(VTKFILE,'(A8,I4.4,A1,I4.4,A1,I6.6,A4)') 
     *               'res_out_',M,'_',RANK,'_',ICOUNT,'.vtk'
        print *, ' *** GENERATING .VTK *** '
C
        open (UNIT=22,FILE=VTKFILE,POSITION='REWIND')
        write(22,'(A)') '# vtk DataFile Version 3.0'
        write(22,'(A)') 'grid'
        write(22,'(A)') 'ASCII'
        write(22,'(A)') 'DATASET STRUCTURED_GRID'
        write(22,'(A,I6,I6,I6)') 'DIMENSIONS', NIMT,NJMT,NKMT
        write(22,'(A6,I9,A6)') 'Points', NIMT*NJMT*NKMT, ' float'
C
        do K=1,NKMT
        do J=1,NJMT
        do I=1,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            write(22,'(E20.10,1X,E20.10,1X,E20.10)'), 
     *                     X(IJK),Y(IJK),Z(IJK)
        end do
        end do
        end do
C
        write(22,'(A10,1X,I9)') 'CELL_DATA ',(NIMT-1)*(NJMT-1)*(NKMT-1)
        write(22,'(A17)') 'VECTORS UVW float'
C
        do K=2,NKMT
        do J=2,NJMT
        do I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            write(22,'(3F12.8)') U(IJK),V(IJK),W(IJK)
        end do
        end do
        end do
C
        write(22,'(A15)') 'SCALARS P float'
        write(22,'(A20)') 'LOOKUP_TABLE default'
C
        do K=2,NKMT
        do J=2,NJMT
        do I=2,NIMT
            IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
            write(22,'(F12.8)') P(IJK)
        end do
        end do
        end do
        close(UNIT=22)
      end do
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
C
      RETURN
      END 
C
#include "petsc.user.inc"
