C###################################################################
C NEW
C     SUBROUTINE POST(ICOUNT)
      SUBROUTINE POST(ICOUNT,RANK)
C END NEW
C###################################################################
C     This routine prepares and saves data for post-processing, in
C     the format required by the post-processor. Since the post-
C     processor knows nothing about the grid type, variable values
C     at boundary nodes along C- and O-grid cuts must be calculated
C     (they are never used in the calculation). Also, block corner
C     values are never used in the computation but are needed in the
C     post-processor (they are simply set equal to the values at
C     the node next to corner). Also, mass fluxes through boundary 
C     faces need to be calculated and stored in arrays F1
C     and F2, for consistency with post-processor.
C
C
C===================================================================
C NEW
#include "finclude/petsc.h"
C END NEW

#include "param3d.inc"
#include "parinw3d.inc"
#include "geo3d.inc"
#include "logic3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "charac3d.inc"
#include "model3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "tecio.inc"
C
C
      INTEGER I,IJBL,IJBR,IJP,IJN,M,K,J,IJK,IJKP
      INTEGER ICOUNT,IJ,IMAX,JMAX,KMAX,IJKL
C
      REAL*8 FACA,FACN,FACP,FAC
C      
      CHARACTER DSN*3
C     VARIABLES NECESSARY FOR TECPLOT OUTPUT
      character*1 NULLCHR
      Integer   Debug,III,NPts,NElm
C
      real, dimension(:), allocatable :: XH, YH, ZH, PH
      real, dimension(:), allocatable :: UH, VH, WH, UVWH, F1H
      real, dimension(:), allocatable :: F2H, F3H, VISH
      integer :: status
C      
      Real*8    SolTime
      Integer*4 VIsDouble, FileType
      Integer*4 ZoneType,StrandID,ParentZn,IsBlock
      Integer*4 ICellMax,JCellMax,KCellMax,NFConns,FNMode,ShrConn
      POINTER   (NullPtr,Null)
      Integer*4 Null(*)
      character*8 nameblock
C NEW
C     character*17 outfile
      character*22 outfile
C END NEW
C NEW
      INTEGER RANK
      PetscErrorCode IERR
#include "petsc.user.inc"
C END NEW
C=====================================================================
C NEW
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)

      CALL VecGetArray(DENVEC,DENARR,DENNI,IERR)

      IF (LGRAD) THEN
      CALL VecGetArray(DUXVEC,DUXARR,DUXXI,IERR) 
      CALL VecGetArray(DUYVEC,DUYARR,DUYYI,IERR) 
      CALL VecGetArray(DUZVEC,DUZARR,DUZZI,IERR) 
      CALL VecGetArray(DVXVEC,DVXARR,DVXXI,IERR) 
      CALL VecGetArray(DVYVEC,DVYARR,DVYYI,IERR) 
      CALL VecGetArray(DVZVEC,DVZARR,DVZZI,IERR) 
      CALL VecGetArray(DWXVEC,DWXARR,DWXXI,IERR) 
      CALL VecGetArray(DWYVEC,DWYARR,DWYYI,IERR) 
      CALL VecGetArray(DWZVEC,DWZARR,DWZZI,IERR) 
      END IF

      CALL VecGetArray(VISVEC,VISARR,VISSI,IERR)

      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
C END NEW
C
C.....SET 'BOUNDARY VALUES' AT O- AND C-GRID CUTS
C
      DO I=1,NOCBKAL
        IJBL=IJLBBK(I)
        IJBR=IJRBBK(I)
        U(IJBL)=0.
        V(IJBL)=0.
        W(IJBL)=0.
        P(IJBL)=0.
        T(IJBL)=0.
        TE(IJBL)=0.
        ED(IJBL)=0.
        U(IJBR)=0.
        V(IJBR)=0.
        W(IJBR)=0.
        P(IJBR)=0.
        T(IJBR)=0.
        TE(IJBR)=0.
        ED(IJBR)=0.
      END DO
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        IJBL=IJLBBK(I)
        IJBR=IJRBBK(I)
        FACA=SQRT(XOCR(I)**2+YOCR(I)**2+ZOCR(I)**2)/
     *       SQRT(XBCR(I)**2+YBCR(I)**2+ZBCR(I)**2)
        FACN=FACA*FOCBK(I)
        FACP=FACA*(1.-FOCBK(I))
C
        U(IJBL) = U(IJBL)+ U(IJN)*FACN+ U(IJP)*FACP
        V(IJBL) = V(IJBL)+ V(IJN)*FACN+ V(IJP)*FACP
        W(IJBL) = W(IJBL)+ W(IJN)*FACN+ W(IJP)*FACP
        P(IJBL) = P(IJBL)+ P(IJN)*FACN+ P(IJP)*FACP
        T(IJBL) = T(IJBL)+ T(IJN)*FACN+ T(IJP)*FACP
        TE(IJBL)=TE(IJBL)+TE(IJN)*FACN+TE(IJP)*FACP
        ED(IJBL)=ED(IJBL)+ED(IJN)*FACN+ED(IJP)*FACP
C
        U(IJBR) = U(IJBR)+ U(IJN)*FACN+ U(IJP)*FACP
        V(IJBR) = V(IJBR)+ V(IJN)*FACN+ V(IJP)*FACP
        W(IJBR) = W(IJBR)+ W(IJN)*FACN+ W(IJP)*FACP
        P(IJBR) = P(IJBR)+ P(IJN)*FACN+ P(IJP)*FACP
        T(IJBR) = T(IJBR)+ T(IJN)*FACN+ T(IJP)*FACP
        TE(IJBR)=TE(IJBR)+TE(IJN)*FACN+TE(IJP)*FACP
        ED(IJBR)=ED(IJBR)+ED(IJN)*FACN+ED(IJP)*FACP
      END DO
C
C.....CALCULATE WEST AND EAST BOUNDARY MASS FLUXES
C
      DO M=1,NBLKS
      CALL SETIND(M)       
      DO K=2,NKM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(1+IST)+J
        F1(IJK)=-DEN(IJK)*(U(IJK)*XER(IJK)+V(IJK)*YER(IJK)+
     *                     W(IJK)*ZER(IJK))
C
        IJK=LKBK(K+KST)+LIBK(NIM+IST)+J
        F1(IJK)=DEN(IJK+NJ)*(U(IJK+NJ)*XER(IJK)+V(IJK+NJ)*YER(IJK)+
     *                       W(IJK+NJ)*ZER(IJK))
      END DO
      END DO
      END DO
C
C.....CALCULATE SOUTH AND NORTH BOUNDARY MASS FLUXES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
        IJK=LKBK(K+KST)+LIBK(I+IST)+1
        F2(IJK)=-DEN(IJK)*(U(IJK)*XNR(IJK)+V(IJK)*YNR(IJK)+
     *                     W(IJK)*ZNR(IJK))
C
        IJK=LKBK(K+KST)+LIBK(I+IST)+NJM
        F2(IJK)=DEN(IJK+1)*(U(IJK+1)*XNR(IJK)+V(IJK+1)*YNR(IJK)+
     *                      W(IJK+1)*ZNR(IJK))
      END DO
      END DO
      END DO
C
C.....CALCULATE BOTTOM AND TOP BOUNDARY MASS FLUXES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(1+KST)+LIBK(I+IST)+J
        F3(IJK)=-DEN(IJK)*(U(IJK)*XTR(IJK)+V(IJK)*YTR(IJK)+
     *                     W(IJK)*ZTR(IJK))
C
        IJK=LKBK(NKM+KST)+LIBK(I+IST)+J
        F3(IJK)=DEN(IJK+NIJ)*(U(IJK+NIJ)*XTR(IJK)+V(IJK+NIJ)*YTR(IJK)+
     *                        W(IJK+NIJ)*ZTR(IJK))
      END DO
      END DO
      END DO
C
C.....LOOP THROUGH BLOCKS
C
      DO M=1,NBLKS
      CALL SETIND(M)
C
C.....COMPUTE EDGE VALUES
C
      DO K=2,NKM
C.....South-West Edge
        IJKP=LKBK(K+KST)+LIBK(1+IST)+1
        U(IJKP)=U(IJKP+1)
        V(IJKP)=V(IJKP+1)
        W(IJKP)=W(IJKP+1)
        P(IJKP)=P(IJKP+1)
        T(IJKP)=T(IJKP+1)
        VIS(IJKP)=VIS(IJKP+1)
        TE(IJKP)=TE(IJKP+1)
        ED(IJKP)=ED(IJKP+1)
C.....South-East Edge
        IJKP=LKBK(K+KST)+LIBK(NI+IST)+1
        U(IJKP)=U(IJKP+1)
        V(IJKP)=V(IJKP+1)
        W(IJKP)=W(IJKP+1)
        P(IJKP)=P(IJKP+1)
        T(IJKP)=T(IJKP+1)
        VIS(IJKP)=VIS(IJKP+1)
        TE(IJKP)=TE(IJKP+1)
        ED(IJKP)=ED(IJKP+1)
C.....North-West Edge
        IJKP=LKBK(K+KST)+LIBK(1+IST)+NJ
        U(IJKP)=U(IJKP-1)
        V(IJKP)=V(IJKP-1)
        W(IJKP)=W(IJKP-1)
        P(IJKP)=P(IJKP-1)
        T(IJKP)=T(IJKP-1)
        VIS(IJKP)=VIS(IJKP-1)
        TE(IJKP)=TE(IJKP-1)
        ED(IJKP)=ED(IJKP-1)
C.....North-East Edge
        IJKP=LKBK(K+KST)+LIBK(NI+IST)+NJ
        U(IJKP)=U(IJKP-1)
        V(IJKP)=V(IJKP-1)
        W(IJKP)=W(IJKP-1)
        P(IJKP)=P(IJKP-1)
        T(IJKP)=T(IJKP-1)
        VIS(IJKP)=VIS(IJKP-1)
        TE(IJKP)=TE(IJKP-1)
        ED(IJKP)=ED(IJKP-1)
      END DO
C
      DO I=2,NIM
C.....Bottom-South Edge
        IJKP=LKBK(1+KST)+LIBK(I+IST)+1
        U(IJKP)=U(IJKP+1)
        V(IJKP)=V(IJKP+1)
        W(IJKP)=W(IJKP+1)
        P(IJKP)=P(IJKP+1)
        T(IJKP)=T(IJKP+1)
        VIS(IJKP)=VIS(IJKP+1)
        TE(IJKP)=TE(IJKP+1)
        ED(IJKP)=ED(IJKP+1)
C.....Bottom-North Edge
        IJKP=LKBK(1+KST)+LIBK(I+IST)+NJ
        U(IJKP)=U(IJKP-1)
        V(IJKP)=V(IJKP-1)
        W(IJKP)=W(IJKP-1)
        P(IJKP)=P(IJKP-1)
        T(IJKP)=T(IJKP-1)
        VIS(IJKP)=VIS(IJKP-1)
C.....Top-South Edge
        IJKP=LKBK(NK+KST)+LIBK(I+IST)+1
        U(IJKP)=U(IJKP+1)
        V(IJKP)=V(IJKP+1)
        W(IJKP)=W(IJKP+1)
        P(IJKP)=P(IJKP+1)
        T(IJKP)=T(IJKP+1)
        VIS(IJKP)=VIS(IJKP+1)
        TE(IJKP)=TE(IJKP+1)
        ED(IJKP)=ED(IJKP+1)
C.....Top-North Edge
        IJKP=LKBK(NK+KST)+LIBK(I+IST)+NJ
        U(IJKP)=U(IJKP-1)
        V(IJKP)=V(IJKP-1)
        W(IJKP)=W(IJKP-1)
        P(IJKP)=P(IJKP-1)
        T(IJKP)=T(IJKP-1)
        VIS(IJKP)=VIS(IJKP-1)
        TE(IJKP)=TE(IJKP-1)
        ED(IJKP)=ED(IJKP-1)
      END DO
C
      DO J=1,NJ
C.....Bottom-West Edge (and corners)
        IJKP=LKBK(1+KST)+LIBK(1+IST)+J
        U(IJKP)=U(IJKP+NI)
        V(IJKP)=V(IJKP+NI)
        W(IJKP)=W(IJKP+NI)
        P(IJKP)=P(IJKP+NI)
        T(IJKP)=T(IJKP+NI)
        VIS(IJKP)=VIS(IJKP+NI)
        TE(IJKP)=TE(IJKP+NI)
        ED(IJKP)=ED(IJKP+NI)
C.....Bottom-East Edge (and corners)
        IJKP=LKBK(1+KST)+LIBK(NI+IST)+J
        U(IJKP)=U(IJKP-NI)
        V(IJKP)=V(IJKP-NI)
        W(IJKP)=W(IJKP-NI)
        P(IJKP)=P(IJKP-NI)
        T(IJKP)=T(IJKP-NI)
        VIS(IJKP)=VIS(IJKP-NI)
        TE(IJKP)=TE(IJKP-NI)
        ED(IJKP)=ED(IJKP-NI)
C.....Top-West Edge (and corners)
        IJKP=LKBK(NK+KST)+LIBK(1+IST)+J
        U(IJKP)=U(IJKP+NI)
        V(IJKP)=V(IJKP+NI)
        W(IJKP)=W(IJKP+NI)
        P(IJKP)=P(IJKP+NI)
        T(IJKP)=T(IJKP+NI)
        VIS(IJKP)=VIS(IJKP+NI)
        TE(IJKP)=TE(IJKP+NI)
        ED(IJKP)=ED(IJKP+NI)
C.....Top-East Edge (and corners)
        IJKP=LKBK(NK+KST)+LIBK(NI+IST)+J
        U(IJKP)=U(IJKP-NI)
        V(IJKP)=V(IJKP-NI)
        W(IJKP)=W(IJKP-NI)
        P(IJKP)=P(IJKP-NI)
        T(IJKP)=T(IJKP-NI)
        VIS(IJKP)=VIS(IJKP-NI)
        TE(IJKP)=TE(IJKP-NI)
        ED(IJKP)=ED(IJKP-NI)
      END DO
C
C.....CLOSE LOOP THROUGH BLOCKS
C
      END DO
C
C.....WRITE SOLUTION ON A FILE FOR POST-PROCESSING
C
      IF(ICOUNT.LT.10) WRITE(DSN,'(I1,2H  )') ICOUNT
      IF(ICOUNT.GE.10.AND.ICOUNT.LT.100) WRITE(DSN,'(I2,1H )') ICOUNT
      IF(ICOUNT.GE.100) WRITE(DSN,'(I3)') ICOUNT
C NEW
C     WRITE(FILPOS,'(A7,1H.,A3)') NAME,DSN
      WRITE(FILPOS,'(A7,I4.4,1H.,A3)') NAME,RANK,DSN
C END NEW
C
C.....Here changed to write binary files 
C
C NEW
C     OPEN (UNIT=8,FILE=FILPOS,FORM='binary')
      OPEN (UNIT=8,FILE=FILPOS,FORM='UNFORMATTED',POSITION='REWIND')
C     REWIND 8
C END NEW
C
      WRITE(8) ITIM,TIME,NBLKS,NIJKBKAL
      WRITE(8) (NIBK(I), I=1,NBLKS),(NJBK(I),I=1,NBLKS)
      WRITE(8) (NKBK(I), I=1,NBLKS),(IBK(I) ,I=1,NBLKS)
      WRITE(8) (JBK(I),  I=1,NBLKS),(KBK(I) ,I=1,NBLKS)
      WRITE(8) (IJKBK(I),I=1,NBLKS),(NIJKBK(I),I=1,NBLKS)
      WRITE(8) (X(IJ), IJ=1,NIJKBKAL),(Y(IJ), IJ=1,NIJKBKAL)
      WRITE(8) (Z(IJ), IJ=1,NIJKBKAL),(XC(IJ),IJ=1,NIJKBKAL)
      WRITE(8) (YC(IJ),IJ=1,NIJKBKAL),(ZC(IJ),IJ=1,NIJKBKAL)
      WRITE(8) (F1(IJ),IJ=1,NIJKBKAL),(F2(IJ),IJ=1,NIJKBKAL)
      WRITE(8) (F3(IJ),IJ=1,NIJKBKAL),(U(IJ), IJ=1,NIJKBKAL)
      WRITE(8) (V(IJ), IJ=1,NIJKBKAL),(W(IJ), IJ=1,NIJKBKAL)
      WRITE(8) (P(IJ), IJ=1,NIJKBKAL),(T(IJ), IJ=1,NIJKBKAL)
      WRITE(8) (VIS(IJ),IJ=1,NIJKBKAL)
      WRITE(8) (TE(IJ), IJ=1,NIJKBKAL),(ED(IJ),IJ=1,NIJKBKAL)
      WRITE(8) (RESMON(IJ),IJ=1,NIJKBKAL)
C     
C.....WRITE OUT VELOCITY GRADIENTS ALSO ?
C
      IF(LGRAD) THEN
         WRITE(8) (DUX(IJ),IJ=1,NIJKBKAL),(DUY(IJ),IJ=1,NIJKBKAL)
         WRITE(8) (DUZ(IJ),IJ=1,NIJKBKAL),(DVX(IJ),IJ=1,NIJKBKAL)
         WRITE(8) (DVY(IJ),IJ=1,NIJKBKAL),(DVZ(IJ),IJ=1,NIJKBKAL)
         WRITE(8) (DWX(IJ),IJ=1,NIJKBKAL),(DWY(IJ),IJ=1,NIJKBKAL)
         WRITE(8) (DWZ(IJ),IJ=1,NIJKBKAL)
      ENDIF
C
      CLOSE(UNIT=8)

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  FALK WRITE TECPLOT RESULT-FILE START
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

      NULLCHR = CHAR(0)
      NullPtr = 0
      Debug   = 1
      FileType = 0
      VIsDouble = 0
      ZoneType = 0
      SolTime = ICOUNT
      StrandID = 0
      ParentZn = 0
      IsBlock = 1
      ICellMax = 0
      JCellMax = 0
      KCellMax = 0
      NFConns = 0
      FNMode = 0
      ShrConn = 0
C
C NEW
C     write(outfile, 110) 'res_out',ICOUNT,'.plt'
      write(outfile, 110) 'res_out',RANK,'_',ICOUNT,'.plt'
C END NEW
C
      write(*,*)'Outfile:',outfile
C
      I = TecIni112('IJK Ordered Zones'//NULLCHR,
     &     'X Y Z U V W UVW P F1 F2 F3 VIS'//NULLCHR,
     &     outfile//NULLCHR,
     &     '.'//NULLCHR,
     &     FileType,
     &     Debug,
     &     VIsDouble)
C
      DO M=1,NBLKS
C              
         CALL SETIND(M)
         allocate(XH(NI*NJ*NK), stat=status)
         allocate(YH(NI*NJ*NK), stat=status)
         allocate(ZH(NI*NJ*NK), stat=status)
         allocate(UH(NI*NJ*NK), stat=status)
         allocate(VH(NI*NJ*NK), stat=status)
         allocate(WH(NI*NJ*NK), stat=status)
         allocate(UVWH(NI*NJ*NK), stat=status)
         allocate(PH(NI*NJ*NK), stat=status)
         allocate(F1H(NI*NJ*NK), stat=status)
         allocate(F2H(NI*NJ*NK), stat=status)
         allocate(F3H(NI*NJ*NK), stat=status)
         allocate(VISH(NI*NJ*NK), stat=status)
C     
         IMax    = NI
         JMax    = NJ
         KMax    = NK
C         
         DO J=1,NJ
            DO I=1,NI
               DO K=1,NK
                  IJK=LKBK(K+KST)+LIBK(I+IST)+J
                  IJKL=(K-1)*(NI*NJ)+(J-1)*NI+I
                  XH(IJKL)=XC(IJK)
                  YH(IJKL)=YC(IJK)
                  ZH(IJKL)=ZC(IJK)
                  UH(IJKL)=U(IJK)
                  VH(IJKL)=V(IJK)
                  WH(IJKL)=W(IJK)
                  UVWH(IJKL)=sqrt(U(IJK)**2+V(IJK)**2+W(IJK)**2)
                  PH(IJKL)=P(IJK)
                  F1H(IJKL)=F1(IJK)
                  F2H(IJKL)=F2(IJK)
                  F3H(IJKL)=F3(IJK)
                  VISH(IJKL)=VIS(IJK)                  
               ENDDO
            ENDDO
         ENDDO
C
         write(nameblock, 100) 'Block',M
C 
         I = TecZne112(nameblock//NULLCHR,
     &        ZoneType,
     &        IMax,
     &        JMax,
     &        KMax,
     &        ICellMax,
     &        JCellMax,
     &        KCellMax,
     &        SolTime,
     &        StrandID,
     &        ParentZn,
     &        IsBlock,
     &        NFConns,
     &        FNMode,
     &        0,
     &        0,
     &        0,
     &        Null,
     &        Null,
     &        Null,
     &        ShrConn)
 
         III = IMax*JMax*KMAX
         I   = TecDat112(III,XH,0)
         I   = TecDat112(III,YH,0)
         I   = TecDat112(III,ZH,0)
         I   = TecDat112(III,UH,0)
         I   = TecDat112(III,VH,0)
         I   = TecDat112(III,WH,0)
         I   = TecDat112(III,UVWH,0)
         I   = TecDat112(III,PH,0)
         I   = TecDat112(III,F1H,0)
         I   = TecDat112(III,F2H,0)
         I   = TecDat112(III,F3H,0)
         I   = TecDat112(III,VISH,0)
C
         deallocate(XH, stat=status)
         deallocate(YH, stat=status)
         deallocate(ZH, stat=status)
         deallocate(UH, stat=status)
         deallocate(VH, stat=status)
         deallocate(WH, stat=status)
         deallocate(UVWH, stat=status)
         deallocate(PH, stat=status)
         deallocate(F1H, stat=status)
         deallocate(F2H, stat=status)
         deallocate(F3H, stat=status)
         deallocate(VISH, stat=status)
      ENDDO
      I = TecEnd112()

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  FALK WRITE TECPLOT RESULT-FILE END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  FALK WRITE TECPLOT GRID-FILE START
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
      NULLCHR = CHAR(0)
      NullPtr = 0
      Debug   = 1
      FileType = 0
      VIsDouble = 0
      ZoneType = 0
      SolTime = ICOUNT
      StrandID = 0
      ParentZn = 0
      IsBlock = 1
      ICellMax = 0
      JCellMax = 0
      KCellMax = 0
      NFConns = 0
      FNMode = 0
      ShrConn = 0
C
C NEW
C     write(outfile, 110) 'grd_out',ICOUNT,'.plt'
      write(outfile, 110) 'grd_out',RANK,'_',ICOUNT,'.plt'
C END NEW
C
      write(*,*)'Outfile:',outfile
C
      I = TecIni112('IJK Ordered Zones'//NULLCHR,
     &     'X Y Z'//NULLCHR,
     &     outfile//NULLCHR,
     &     '.'//NULLCHR,
     &     FileType,
     &     Debug,
     &     VIsDouble)
C
      DO M=1,NBLKS
C              
         CALL SETIND(M)
         allocate(XH(NIM*NJM*NKM), stat=status)
         allocate(YH(NIM*NJM*NKM), stat=status)
         allocate(ZH(NIM*NJM*NKM), stat=status)
C     
         IMax    = NIM
         JMax    = NJM
         KMax    = NKM
C         
         DO J=1,NJM
            DO I=1,NIM
               DO K=1,NKM
                  IJK=LKBK(K+KST)+LIBK(I+IST)+J
                  IJKL=(K-1)*(NIM*NJM)+(J-1)*NIM+I
                  XH(IJKL)=X(IJK)
                  YH(IJKL)=Y(IJK)
                  ZH(IJKL)=Z(IJK)
               ENDDO
            ENDDO
         ENDDO
C
         write(nameblock, 100) 'Block',M     
         I = TecZne112(nameblock//NULLCHR,
     &        ZoneType,
     &        IMax,
     &        JMax,
     &        KMax,
     &        ICellMax,
     &        JCellMax,
     &        KCellMax,
     &        SolTime,
     &        StrandID,
     &        ParentZn,
     &        IsBlock,
     &        NFConns,
     &        FNMode,
     &        0,
     &        0,
     &        0,
     &        Null,
     &        Null,
     &        Null,
     &        ShrConn)
C
         III = IMax*JMax*KMAX
         I   = TecDat112(III,XH,0)
         I   = TecDat112(III,YH,0)
         I   = TecDat112(III,ZH,0)
C     
         deallocate(XH, stat=status)
         deallocate(YH, stat=status)
         deallocate(ZH, stat=status)
C       
      ENDDO
      I = TecEnd112()
C NEW
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)

      CALL VecRestoreArray(DENVEC,DENARR,DENNI,IERR)

      IF (LGRAD) THEN
      CALL VecRestoreArray(DUXVEC,DUXARR,DUXXI,IERR) 
      CALL VecRestoreArray(DUYVEC,DUYARR,DUYYI,IERR) 
      CALL VecRestoreArray(DUZVEC,DUZARR,DUZZI,IERR) 
      CALL VecRestoreArray(DVXVEC,DVXARR,DVXXI,IERR) 
      CALL VecRestoreArray(DVYVEC,DVYARR,DVYYI,IERR) 
      CALL VecRestoreArray(DVZVEC,DVZARR,DVZZI,IERR) 
      CALL VecRestoreArray(DWXVEC,DWXARR,DWXXI,IERR) 
      CALL VecRestoreArray(DWYVEC,DWYARR,DWYYI,IERR) 
      CALL VecRestoreArray(DWZVEC,DWZARR,DWZZI,IERR) 
      END IF

      CALL VecRestoreArray(VISVEC,VISARR,VISSI,IERR)

      CALL VecRestoreArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCVEC,ZCARR,ZCCI,IERR)
C END NEW
C END NEW

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  FALK WRITE TECPLOT OUTPUT END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
C
C.....RESET WEST AND EAST BOUNDARY MASS FLUXES TO ZERO
C
c$$$      DO M=1,NBLKS
c$$$      CALL SETIND(M)       
c$$$      DO K=2,NKM
c$$$      DO J=2,NJM
c$$$        F1(LKBK(K+KST)+LIBK(1+IST)+J)=0.
c$$$        F1(LKBK(K+KST)+LIBK(NIM+IST)+J)=0.
c$$$      END DO
c$$$      END DO
c$$$      END DO
c$$$C
c$$$C.....RESET SOUTH AND NORTH BOUNDARY MASS FLUXES TO ZERO
c$$$C
c$$$      DO M=1,NBLKS
c$$$      CALL SETIND(M)       
c$$$      DO K=2,NKM
c$$$      DO I=2,NIM
c$$$        F2(LKBK(K+KST)+LIBK(I+IST)+1)=0.
c$$$        F2(LKBK(K+KST)+LIBK(I+IST)+NJM)=0.
c$$$      END DO
c$$$      END DO
c$$$      END DO
c$$$C
c$$$C.....RESET BOTTOM AND TOP BOUNDARY MASS FLUXES TO ZERO
c$$$C
c$$$      DO M=1,NBLKS
c$$$      CALL SETIND(M)       
c$$$      DO I=2,NIM
c$$$      DO J=2,NJM
c$$$        F3(LKBK(1+KST)+LIBK(I+IST)+J)=0.
c$$$        F3(LKBK(NKM+KST)+LIBK(I+IST)+J)=0.
c$$$      END DO
c$$$      END DO
c$$$      END DO
C
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  FORMAT STUFF
 100  format (A5,i3.3) 
C NEW
C110  format (A7,i6.6,A4) 
 110  format (A7,I4.4,A1,i6.6,A4) 
C END NEW
C
C 
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C 
C
