C############################################################
      SUBROUTINE SOLVER(FI,IFI)
C############################################################
C     This routine incorporates the Stone's SIP solver, based
C     on ILU-decomposition. See Sect. 5.3.4 for details.
C
C     A 3D version of SIPSOL has been incorporated here
C     obtained from 3D cartesian code in /3dc/prog.f
C
C     This version has been adapted for use with block-
C     structured grids.
C
C     This version has been anotated with OpenMP directives
C     for multithreaded execution. Each thread receives
C     complete grid blocks.
C============================================================
#INCLUDE "param3d.inc"
#INCLUDE "partype3d.inc"
#INCLUDE "parinw3d.inc"
#INCLUDE "indexc3d.inc"
#INCLUDE "logic3d.inc"
#INCLUDE "rcont3d.inc"
#INCLUDE "coef3d.inc"
#INCLUDE "bound3d.inc"
#INCLUDE "bcinw3d.inc"
C
      DIMENSION BB(NXYZA),BW(NXYZA),BS(NXYZA),BP(NXYZA),BN(NXYZA),
     *          BE(NXYZA),BT(NXYZA),RES(NXYZA),FI(NXYZA)
C  
C
C.....INITIALIZE LU COEFICIENTS AND RESIDUALS
C
      BE=0.
      BN=0.
      BT=0.
      RES=0.
C
c.....OpenMP : Start parallel loop section
c$OMP PARALLEL DO DEFAULT(SHARED) 
c$OMP*  PRIVATE(M,K,LKK,I,LKI,IJK,IMJK,IJKM,IJMK,NKMT,
c$OMP*  NIMT,NJMT,KSTT,ISTT,NJT,NIJT,P1,P2,P3,ALFAT)
C
      DO M=1,NBLKS
C
C.....CALCULATE COEFFICIENTS OF  L  AND  U  MATRICES
C
      NKMT=NKBK(M)-1
      NIMT=NIBK(M)-1
      NJMT=NJBK(M)-1
      KSTT=KBK(M)
      ISTT=IBK(M)
      NJT=NJMT+1
      NIJT=(NIMT+1)*NJT
      ALFAT=ALFA
C
      DO K=2,NKMT
      LKK=LKBK(K+KSTT)
      DO I=2,NIMT 
      LKI=LKK+LIBK(I+ISTT)
      DO IJK=LKI+2,LKI+NJMT
        IMJK=IJK-NJT
        IJKM=IJK-NIJT
        IJMK=IJK-1
        BB(IJK)=+AB(IJK)/(1.+ALFAT*(BN(IJKM)+BE(IJKM)))
        BW(IJK)=+AW(IJK)/(1.+ALFAT*(BN(IMJK)+BT(IMJK)))
        BS(IJK)=+AS(IJK)/(1.+ALFAT*(BE(IJMK)+BT(IJMK)))
        P1=ALFAT*(BB(IJK)*BN(IJKM)+BW(IJK)*BN(IMJK))
        P2=ALFAT*(BB(IJK)*BE(IJKM)+BS(IJK)*BE(IJMK))
        P3=ALFAT*(BW(IJK)*BT(IMJK)+BS(IJK)*BT(IJMK))
        BP(IJK)=1./(AP(IJK)+P1+P2+P3-BB(IJK)*BT(IJKM)-BW(IJK)*BE(IMJK)
     *         -BS(IJK)*BN(IJMK)+SMALL)
        BN(IJK)=(+AN(IJK)-P1)*BP(IJK)
        BE(IJK)=(+AE(IJK)-P2)*BP(IJK)
        BT(IJK)=(+AT(IJK)-P3)*BP(IJK)
      END DO
      END DO
      END DO
C
      END DO
c.....OpenMP : Here ends this parallel loop section
C
C
C.....INNER ITERATIONS LOOP
C
      DO N=1,NSW(IFI)
        RES1=0.0
C
c.....OpenMP : Start parallel section
c$OMP   PARALLEL DEFAULT(SHARED)
C
c.....OpenMP : Start a parallel loop
c$OMP   DO PRIVATE(M,K,LKK,I,LKI,IJK,NKMT,NIMT,NJMT,
c$OMP*   KSTT,ISTT,NJT,NIJT)
C
        DO M=1,NBLKS
C
C.....COMPUTE RESIDUAL VECTOR, SUM OF RESIDUALS AND AUXILIARY VECTOR
C
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
        NJT=NJMT+1
        NIJT=(NIMT+1)*NJT
C
        DO K=2,NKMT
        LKK=LKBK(K+KSTT)
        DO I=2,NIMT 
        LKI=LKK+LIBK(I+ISTT)
        JP=LKI+2
        JPL=LKI+NJMT
        RES(JP:JPL)=SU(JP:JPL)-AP(JP:JPL)*FI(JP:JPL)
     *             -AE(JP:JPL)*FI(JP+NJT :JPL+NJT )
     *             -AW(JP:JPL)*FI(JP-NJT :JPL-NJT )
     *             -AN(JP:JPL)*FI(JP+1   :JPL+1   )
     *             -AS(JP:JPL)*FI(JP-1   :JPL-1   )
     *             -AT(JP:JPL)*FI(JP+NIJT:JPL+NIJT)
     *             -AB(JP:JPL)*FI(JP-NIJT:JPL-NIJT)
        END DO
        END DO
C
        END DO
c.....OpenMP : Here ends this parallel loop
C
C.....CONTRIBUTION FROM OC-CUTS TO RESIDUALS
C
c$OMP   SECTIONS
        DO I=1,NOCBKAL
          RES(IJLPBK(I))=RES(IJLPBK(I))-AR(I)*FI(IJRPBK(I))
        END DO
c$OMP   SECTION
        DO I=NOCBKAL,1,-1
          RES(IJRPBK(I))=RES(IJRPBK(I))-AL(I)*FI(IJLPBK(I))
        END DO
C
c.....OpenMP : Here ends this 'sections' section
c$OMP   END SECTIONS
c
c.....OpenMP : Start a single section
c$OMP   SINGLE
C
C.....IF TESTING COPY OUT RESIDUALS
C
        IF(LTEST.AND.IFI.EQ.1) THEN
          RESMON=RES
        ENDIF
C
c.....OpenMP : Here ends this single section
c$OMP   END SINGLE
C
c.....OpenMP : Start parallel do
c$OMP   DO PRIVATE(M,K,LKK,I,LKI,IJK,NKMT,NIMT,NJMT,
c$OMP*   KSTT,ISTT,NJT,NIJT) REDUCTION(+:RES1)
C
        DO M=1,NBLKS
C
C.....START LOOP THROUGH BLOCKS
C
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
        NJT=NJMT+1
        NIJT=(NIMT+1)*NJT
C
C.....COMPUTE SUM OF RESIDUALS AND AUXILIARY VECTOR
C
        DO K=2,NKMT
        LKK=LKBK(K+KSTT)
        DO I=2,NIMT 
        LKI=LKK+LIBK(I+ISTT)
        DO IJK=LKI+2,LKI+NJMT
          RES1=RES1+ABS(RES(IJK))
          RES(IJK)=(RES(IJK)-BB(IJK)*RES(IJK-NIJT)
     *                      -BW(IJK)*RES(IJK-NJT)
     *                      -BS(IJK)*RES(IJK-1))*BP(IJK)
        END DO
        END DO
        END DO
C
C.....COMPUTE INCREMENT AND UPDATE VARIABLES (BACKWARD SUBSTITUTION)
C
        DO K=NKMT,2,-1
        LKK=LKBK(K+KSTT)
        DO I=NIMT,2,-1
        LKI=LKK+LIBK(I+ISTT)
        DO IJK=LKI+NJMT,LKI+2,-1
          RES(IJK)=RES(IJK)-BN(IJK)*RES(IJK+1)
     *                     -BE(IJK)*RES(IJK+NJT)
     *                     -BT(IJK)*RES(IJK+NIJT)
          FI(IJK)=FI(IJK)+RES(IJK)
        END DO
        END DO
        END DO
C
        END DO
c.....OpenMP : Here ends this parallel loop
c.....OpenMP : Here ends this parallel section
c$OMP   END PARALLEL
C
C.....CHECK CONVERGENCE OF INNER ITERATIONS
C
        IF(N.EQ.1) RESOR(IFI)=RES1
        RSM=RES1/(RESOR(IFI)+SMALL)
C
        IF(LTEST) WRITE(6,600) IFI,N,RES1
  600   FORMAT(20X,'FI=',I2,'  SWEEP=',I3,'  RES=',1PE10.3)
        IF(RSM.LT.SOR(IFI)) RETURN
      END DO
C
      RETURN
      END
C
C
