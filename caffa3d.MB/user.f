C#########################################################
      SUBROUTINE BCIN
C#########################################################
C     This routine sets inlet boundary conditions (which 
C     may also change in time in an unsteady problem; in
C     that case, set the logical variable INIBC to 'true'.)
C     It is called once at the begining of calculations on 
C     each grid level, unles INIBC=.TRUE. forces calls in
C     each time step.
C     This routine includes several options which are most
C     often required and serves the test cases provided with
C     the code; it may require adaptation to the problem
C     being solved, especially regarding velocity or
C     temperature distribution at boundaries.
C=========================================================
      IMPLICIT NONE
C
C NEW
#include "finclude/petsc.h"
C END NEW
c
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "var3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "logic3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
c
      INTEGER IW,IJB,L,IJP,II,IJK1,IJK2,IJK3,IJK4
c
      REAL*8 FLOWO,FLOWEN,DX12,DY12,DZ12,DX13,DY13,DZ13
      REAL*8 DX14,DY14,DZ14,S23,S34,FLOMON,XR23,YR23,ZR23
      REAL*8 XR34,YR34,ZR34,XNV,YNV,ZNV
      REAL*8  LPI, FLOMASH,TCA,TCD,SUMAREA
      PARAMETER (LPI=3.141592653589793238462643383279d0)
C
C NEW
      REAL*8 SUMAREAG
      PetscErrorCode IERR
#include "petsc.user.inc"
C END NEW
C=========================================================
C
C.....SET INDICES, INITIALIZE MOMENTUM AND MASS FLOW RATE
C
      INIBC=.TRUE.
      FLOMOM=0.D0
      FLOMAS=0.D0
      FLOWO=0.D0
      FLOWEN=0.D0      
C
C     WALL BOUNDARY CONDITIONS
C=========================================================
C
C NEW
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
C END NEW
      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        U(IJB)=0.D0 
C NEW
        IF (ZC(IJB).GT.0.9D0) U(IJB)=1.0D0
C END NEW
        V(IJB)=0.D0
        W(IJB)=0.D0
      END DO

      DO IW=1,NWALBKAL
        IJB=IJW(IW)
        IJP=IJPW(IW)
        ARE=SQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
C
C.....NORMAL DISTANBE FROM CELL FACE CENTER TO CELL CENTER
C
        DN(IW)=((XC(IJB)-XC(IJP))*XNW(IW)+
     *          (YC(IJB)-XC(IJP))*YNW(IW)+
     *          (ZC(IJB)-ZC(IJP))*ZNW(IW))/(ARE+SMALL)
C
      END DO


C     INLET  BOUNDARY CONDITIONS
C=========================================================
C
      TCA=LPI*0.25D0
      TCD=LPI*0.5D0
      SUMAREA=0.D0
      DO II=1,NINLBKAL
         IJB=IJI(II)
C
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  SPECIFICATION OF INLET VELOCITY
C%%%  SIGN ACCORDING TO FACE IS AUTOMATICALLY SET! 
C%%%  U POSITIV ON WEST SIDE RESULTS IN NEGATIVE MASS FLUX
C%%%  ON THIS SIDE!
#ifdef USE_ANALYTICAL
           U(IJB)=0.5d0*DSIN(LPI*XC(IJB))*DCOS(LPI*YC(IJB))
     &          *DCOS(LPI*ZC(IJB)) 
           V(IJB)=0.5d0*DSIN(LPI*YC(IJB))*DCOS(LPI*XC(IJB))
     &          *DCOS(LPI*ZC(IJB))
           W(IJB)=-DSIN(LPI*ZC(IJB))*DCOS(LPI*XC(IJB))
     &          *DCOS(LPI*YC(IJB)) 
#else
         U(IJB)=4.D0*0.3D0*YC(IJB)*(0.41D0-YC(IJB))/(0.41D0**2)
         V(IJB)=0.D0
         W(IJB)=0.D0
#endif
C         
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  CALCULATION OF MASS FLUX

C%%%  CALCULATION OF FACE AREA
         IJK1=IJI1(II)
         IJK2=IJI2(II)
         IJK3=IJI3(II)
         IJK4=IJI4(II)
C
         DX12=X(IJK2)-X(IJK1)
         DY12=Y(IJK2)-Y(IJK1)
         DZ12=Z(IJK2)-Z(IJK1)
C
         DX13=X(IJK3)-X(IJK1)
         DY13=Y(IJK3)-Y(IJK1)
         DZ13=Z(IJK3)-Z(IJK1)
C     
         DX14=X(IJK4)-X(IJK1)
         DY14=Y(IJK4)-Y(IJK1)
         DZ14=Z(IJK4)-Z(IJK1)

         XR23=DY12*DZ13-DZ12*DY13
         YR23=DZ12*DX13-DX12*DZ13
         ZR23=DX12*DY13-DY12*DX13

         XR34=DY13*DZ14-DZ13*DY14
         YR34=DZ13*DX14-DX13*DZ14
         ZR34=DX13*DY14-DY13*DX14

         S23=SQRT(XR23**2+YR23**2+ZR23**2)
         S34=SQRT(XR34**2+YR34**2+ZR34**2)  
         
         XNV=(XR23+XR34)/(S23+S34)
         YNV=(YR23+YR34)/(S23+S34)
         ZNV=(ZR23+ZR34)/(S23+S34)
              
C%%%  CALCULATION OF MASS FLUX        
         FMI(II)=DENS*(U(IJB)*(S23+S34)/2.0D0*XNV+
     *        V(IJB)*(S23+S34)/2.0D0*YNV+W(IJB)*(S23+S34)/2.0D0*ZNV)
C#ifdef USE_ANALYTICAL
C         SUMAREA=SUMAREA+FMI(II)
C          FLOMASH=MAX(0.d0,FMI(II))
C          FLOMAS=FLOMAS+FLOMASH
C#else
         FLOMASH=ABS(FMI(II))
         FLOMAS=FLOMAS-FMI(II)
C#endif
         FLOMON=FLOMON+FLOMASH*DSQRT(U(IJB)**2+V(IJB)**2+W(IJB)**2)   
      ENDDO
C NEW
      SUMAREAG=0.0D0
      CALL MPI_REDUCE(SUMAREA, SUMAREAG, 1, MPI_REAL8, MPI_SUM,0,
     *     PETSC_COMM_WORLD, IERR)
C     write(*,*)'SUMAREA',SUMAREA
C     write(*,*)'SUMAREA',SUMAREAG
C END NEW
C     write(*,*)'DENS',DENS
C NEW
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
C
C END NEW
C
C.....SET RESIDUAL NORMALISATION FACTORS
C
C NEW
#ifdef USE_SIPSOL
      DO L=1,NFI
        RNOR(L)=1.0D0
        RESOR(L)=0.0D0
      END DO
#endif
C END NEW
C
      IF(FLOMAS.LT.SMALL) FLOMAS=1.0D0
      IF(FLOMOM.LT.SMALL) FLOMOM=1.0D0
C
C
C      FLOMOM=1.
C      FLOMAS=1.
      FLOWEN=1.0D0
C
      RNOR(IU) =1.0D0/(FLOMOM+SMALL)
      RNOR(IV) =RNOR(IU)
      RNOR(IWW) =RNOR(IU)
      RNOR(IP) =1.0D0/(FLOMAS+SMALL)
c      RNOR(IEN)=1./(FLOWEN+SMALL)
      RNOR(IEN)=1.D0
C
     
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C#########################################################
      SUBROUTINE INITCOND
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "var3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "logic3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
C=========================================================
C
C
      IJRBBKO=IJRBBK
      IJRPBKO=IJRPBK
      XBCC=XOCC
      YBCC=YOCC
      ZBCC=ZOCC
      XBCR=XOCR
      YBCR=YOCR
      ZBCR=ZOCR
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE SOURCEUVW
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
c
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "var3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "logic3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
C=========================================================
C
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE SOURCEM
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "var3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "logic3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
C=========================================================
C
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE SOURCESC(IFI,FI)
C#########################################################
C     Special intial conditions.                      
C=========================================================
      implicit none
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "var3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "logic3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
C
      INTEGER IFI
C
C NEW
C      REAL*8 FI(NXYZA)
       REAL*8 FI(    1)
C END NEW
C=========================================================
C
      RETURN
      END
C
C
