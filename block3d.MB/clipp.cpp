#include <vector>
#include <math.h>
#include <omp.h>
#include <iostream>
#include <cfloat>
#include <math.h>

#include "clipper.hpp"
using namespace ClipperLib;

#include "clipp.h"

/*
 * This function performs a 3 dimensional polygon clipping, to match boundary faces.
 * It is based on the 2 dimensional polygon clipper library clipper. To match 3 dimensional
 * boundary faces one needs to perform 3 projections onto the coordinate planes of the cartesian
 * coordinate system. This way the function manages to successively calculate the 3 components of the
 * boundary face vertices. Each projection (if successful i.e. the polygon intersection exists) may
 * contribute 2 components of the vertices.
 */
//void matchfaces_(double XYZL[], double XYZR[],
//                 double XYZCOM[], double &AR, double XYZFC[])
void matchfaces_(double XYZL[], double XYZR[], double &AR, double XYZFC[])
{
  std::vector<long64> XYZLINT(12),XYRLINT(12);
  Polygon p1(4),p2(4);
  Polygons pxy(1),pxz(1),pyz(1);

// check normal vector of polygons (see calcface in caffa)
  std::vector<double> np1(3),np2(3),dx12p1(3),dx14p1(3),dx12p2(3),dx14p2(3);
  std::vector<long64> xyzli64(12),xyzri64(12);
  double anp1,anp2;
  int i,ip,orientation;
  double XYZCOM[12];
  double ARX,ARY,ARZ;

/*
 * check if orientation of adjacent faces matches via the dimension of the respective matrix nullspace
*/
  AR=-1.0;
  if (!checkorientation(XYZL,XYZR)) return;

/*
 * initialization with the vertices of the left boundary face ensures that if the boundary is parallel to a
 * coordinate plane the respective coordinates of the direction perpendicular to the coordinate plane will be
 * saved in the output array. E.g. if face 0 1 2 3 is parallel to the yz plane, the x coordinates of the vector
 * xyzl will be saved in xyzcom an remain untouched on the progress of the function matchfaces_
*/
  for (i = 0; i < 12; i++)
  {
    XYZCOM[i]=XYZL[i];
  }

// initialize center coordinates with vertex coordinates (in case the face is parallel to a coordinate plane)
  for (i = 0; i<3; i++)
  {
    XYZFC[i]=XYZL[i];
  }

// conversion of coordinates to integer
  for (int i = 0; i < 4; i++)
  {
    xyzli64[i*3+0]=long64(XYZL[i*3+0]*10E16);
    xyzli64[i*3+1]=long64(XYZL[i*3+1]*10E16);
    xyzli64[i*3+2]=long64(XYZL[i*3+2]*10E16);

    xyzri64[i*3+0]=long64(XYZR[i*3+0]*10E16);
    xyzri64[i*3+1]=long64(XYZR[i*3+1]*10E16);
    xyzri64[i*3+2]=long64(XYZR[i*3+2]*10E16);
  }

// projection onto xy axis get x and y coordinates
  Clipper clprxy;
  for (i = 0; i < 4; i++)
  {
    p1[i]=IntPoint(xyzli64[i*3],xyzli64[i*3+1]);
    p2[i]=IntPoint(xyzri64[i*3],xyzri64[i*3+1]);
  }
  clprxy.AddPolygon(p1,ptSubject);
  clprxy.AddPolygon(p2,ptClip);
  clprxy.Execute(ctIntersection,pxy,pftEvenOdd,pftEvenOdd);
  if (pxy[0].size())
  {
    getPolyOrientation(pxy[0],ip,orientation);
    if (AR < 0.0) AR=0.0;
    ARZ=double(Area(pxy[0]))/10E33;
    AR+=pow(ARZ,2.0);
    getCenter(pxy[0],XYZFC[0],XYZFC[1],ARZ);
    for (i = 0; i < 4; i++,ip+=orientation)
    {
      if (ip==4 && orientation==1)
      {
        ip = 0;
      }
      else if (ip==-1 && orientation==-1)
      {
        ip = 3;
      }

      XYZCOM[(i*3)+0]= double(pxy[0][ip].X/10E16);
      XYZCOM[(i*3)+1]= double(pxy[0][ip].Y/10E16);
    }
  }

// projection onto xz axis get x and z coordinates
  Clipper clprxz;
  for (i = 0; i < 4; i++)
  {
    p1[i]=IntPoint(xyzli64[i*3],xyzli64[i*3+2]);
    p2[i]=IntPoint(xyzri64[i*3],xyzri64[i*3+2]);
  }
  clprxz.AddPolygon(p1,ptSubject);
  clprxz.AddPolygon(p2,ptClip);
  clprxz.Execute(ctIntersection,pxz,pftEvenOdd,pftEvenOdd);
  if (pxz[0].size())
  {
    getPolyOrientation(pxz[0],ip,orientation);
    orientation*=-1;
    if (AR < 0.0) AR=0.0;
    ARY=double(Area(pxz[0]))/10E33;
    AR+=pow(ARY,2.0);
    getCenter(pxz[0],XYZFC[0],XYZFC[2],ARY);
    for (i = 0; i < 4; i++,ip+=orientation)
    {
      XYZCOM[(i*3)+2]= double(pxz[0][ip].Y/10E16);
      if (ip==4 && orientation==1)
      {
        ip = 0;
      }
      else if (ip==-1 && orientation==-1)
      {
        ip = 3;
      }

      XYZCOM[(i*3)+0]= double(pxz[0][ip].X/10E16);
      XYZCOM[(i*3)+2]= double(pxz[0][ip].Y/10E16);
    }
  }

  // projection onto yz axis get y and z coordinates
  Clipper clpryz;
  for (i = 0; i < 4; i++)
  {
    p1[i]=IntPoint(xyzli64[i*3+1],xyzli64[i*3+2]);
    p2[i]=IntPoint(xyzri64[i*3+1],xyzri64[i*3+2]);
  }
  clpryz.AddPolygon(p1,ptSubject);
  clpryz.AddPolygon(p2,ptClip);
  clpryz.Execute(ctIntersection,pyz,pftEvenOdd,pftEvenOdd);
  if (pyz[0].size())
  {
    getPolyOrientation(pyz[0],ip,orientation);
    if (AR < 0.0) AR=0.0;
    ARX=double(Area(pyz[0]))/10E33;
    AR+=pow(ARX,2.0);
    getCenter(pyz[0],XYZFC[1],XYZFC[2],ARX);
    for (i = 0; i < 4; i++,ip+=orientation)
    {
      if (ip==4 && orientation==1)
      {
        ip = 0;
      }
      else if (ip==-1 && orientation==-1)
      {
        ip = 3;
      }

      XYZCOM[(i*3)+1]= double(pyz[0][ip].X/10E16);
      XYZCOM[(i*3)+2]= double(pyz[0][ip].Y/10E16);
    }
  }

  if (AR > 0.0)
  {
    AR=sqrt(AR);
    //baricenter(XYZCOM,XYZFC);
  }

  return;
}

/*
 * This routine checks if the boundary plane of the
 * adjacent block and the boundary face of the boundary
 * control volume are coplanar and if they overlap
 *
 * using capital letters for vectors:
 *
 * E1=A+s*B+t*C; E2=D+u*E+v*F
 *
 * SINCE THE SYSTEM BY DEFAULT HAS RANK < 3 THERE IS ONE
 * FREE VARIABLE: v=1
 *
 * A LAPACK ROUTINE IS USED TO CHECK WHETHER THE RESULTING
 * SYSTEM IS SINGULAR. DGETRF CALCULATES A LU DECOMPOSITION.
 * SINGULARITY IS CHECKED VIA INFO OUTPUT PARAMETER:
 *                   INFO = 0 -> PLANES ARE OBLIQUE
 *                   INFO > 0 -> PLANES ARE PARALLEL
 *
 * IF PLANES ARE PARALLEL (THE LINEAR SYSTEM IS SINGULAR)
 * A CHECK IS PERFOMED TO SEE IF THE PLANES ARE IDENTICAL
 * I.E. THE THIRD EQUATION OF THE LS AFTER PERMUTATION IS
 * ALWAYS TRUE (SYSTEM HAS RANK 2)
*/
bool checkorientation(double * XYZL, double * XYZR)
{
  double AMAT[4][3];
  int ipiv[3],info,i;
  int m = 3;
  int n = 4;
  int lda = 3;

  for (int i=0; i<3;i++)
  {
    AMAT[0][i]=XYZL[i+3]-XYZL[i];
    AMAT[1][i]=XYZL[i+9]-XYZL[i];
    AMAT[2][i]=XYZR[i]-XYZR[i+3];
    AMAT[3][i]=-XYZL[i]+XYZR[i+9];
  }

  //dgesv_(&n,&nrhs, &AMAT[0][0],&lda, ipiv, &RHS[0][0], &ldb, &info);
  dgetrf_(&m,&n,&AMAT[0][0],&lda,ipiv,&info);

  if (info)
  {
    if (AMAT[2][2] == AMAT[3][2] && AMAT[3][2] == 0){
//    if (abs(AMAT[2][2]-AMAT[3][2]) <= 0.1){
      return true;
    }
  }

  return false;
}

/*
void baricenter(double * vert, int x1, int x2, double * cent)
{
   * vert [I]
   * cent  X
   * x1,x2 coordinate axes {xy,xz,yz}
   *
   *       [0]---------[1]
   *        |           |
   *        |           |
   *        |     X     |
   *        |           |
   *        |           |
   *       [3]---------[2]
   *
   *

  std::vector<double> DX41(2),DX42(2),DX43(2);
  double S124,S234;

//Vectors to vertices ( from IJK1 to IJK2, IJK3 and IJK4 )
  DX41[0]=vert[0  +x1]-vert[3*3+x1];
  DX41[1]=vert[0  +x2]-vert[3*3+x2];

  DX42[0]=vert[1*3+x1]-vert[3*3+x1];
  DX42[1]=vert[1*3+x2]-vert[3*3+x2];

  DX43[0]=vert[2*3+x1]-vert[3*3+x1];
  DX43[1]=vert[2*3+x2]-vert[3*3+x2];

//Area of each triangle
  S124=0.5*abs(DX41[0]*DX42[0]+DX41[1]*DX42[1]);
  S234=0.5*abs(DX42[0]*DX42[0]+DX43[1]*DX42[1]);

//Baricenters of each triangle
  DX41[0]=(vert[0  +x1]+vert[1*3+x1]+vert[4*3+x1])/3.0;
  DX41[1]=(vert[0  +x2]+vert[1*3+x2]+vert[4*3+x2])/3.0;

  DX43[0]=(vert[1*3+x1]+vert[2*3+x1]+vert[4*3+x1])/3.0;
  DX43[1]=(vert[1*3+x2]+vert[2*3+x2]+vert[4*3+x2])/3.0;

//Baricenter of face (weighted average)
  cent[x1]=(DX41[0]*S124+DX43[0]*S234)/(S124+S234);
  cent[x2]=(DX41[1]*S124+DX43[1]*S234)/(S124+S234);
  return;
}
*/

/*
 * This function is based on calcface from caffa3d.MB.f. It calculates the baricenter of the tetragons
 * resulting from polygon clipping
 */
void baricenter(double * vert, double * cent)
{
  /* vert [I]
   * cent  X
   * x1,x2 coordinate axes {xy,xz,yz}
   *
   *       [0]---------[1]
   *        |           |
   *        |           |
   *        |     X     |
   *        |           |
   *        |           |
   *       [3]---------[2]
   *
   */

  double DX12[3],DX13[3],DX14[3];
  double XR23[3],XR34[3];
  double XXCR[3];
  double S23,S34;

//.....Vectors to vertices ( from IJK1 to IJK2, IJK3 and IJK4 )
      DX12[0]=vert[3*1  ]-vert[0];
      DX12[1]=vert[3*1+1]-vert[1];
      DX12[2]=vert[3*1+2]-vert[2];

      DX13[0]=vert[3*2  ]-vert[0];
      DX13[1]=vert[3*2+1]-vert[1];
      DX13[2]=vert[3*2+2]-vert[2];

      DX14[0]=vert[3*3  ]-vert[0];
      DX14[1]=vert[3*3+1]-vert[1];
      DX14[2]=vert[3*3+2]-vert[2];
//
//.....Cross Products for triangle surface vectors
//.....This is (IJK1,IJK2,IJK3)
      XR23[0]=DX12[1]*DX13[2]-DX12[2]*DX13[1];
      XR23[1]=DX12[2]*DX13[0]-DX12[0]*DX13[2];
      XR23[2]=DX12[0]*DX13[1]-DX12[1]*DX13[0];
//
//.....This is (IJK1,IJK3,IJK4)
      XR34[0]=DX13[1]*DX14[2]-DX13[2]*DX14[1];
      XR34[1]=DX13[2]*DX14[0]-DX13[0]*DX14[2];
      XR34[2]=DX13[0]*DX14[1]-DX13[1]*DX14[0];
//
//.....Face surface vectors (add both triangles)
      XXCR[0]=0.5*(XR23[0]+XR34[0]);
      XXCR[1]=0.5*(XR23[1]+XR34[1]);
      XXCR[2]=0.5*(XR23[2]+XR34[2]);
//
//.....Baricenters of each triangle
      DX12[0]=(vert[3*0+0]+vert[3*1+0]+vert[3*2+0])/3.0;
      DX12[1]=(vert[3*0+1]+vert[3*1+1]+vert[3*2+1])/3.0;
      DX12[2]=(vert[3*0+2]+vert[3*1+2]+vert[3*2+2])/3.0;

      DX14[0]=(vert[3*0+0]+vert[3*2+0]+vert[3*3+0])/3.0;
      DX14[1]=(vert[3*0+1]+vert[3*2+1]+vert[3*3+1])/3.0;
      DX14[2]=(vert[3*0+2]+vert[3*2+2]+vert[3*3+2])/3.0;
//
//.....Area of each triangle
      S23=sqrt(pow(XR23[0],2)+pow(XR23[1],2)+pow(XR23[2],2));
      S34=sqrt(pow(XR34[0],2)+pow(XR34[1],2)+pow(XR34[2],2));
//
//.....Baricenter of face (weighted average)
      cent[0]=(DX12[0]*S23+DX14[0]*S34)/(S23+S34+DBL_MIN);
      cent[1]=(DX12[1]*S23+DX14[1]*S34)/(S23+S34+DBL_MIN);
      cent[2]=(DX12[2]*S23+DX14[2]*S34)/(S23+S34+DBL_MIN);
  return;
}

/*
 * This function finds the lower left corner of the polygon and checks its orientation
 * by calculating the determinant of the orientation matrix of the first three points
 * of the polygon. In case of clockwise orientation the orientation is set to -1.
 */
void getPolyOrientation(const Polygon & poly, int & istart, int & orientation)
{
  long64 xmin,ymin;
  double xa,xb,xc,ya,yb,yc;

  istart=0;
  xmin = poly[istart].X;
  ymin = poly[istart].Y;

  for (int i = 1; i < 4; i++)
  {
    if (poly[i].X <= xmin && poly[i].Y <= ymin)
    {
      istart = i;
      xmin = poly[i].X;
      ymin = poly[i].Y;
    }
  }
  //std::cout << "istart= " << istart << std::endl;

  xa=double(poly[0].X/10E16);
  ya=double(poly[0].Y/10E16);
  xb=double(poly[1].X/10E16);
  yb=double(poly[1].Y/10E16);
  xc=double(poly[2].X/10E16);
  yc=double(poly[2].Y/10E16);

  if ((xb-xa)*(yc-ya)-(xc-xa)*(yb-ya) > 0)
  {
    orientation = 1;
  }
  else
  {
    orientation =-1;
  }

  return;
}

/*
 * This function calculates the center of mass of an arbitrary polygon using this formula:
 * http://de.wikipedia.org/wiki/Geometrischer_Schwerpunkt#Polygon
 */
void getCenter(const Polygon & poly, double & xs, double & ys, double & a)
{
  int n = poly.size();
  xs=(double(poly[n-1].X/10E16)+double(poly[0].X/10E16))
    *(double(poly[n-1].X/10E16)*double(poly[0].Y/10E16)-double(poly[0].X/10E16)*double(poly[n-1].Y/10E16));
  ys=(double(poly[n-1].Y/10E16)+double(poly[0].Y/10E16))
    *(double(poly[n-1].X/10E16)*double(poly[0].Y/10E16)-double(poly[0].X/10E16)*double(poly[n-1].Y/10E16));

  for (int i=0; i<n-1; i++)
  {
  xs+=(double(poly[i].X/10E16)+double(poly[i+1].X/10E16))
     *(double(poly[i].X/10E16)*double(poly[i+1].Y/10E16)-double(poly[i+1].X/10E16)*double(poly[i].Y/10E16));
  ys+=(double(poly[i].Y/10E16)+double(poly[i+1].Y/10E16))
     *(double(poly[i].X/10E16)*double(poly[i+1].Y/10E16)-double(poly[i+1].X/10E16)*double(poly[i].Y/10E16));
  }
  xs*=1.0/(6.0*a);
  ys*=1.0/(6.0*a);

  return;
}
