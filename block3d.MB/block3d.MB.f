C#########################################################
      PROGRAM GBLOCK
C#########################################################
C     This code assembles block-structured grid, from
C     grid blocks produced by 'grid3dMB.f'.
C     The interfaces must be 'matching' (rather than
C     'non-matching')
C
C     Autor: FALK   DATE: 25.11.2011
C     Block structured grid can also be supplied via
C     ICEMCFD. 2 Translation Tools have to be used for that.
C     1. r_icem_w_caffa.cpp to read data from ICEMCFD
C        .grd and .tbc files
C     2. w_preprocessor.F to prepare data read by
C        r_icem_w_caffa.cpp for block-preprocessor and 
C        solver Caffa3D.
C
C
C     Each block is read and global indexes are calculated.
C
C     Information about the boundary interfaces is collected,
C     and matching interface CVs are found.
C
C     Finally, a master grid file is generated with the 
C     required global information about the grid.
C     This file is used by the flow solver 'caffa3dMB.f'
C     to know how to read each grid block, and to treat
C     interface boundaries. 
C
C     Also a 'param3d.inc' file is produced with appropiate
C     dimensions for the flow solver
C
C     VERSION 1.1       DATE: 06.01.2012    AUTHOR: U.Falk
C     
C     Code is extended in order to match blocks when user 
C     specifies integer local block refinement. 
C     
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "logico3d.inb"
#include "grid3d.inb"
#include "bound3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
C     CHARACTER*11 FILIN,FILGRD,FILBCK 
      CHARACTER*11 FILIN
      CHARACTER*13 FILGRD
      CHARACTER*15 FILBCK,FILPRC
      CHARACTER*7  NAME
      integer st,L,P,TID,OMP_GET_THREAD_NUM,NEWFSG
      integer STT,ENT,NTHREADS,OMP_GET_NUM_THREADS
C NEW
      C=0
C END NEW
C
C.....THIS FLAG KEEPS TRACK OF 'OK' STATUS.
C
      LOK=.TRUE.      
C
C.....DEFINE FILES
C
      PRINT *, ' ENTER PROBLEM NAME (SIX CHARACTERS):  '
C      READ(*,'(A6)') NAME
      NAME='control'

      write(*,*)'INPUT FILE ',NAME,' IS READ'
C
      WRITE(FILIN ,'(A7,4H.kin)') NAME
C     WRITE(FILBCK,'(A7,4H.bck)') NAME
C
C.....OPEN FILES
C
      OPEN (UNIT=5,FILE=FILIN)
C     OPEN (UNIT=8,FILE=FILBCK,FORM='binary')
C
C.....READ NBLOCKS (number of blocks) FROM INPUT FILE
C
      READ(5,*) NBLKS
C
C.....LOOP THROUGH GRID BLOCKS
C
      DO L=1,NBLKS
C
C.....READ FILE NAME FOR THIS BLOCK FROM INPUT FILE
C
        READ(5,'(A13)') FILGRD
C       WRITE( FILGRD,'(A9,4H.grd)') NAME
C
C.....OPEN FILE FOR THIS GRID BLOCK
C
C NEW
C       OPEN (UNIT=4,FILE=FILGRD,FORM='binary')
        OPEN (UNIT=4,FILE=FILGRD,FORM='UNFORMATTED',POSITION='REWIND')
C END NEW
c        OPEN (UNIT=4,FILE=FILGRD,status='old',
c     *       form='UNFORMATTED', iostat=st)
C       REWIND(4)
C
C.....READ NEXT BLOCK
C
        CALL READGRD(L)
        CLOSE(UNIT=4)
C
C.....BUILD INDEXES
        CALL BUILDINDX(L)        
C
C.....ACCUMULATE X,Y,X,NOC,IOCS,IJL,IJOC1,IJOC2,IJOC3,IJOC4
C.....FOR GLOBAL INTERFACE SEARCH AND MATCHING
        CALL KEEPOCDATA(L)
C
      END DO
C NEW
C     
C.....PREPARATION OF TECPLOT OUTPUT
C
C     TECMATCH(1:IJKBK(NBLKS))=0      
C END NEW
C
C.....MATCH MANY-TO-ONE INTERFACES (USER GUIDED THROUGH <PROJECT>.kin-FILE)
C     ONLY INTERGER MANY-TO-ONE INTERFACE POINTS ARE POSSIBLE
C
      CALL MATCHGROUP
C
C.....MATCH ONE-TO-ONE INTERFACES (AUTOMATIC)
C
      CALL INTFIND
C NEW
C
C.....PARTITION BLOCKS ON DIFFERENT PROCESSORS
C
      CALL BLOCKPART
C
C.....MATCH MANY-TO-MANY INTERFACES (OPENMP PARALLELIZED)
C
      CALL MTMFIND
C
C NEW
C
C.....OUTPUT GRID
C
C     CALL TECPLOTOUT
C
C.....PARTITION DATA FOR MULTIPROCESSOR USE
C
      CALL PARTITION
C
      NIBKAL=  MAXVAL(NIPR)
      NJBKAL=  MAXVAL(NJPR)
      NKBKAL=  MAXVAL(NKPR)
      NIJKBKAL=MAXVAL(NIJKPR)
      NIABK=   MAXVAL(NINLPR)
      NOABK=   MAXVAL(NOUTPR)
      NSABK=   MAXVAL(NSYMPR)
      NWABK=   MAXVAL(NWALPR)
      NOCABK=  MAXVAL(NOCPR)
      NMABK=   MAXVAL(NMTMPR)
      NFABK=   MAXVAL(NFSGPR)
      NBLKS=   MAXVAL(NBPR)
      IF(LOK) CALL PARAMOUT
C
      DO P=1,NPROC
C
C.....WRITE MATER PROCESSOR-GRID (CHANGED ORDER OF SUBROUTINE CALLS)
C
      NAME='control'
        WRITE(FILBCK,'(A7,I4.4,4H.bck)') NAME, P-1
        WRITE(FILPRC,'(A7,I4.4,4H.prc)') NAME, P-1
        OPEN (UNIT=8,FILE=FILBCK,FORM='UNFORMATTED',POSITION='REWIND')
C       OPEN (UNIT=8,FILE=FILBCK)
        OPEN (UNIT=9,FILE=FILPRC)
        CALL MASTEROUT(8,P)
        CLOSE(UNIT=8)
        CLOSE(UNIT=9)
      END DO
      CLOSE(UNIT=5)     
C
C END NEW
C
C
C
      STOP
      END
C
C
C#########################################################
      SUBROUTINE READGRD(L)
C#########################################################
C     This routine reads the next grid block listed 
C     in the input file
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "grid3d.inb"
#include "bound3d.inb"
#include "indexb3d.inb"
      integer NINX,NOUX,NSYX,NWAX,NOCX,I,K,L
C NEW
      integer NMTX        
C END NEW
C
C.....READ GRID BLOCK DATA
C     

      READ(4) NI,NJ,NK,NIJK,NIABK,NOABK,NSABK,NWABK,NOCABK,
C NEW
     *        NMABK,
C END NEW
     *        NWAISBK,NWAADBK,NINX,NOUX,NSYX,NWAX,NOCX,
C NEW
     *        NMTX
C END NEW
C
      READ(4) (LI(I),I=1,NI),(LK(K),K=1,NK)
C
      READ(4) (IJI(I)  ,I=1,NINX), (IJPI(I) ,I=1,NINX),
     *        (IJI1(I) ,I=1,NINX), (IJI2(I) ,I=1,NINX),
     *        (IJI3(I) ,I=1,NINX), (IJI4(I) ,I=1,NINX),
     *        (ITAGI(I),I=1,NINX)
      READ(4) (IJO(I)  ,I=1,NOUX), (IJPO(I) ,I=1,NOUX),
     *        (IJO1(I) ,I=1,NOUX), (IJO2(I) ,I=1,NOUX),
     *        (IJO3(I) ,I=1,NOUX), (IJO4(I) ,I=1,NOUX),
     *        (ITAGO(I),I=1,NOUX)
      READ(4) (IJW(I)  ,I=1,NWAX), (IJPW(I) ,I=1,NWAX),
     *        (IJW1(I) ,I=1,NWAX), (IJW2(I) ,I=1,NWAX),
     *        (IJW3(I) ,I=1,NWAX), (IJW4(I) ,I=1,NWAX), 
     *        (ITAGW(I),I=1,NWAX)
      READ(4) (IJS(I)  ,I=1,NSYX), (IJPS(I) ,I=1,NSYX),
     *        (IJS1(I) ,I=1,NSYX), (IJS2(I) ,I=1,NSYX),
     *        (IJS3(I) ,I=1,NSYX), (IJS4(I) ,I=1,NSYX),
     *        (ITAGS(I),I=1,NSYX)
      READ(4) (IJL(I)  ,I=1,NOCX), (IJR(I)  ,I=1,NOCX),
     *        (IJOC1(I),I=1,NOCX), (IJOC2(I),I=1,NOCX),
     *        (IJOC3(I),I=1,NOCX), (IJOC4(I),I=1,NOCX),
     *        (ITAGOC(I),I=1,NOCX)   
C NEW
      READ(4)
     *        (IJML(I) ,I=1,NMTX),(IJMR(I) ,I=1,NMTX),
     *        (IJM1(I) ,I=1,NMTX),(IJM2(I) ,I=1,NMTX),
     *        (IJM3(I) ,I=1,NMTX),(IJM4(I) ,I=1,NMTX),
     *        (ITAGM(I),I=1,NMTX)
C END NEW
C
      READ(4) (X(I) ,I=1,NIJK),(Y(I) ,I=1,NIJK),(Z(I) ,I=1,NIJK),
     *        (XC(I),I=1,NIJK),(YC(I),I=1,NIJK),(ZC(I),I=1,NIJK),
     *        (FX(I),I=1,NIJK),(FY(I),I=1,NIJK),(FZ(I),I=1,NIJK),
     *        (VOL(I),I=1,NIJK),
     *        (SRDW(I),I=1,NWAX),
     *        (XNW(I),I=1,NWAX),(YNW(I),I=1,NWAX),(ZNW(I),I=1,NWAX),
     *        (SRDS(I),I=1,NSYX),
     *        (XNS(I),I=1,NSYX),(YNS(I),I=1,NSYX),(ZNS(I),I=1,NSYX),
     *        (FOC(I),I=1,NOCX)
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE BUILDINDX(L)
C#########################################################
C     This routine builds the global indexes for dealing
C     with the block structured grid.
C     The order for the indexes is : J,I,K,L where L is
C     the block index.
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "logico3d.inb"
#include "bound3d.inb"
#include "indexb3d.inb"
      integer L,NIJ,I
      integer K,IJK,J
C
C
C.....COPY DATA FROM CURRENT BLOCK
C
      NIJ=NI*NJ
C
C.....GLOBAL INDEXES TO CVs
C
      IF(L.EQ.1) THEN
        IBK(1)=0
        JBK(1)=0
        KBK(1)=0
        IJKBK(1)=0
      ELSE
        IBK(L)=IBK(L-1)+NIBK(L-1)
        JBK(L)=JBK(L-1)+NJBK(L-1)
        KBK(L)=KBK(L-1)+NKBK(L-1)
        IJKBK(L)=IJKBK(L-1)+NIJKBK(L-1)
      ENDIF
C
      NIBK(L)=NI
      NJBK(L)=NJ
      NKBK(L)=NK
      NIJKBK(L)=NIJK
C
      DO I=1,NI
        LIBK(I+IBK(L))=(I-1)*NJ
      END DO
C
      DO K=1,NK
        LKBK(K+KBK(L))=(K-1)*NIJ+IJKBK(L)
      END DO
C
C.....GLOBAL INDEXES TO BCs ARRAYS
C
      IF (L.EQ.1) THEN
        IIBK(1)=0
        IOBK(1)=0
        IWBK(1)=0
        IWABK(1)=NWAISBK
        ISBK(1)=0
        IOCBK(1)=0
C NEW
        IMBK(1)=0
C END NEW
      ELSE
        IIBK(L)=IIBK(L-1)+NINLBK(L-1)
        IOBK(L)=IOBK(L-1)+NOUTBK(L-1)
        IWBK(L)=IWBK(L-1)+NWALBK(L-1)
        IWABK(L)=IWBK(L)+NWAISBK
        ISBK(L)=ISBK(L-1)+NSYMBK(L-1)
        IOCBK(L)=IOCBK(L-1)+NOCBK(L-1)
C NEW
        IMBK(L)=IMBK(L-1)+NMTMBK(L-1)
C END NEW
      ENDIF
C
      NINLBK(L)=NIABK
      NOUTBK(L)=NOABK
      NWALBK(L)=NWABK
      NWALIBK(L)=NWAISBK
      NWALABK(L)=NWAADBK
      NSYMBK(L)=NSABK
      NOCBK(L)=NOCABK
C NEW
      NMTMBK(L)=NMABK
C END NEW
C
C.....TOTAL NUMBER OF EVERYTHING
C
      IF(L.EQ.NBLKS) THEN
        NIBKAL=IBK(L)+NIBK(L)
        NJBKAL=JBK(L)+NJBK(L)
        NKBKAL=KBK(L)+NKBK(L)
        NIJKBKAL=IJKBK(L)+NIJKBK(L)
        NINLBKAL=IIBK(L)+NINLBK(L)
        NOUTBKAL=IOBK(L)+NOUTBK(L)
        NWALBKAL=IWBK(L)+NWALBK(L)
        NSYMBKAL=ISBK(L)+NSYMBK(L)
        NOCBKAL=IOCBK(L)+NOCBK(L)
C NEW - NECESSARY?
        NMTMBKAL=IMBK(L)+NMTMBK(L)
C END NEW
      ENDIF
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE KEEPOCDATA(L)
C#########################################################
C     For processing the Interface Boundaries we only
C     need to keep for grid blocks : X, Y, Z, XC, YC, ZC
C     and the OC data
C
C     This routine accumulates this arrays into global
C     indexed arrays for later use
C=============================================================
      implicit none

#include "param3d.inb"
#include "grid3d.inb"
#include "bound3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
      integer L,K,I,J,IJK,IJKGL,IOC,IM
C
C
C.....COPY X,Y,Z
C
      DO K=1,NK
      DO I=1,NI
      DO J=1,NJ
        IJK=LK(K)+LI(I)+J
        IJKGL=LKBK(K+KBK(L))+LIBK(I+IBK(L))+J
        XGL(IJKGL)=X(IJK)
        YGL(IJKGL)=Y(IJK)
        ZGL(IJKGL)=Z(IJK)
        XCGL(IJKGL)=XC(IJK)
        YCGL(IJKGL)=YC(IJK)
        ZCGL(IJKGL)=ZC(IJK)
      END DO
      END DO
      END DO
C
C.....COPY OC DATA
C
      DO IOC=1,NOCABK
        IJLPBM(IOC+IOCBK(L))=IJL(IOC)
        IJLBBM(IOC+IOCBK(L))=IJR(IOC)
        IJOC1BM(IOC+IOCBK(L))=IJOC1(IOC)
        IJOC2BM(IOC+IOCBK(L))=IJOC2(IOC)
        IJOC3BM(IOC+IOCBK(L))=IJOC3(IOC)
        IJOC4BM(IOC+IOCBK(L))=IJOC4(IOC)
        ITAGOCBM(IOC+IOCBK(L))=ITAGOC(IOC)
      END DO
C NEW
C
C.....COPY MTM DATA
C
      DO IM=1,NMABK
        IJMLPBM(IM+IMBK(L))=IJML(IM)
        IJMLBBM(IM+IMBK(L))=IJMR(IM)
        IJM1BM(IM+IMBK(L))=IJM1(IM)
        IJM2BM(IM+IMBK(L))=IJM2(IM)
        IJM3BM(IM+IMBK(L))=IJM3(IM)
        IJM4BM(IM+IMBK(L))=IJM4(IM)
        ITAGMBM(IM+IMBK(L))=ITAGM(IM)
      END DO
C END NEW
C
C
      RETURN 
      END
C
C
C#########################################################
      SUBROUTINE MATCHGROUP
C#########################################################
C     This routine matches CV faces implicated in 
C     many-to-one interface boundaries. User guided through
C     <project>.kin-File. Only integer many-to-one interface
C     points are possible.
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "logico3d.inb"
#include "grid3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
      integer IOCL,NEWOC,NGROUPS,N
      integer M1,IS1,IE1,JS1,JE1,KS1,KE1
      integer M2,IS2,IE2,JS2,JE2,KS2,KE2,KSTM2,ISTM2
      integer KSTM1,ISTM1,K2,I2,J2,IJB2,IO2,IJP2
      integer IJ12,IJ22,IJ32,IJ42,ITAG2,IJB,IJP
      integer KST1,IST1,JST1,K1,I1,J1,IJB1,IJBIO
      integer IO1,IJP1,IJ11,IJ21,IJ31,IJ41,ITAG1,IO,M
      integer IJ1,IJ2,IJ3,IJ4,ITAG
      integer IJBM,HIE1,HJE1,HKE1,HI1,COR1,COR2
      integer :: st
      real*8 XCC1,YCC1,ZCC1,XCR1,YCR1,ZCR1,DL1,DL2
      real*8 DI,DJ,DK,SMALL
      real*8 XCMF,YCMF,ZCMF,AGM1,AGM2,AGM3,SUMAGM
      real*8, dimension(:), allocatable :: MATCHPOINT
      real*8, dimension(:,:), allocatable :: VECM
C     
      COMMON /OCB/ FLAGOC(MXINTF),LNOMATCH,LFREE
      LOGICAL FLAGOC,LNOMATCH,LFREE
C NEW
      LOGICAL LMTBK(MXBLKS)
C END NEW
C
C.....INITIALIZE FLAGS IN ALL INTERFACE FACES
C
      SMALL=1.E-25
      DO IOCL=1,NOCBKAL
       FLAGOC(IOCL)=.TRUE.
      END DO
      allocate(VECM(3,3), stat=st)
      if (st /= 0) then    
         stop "allocation-Fehler VECM!"
      end if
C
C.....SET UP CONTROL FLAGS
C
      LFREE=.TRUE.
C
C.....INITIALIZE COUNTER FOR MATCHED INTERFACES
C
      NEWOC=0
      HI1=0
C NEW
      IMTBK=0
      NMTOBK=0
      LMTBK=.TRUE.
C END NEW
C
C.....READ DATA FROM INPUT FILE
C    
      READ(5,*) NGROUPS
C
      DO N=1,NGROUPS
C       
      READ(5,*) M1,IS1,IE1,JS1,JE1,KS1,KE1
      READ(5,*) M2,IS2,IE2,JS2,JE2,KS2,KE2 

C NEW
      IF (IMTBK(M1).EQ.0.AND.LMTBK(M1)) THEN
        IMTBK(M1)=NEWOC
        LMTBK(M1)=.FALSE.
      END IF
C END NEW
      
      st=0
      allocate(MATCHPOINT(IE1*JE1*KE1), stat=st)
c
      if (st /= 0) then    
         stop "allocation-Fehler MATCHPOINT!"
      end if
      MATCHPOINT(1:(IE1*JE1*KE1))=0
      HIE1=IE1
      HJE1=JE1
      HKE1=KE1
      COR1=0
      COR2=0     
C
      IF(IS1.NE.IE1)THEN
         IS1=IS1+1
         IE1=IE1-1
         COR1=COR1-NJBK(M1)
      ENDIF
      IF(JS1.NE.JE1)THEN
         JS1=JS1+1
         JE1=JE1-1
         COR1=COR1-1
      ENDIF
      IF(KS1.NE.KE1)THEN
         KS1=KS1+1
         KE1=KE1-1
         COR1=COR1-NJBK(M1)*NIBK(M1)
      ENDIF
C
      IF(IS2.NE.IE2)THEN
         IS2=IS2+1
         IE2=IE2-1
         COR2=COR2-NJBK(M2)
      ENDIF
      IF(JS2.NE.JE2)THEN
         JS2=JS2+1
         JE2=JE2-1
         COR2=COR2-1
      ENDIF
      IF(KS2.NE.KE2)THEN
         KS2=KS2+1
         KE2=KE2-1
         COR2=COR2-NJBK(M2)*NIBK(M2)
      ENDIF
C
      KSTM2=KBK(M2)
      ISTM2=IBK(M2)
      KSTM1=KBK(M1)
      ISTM1=IBK(M1)
C
C.....LOOP THROUGH "COARSE" GRID FACE
C      
      DO K2=KS2,KE2
      DO I2=IS2,IE2
      DO J2=JS2,JE2
         HI1=HI1+1
C     
C.....BASED ON COORDINATES SUPPLIED VIA <PROJECT>.KIN FILE 
C     POINTS CONNECTED TO THAT CV FACE ARE RETRIEVED FROM
C     CONNECTIVITY INFORMATION ARRAY.
        IJB2=LKBK(K2+KSTM2)+LIBK(I2+ISTM2)+J2
        CALL FINDFACE(M2,IJB2,IO2,IJP2,IJ12,IJ22,IJ32,IJ42,ITAG2)
C
C.....CHECK WHETHER POINT IS ALREADY MATCHED WITH SOME OTHER POINTS 
        IF(.NOT.FLAGOC(IO2)) LFREE=.FALSE.
        FLAGOC(IO2)=.FALSE.
C
C.....LOOP THROUGH ADJANCED "FINE" GRID FACE 
        DO K1=KS1,KE1  
        DO I1=IS1,IE1
        DO J1=JS1,JE1
C
         IJBM=(K1-1)*HIE1*HJE1+(I1-1)*HJE1+J1 
         IF(MATCHPOINT(IJBM).eq.0)THEN
          IJB1=LKBK(K1+KSTM1)+LIBK(I1+ISTM1)+J1
C
C.....CHECK WHETHER POINT IS ALREADY MATCHED TO "COARSE" GRID CELL
C     IF YES PROCEED

C.....BASED ON COORDINATES POINTS CONNECTED TO THAT CV FACE ARE RETRIEVED 
C     FROM CONNECTIVITY INFORMATION ARRAY.
          CALL FINDFACE(M1,IJB1,IO1,IJP1,IJ11,IJ21,IJ31,IJ41,ITAG1)
C
C.....MATCHING PROCEDURE HAS TO BE APPLIED
C     
C.....CENTER OF FINE GRID POINT IS CALCULATED
          XCMF=(XGL(IJ11)+XGL(IJ21)+XGL(IJ31)+XGL(IJ41))/4.d0
          YCMF=(YGL(IJ11)+YGL(IJ21)+YGL(IJ31)+YGL(IJ41))/4.d0
          ZCMF=(ZGL(IJ11)+ZGL(IJ21)+ZGL(IJ31)+ZGL(IJ41))/4.d0
C
C.....CHECK FIRST TRIANGLE
          VECM(1,1)=XCMF-XGL(IJ42)
          VECM(1,2)=YCMF-YGL(IJ42)
          VECM(1,3)=ZCMF-ZGL(IJ42)
C                    
          VECM(2,1)=XCMF-XGL(IJ12)
          VECM(2,2)=YCMF-YGL(IJ12)
          VECM(2,3)=ZCMF-ZGL(IJ12)
C                    
          VECM(3,1)=XCMF-XGL(IJ22)
          VECM(3,2)=YCMF-YGL(IJ22)
          VECM(3,3)=ZCMF-ZGL(IJ22)
C        
          AGM1=(VECM(1,1)*VECM(2,1)
     &         +VECM(1,2)*VECM(2,2)
     &         +VECM(1,3)*VECM(2,3))/
     &         (sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)*
     &         sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)+SMALL)
C          
          AGM2=(VECM(2,1)*VECM(3,1)
     &         +VECM(2,2)*VECM(3,2)
     &         +VECM(2,3)*VECM(3,3))/
     &         (sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)*
     &         sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)+SMALL)
C          
          AGM3=(VECM(3,1)*VECM(1,1)
     &         +VECM(3,2)*VECM(1,2)
     &         +VECM(3,3)*VECM(1,3))/
     &         (sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)*
     &         sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)+SMALL)
          if(AGM1.gt.1.d0)then
             AGM1=1.d0
          endif
          if(AGM2.gt.1.d0)then
             AGM2=1.d0
          endif
          if(AGM3.gt.1.d0)then
             AGM3=1.d0
          endif
           if(AGM1.lt.-1.d0)then
             AGM1=-1.d0
          endif
          if(AGM2.lt.-1.d0)then
             AGM2=-1.d0
          endif
          if(AGM3.lt.-1.d0)then
             AGM3=-1.d0
          endif
          SUMAGM=(ACOS(AGM1)+ACOS(AGM2)+ACOS(AGM3))
                 
          IF(ABS(SUMAGM-2.d0*3.141592653589793d0)<0.0001d0)THEN
             MATCHPOINT(IJBM)=1
C             WRITE(*,*)'MATCH 1'
             GOTO 111  
          ENDIF                   
C
C.....CHECK SECOND TRIANGLE
          VECM(1,1)=XCMF-XGL(IJ22)
          VECM(1,2)=YCMF-YGL(IJ22)
          VECM(1,3)=ZCMF-ZGL(IJ22)
C                    
          VECM(2,1)=XCMF-XGL(IJ32)
          VECM(2,2)=YCMF-YGL(IJ32)
          VECM(2,3)=ZCMF-ZGL(IJ32)
C                    
          VECM(3,1)=XCMF-XGL(IJ42)
          VECM(3,2)=YCMF-YGL(IJ42)
          VECM(3,3)=ZCMF-ZGL(IJ42)
C
          AGM1=(VECM(1,1)*VECM(2,1)
     &         +VECM(1,2)*VECM(2,2)
     &         +VECM(1,3)*VECM(2,3))/
     &         (sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)*
     &         sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)+SMALL)
C          
          AGM2=(VECM(2,1)*VECM(3,1)
     &         +VECM(2,2)*VECM(3,2)
     &         +VECM(2,3)*VECM(3,3))/
     &         (sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)*
     &         sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)+SMALL)
C          
          AGM3=(VECM(3,1)*VECM(1,1)
     &         +VECM(3,2)*VECM(1,2)
     &         +VECM(3,3)*VECM(1,3))/
     &         (sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)*
     &         sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)+SMALL)
          if(AGM1.gt.1.d0)then
             AGM1=1.d0
          endif
          if(AGM2.gt.1.d0)then
             AGM2=1.d0
          endif
          if(AGM3.gt.1.d0)then
             AGM3=1.d0
          endif
          if(AGM1.lt.-1.d0)then
             AGM1=-1.d0
          endif
          if(AGM2.lt.-1.d0)then
             AGM2=-1.d0
          endif
          if(AGM3.lt.-1.d0)then
             AGM3=-1.d0
          endif
          SUMAGM=(ACOS(AGM1)+ACOS(AGM2)+ACOS(AGM3))
          IF(ABS(SUMAGM-2.d0*3.141592653589793d0)<0.0001d0)THEN
             MATCHPOINT(IJBM)=1
C             WRITE(*,*)'MATCH 2'
             GOTO 111
          ENDIF   
C
C.....CHECK THIRD TRIANGLE
          VECM(1,1)=XCMF-XGL(IJ12)
          VECM(1,2)=YCMF-YGL(IJ12)
          VECM(1,3)=ZCMF-ZGL(IJ12)
C                    
          VECM(2,1)=XCMF-XGL(IJ22)
          VECM(2,2)=YCMF-YGL(IJ22)
          VECM(2,3)=ZCMF-ZGL(IJ22)
C                    
          VECM(3,1)=XCMF-XGL(IJ32)
          VECM(3,2)=YCMF-YGL(IJ32)
          VECM(3,3)=ZCMF-ZGL(IJ32)
C
          AGM1=(VECM(1,1)*VECM(2,1)
     &         +VECM(1,2)*VECM(2,2)
     &         +VECM(1,3)*VECM(2,3))/
     &         (sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)*
     &         sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)+SMALL)
C          
          AGM2=(VECM(2,1)*VECM(3,1)
     &         +VECM(2,2)*VECM(3,2)
     &         +VECM(2,3)*VECM(3,3))/
     &         (sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)*
     &         sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)+SMALL)
C          
          AGM3=(VECM(3,1)*VECM(1,1)
     &         +VECM(3,2)*VECM(1,2)
     &         +VECM(3,3)*VECM(1,3))/
     &         (sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)*
     &         sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)+SMALL)
          if(AGM1.gt.1.d0)then
             AGM1=1.d0
          endif
          if(AGM2.gt.1.d0)then
             AGM2=1.d0
          endif
          if(AGM3.gt.1.d0)then
             AGM3=1.d0
          endif
          if(AGM1.lt.-1.d0)then
             AGM1=-1.d0
          endif
          if(AGM2.lt.-1.d0)then
             AGM2=-1.d0
          endif
          if(AGM3.lt.-1.d0)then
             AGM3=-1.d0
          endif
          SUMAGM=(ACOS(AGM1)+ACOS(AGM2)+ACOS(AGM3))
          IF(ABS(SUMAGM-2.d0*3.141592653589793d0)<0.0001d0)THEN
             MATCHPOINT(IJBM)=1
C             WRITE(*,*)'MATCH 3'
             GOTO 111
          ENDIF   
C     
C.....CHECK FOURTH TRIANGLE
          VECM(1,1)=XCMF-XGL(IJ32)
          VECM(1,2)=YCMF-YGL(IJ32)
          VECM(1,3)=ZCMF-ZGL(IJ32)
C                    
          VECM(2,1)=XCMF-XGL(IJ42)
          VECM(2,2)=YCMF-YGL(IJ42)
          VECM(2,3)=ZCMF-ZGL(IJ42)
C                    
          VECM(3,1)=XCMF-XGL(IJ12)
          VECM(3,2)=YCMF-YGL(IJ12)
          VECM(3,3)=ZCMF-ZGL(IJ12)
C
          AGM1=(VECM(1,1)*VECM(2,1)
     &         +VECM(1,2)*VECM(2,2)
     &         +VECM(1,3)*VECM(2,3))/
     &         (sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)*
     &         sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)+SMALL)
C          
          AGM2=(VECM(2,1)*VECM(3,1)
     &         +VECM(2,2)*VECM(3,2)
     &         +VECM(2,3)*VECM(3,3))/
     &         (sqrt(VECM(2,1)**2+VECM(2,2)**2+VECM(2,3)**2)*
     &         sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)+SMALL)
C          
          AGM3=(VECM(3,1)*VECM(1,1)
     &         +VECM(3,2)*VECM(1,2)
     &         +VECM(3,3)*VECM(1,3))/
     &         (sqrt(VECM(3,1)**2+VECM(3,2)**2+VECM(3,3)**2)*
     &         sqrt(VECM(1,1)**2+VECM(1,2)**2+VECM(1,3)**2)+SMALL)
C
          if(AGM1.gt.1.d0)then
             AGM1=1.d0
          endif
          if(AGM2.gt.1.d0)then
             AGM2=1.d0
          endif
          if(AGM3.gt.1.d0)then
             AGM3=1.d0
          endif
          if(AGM1.lt.-1.d0)then
             AGM1=-1.d0
          endif
          if(AGM2.lt.-1.d0)then
             AGM2=-1.d0
          endif
          if(AGM3.lt.-1.d0)then
             AGM3=-1.d0
          endif
          SUMAGM=(ACOS(AGM1)+ACOS(AGM2)+ACOS(AGM3))
          IF(ABS(SUMAGM-2.d0*3.141592653589793d0)<0.0001d0)THEN
             MATCHPOINT(IJBM)=1
C             WRITE(*,*)'MATCH 4'
             GOTO 111
          ENDIF   
C
 111     CONTINUE 
C
C.....IN CASE MATCH EXISTS
         IF(MATCHPOINT(IJBM).EQ.1)THEN 
          !  WRITE(*,*)'SOURCE IJK',IJP1
          !  WRITE(*,*)'DESTINATION IJK',IJP2
            TECMATCH(IJP1+COR1)=HI1
            TECMATCH(IJP2+COR2)=HI1
           
           IF(.NOT.FLAGOC(IO1)) LFREE=.FALSE.
           FLAGOC(IO1)=.FALSE.
           CALL CALCFACEM(IJ11,IJ21,IJ31,IJ41,
     *        IJP1,IJP2,DL1,DL2)
           
C
C.....SAVE MATCH INFORMATIONS        
           NEWOC=NEWOC+1
           IF(MOD(NEWOC,1000).EQ.0) PRINT *,'NEWOC',NEWOC
           IJLPBK(NEWOC)=IJP1
           IJRPBK(NEWOC)=IJP2
           IJLBBK(NEWOC)=IJB1
           IJRBBK(NEWOC)=IJB2
           IJOC1BK(NEWOC)=IJ11
           IJOC2BK(NEWOC)=IJ21
           IJOC3BK(NEWOC)=IJ31
           IJOC4BK(NEWOC)=IJ41
C         
           FOCBK(NEWOC)=DL1/DL2
C          write(*,*) NEWOC,IJP1,IJP2
         
           ITAGOCBK(NEWOC)=ITAG1
           IBLKOCBK(NEWOC)=M1
C
         ENDIF
         ENDIF
         END DO
         END DO
         END DO
C
      END DO
      END DO
      END DO
C NEW
      NMTOBK(M1)=NEWOC-IMTBK(M1)
C END NEW
C
      deallocate(MATCHPOINT, stat=st)
      if (st /= 0) then    
         stop "deallocation-Fehler MATCHPOINT!"
      end if
      END DO
      write(*,*)'NUMBER OF CONNECTIVITIES FOUND',NEWOC
C
C.....FINISH THE SETUP OF UNIFIED OC DATA
C
      NOCBKAL=NEWOC
C
C.....WARNING IF SOMETHING WENT WRONG
C
      IF((.NOT.LFREE)) THEN
        PRINT *,'WARNING MANY-TO-ONE FAILED'
        PRINT *,LFREE
      ENDIF
C
      deallocate(VECM, stat=st)
      if (st /= 0) then    
         stop "deallocation-Fehler VECM!"
      end if
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE FINDFACE(M,IJB,IO,IJP,IJ1,IJ2,IJ3,IJ4,ITAG)
C#########################################################
C     This routine finds the interface face with given
C     IJB in block M, and retrieves the values of
C     IO,IJP,IJ1,IJ2,IJ3,IJ4,ITAG
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "logico3d.inb"
#include "grid3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
      integer IO,M,IJBIO,IJB,IJP,IJ1,IJ2,IJ3,IJ4,ITAG
C
      DO IO=IOCBK(M)+1,IOCBK(M)+NOCBK(M)
        IJBIO=IJLBBM(IO)+IJKBK(M)    
        IF(IJB.EQ.IJBIO) GOTO 100
      END DO
C
  100 CONTINUE
C
      IF(IJB.EQ.IJBIO) THEN
        IJP=IJLPBM(IO) +IJKBK(M)
        IJ1=IJOC1BM(IO)+IJKBK(M)
        IJ2=IJOC2BM(IO)+IJKBK(M)
        IJ3=IJOC3BM(IO)+IJKBK(M)
        IJ4=IJOC4BM(IO)+IJKBK(M)
        ITAG=ITAGOCBM(IO)
      ELSE
        PRINT *,'WARNING FACE NOT FOUND'
      ENDIF
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE INTFIND
C#########################################################
C     This routine matches all CV faces implicated in
C     one-to-one interface boundaries.
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "logico3d.inb"
#include "grid3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
C
      COMMON /OCB/ FLAGOC(MXINTF),LNOMATCH,LFREE
      LOGICAL FLAGOC,LNOMATCH,LFREE
      integer NEWOC,LL,IOL,IJPL,IJBL,IJ1L,IJ2L,IJ3L,IJ4L
      integer LR,IOR,IJPR,IJBR,IJ1R,IJ2R,IJ3R,IJ4R
      real*8 XCCL,YCCL,ZCCL,XCRL,YCRL,ZCRL,XYZL,XYZE,ERR
      real*8 DL1,DL2,EPSREL,ERRMIN,XCCR,YCCR,ZCCR,XCRR,YCRR,ZCRR
C
C     EPSREL=1.E-1
      EPSREL=1.65E-1
      LNOMATCH=.FALSE.
      NEWOC=NOCBKAL
C
C.....LOOP THROUGH ALL INTERFACE FACES
C
      DO LL=1,NBLKS
C NEW
      IOTBK(LL)=NEWOC
C END NEW
      DO IOL=IOCBK(LL)+1,IOCBK(LL)+NOCBK(LL)
        IF(FLAGOC(IOL)) THEN
          LNOMATCH=.TRUE.
          FLAGOC(IOL)=.FALSE.
          ERRMIN=999.
          IJPL=IJLPBM(IOL) +IJKBK(LL)
          IJBL=IJLBBM(IOL) +IJKBK(LL)
          IJ1L=IJOC1BM(IOL)+IJKBK(LL)
          IJ2L=IJOC2BM(IOL)+IJKBK(LL)
          IJ3L=IJOC3BM(IOL)+IJKBK(LL)
          IJ4L=IJOC4BM(IOL)+IJKBK(LL)
          CALL CALCFACE(IJ1L,IJ2L,IJ3L,IJ4L,
     *         XCCL,YCCL,ZCCL,XCRL,YCRL,ZCRL)
          XYZL=SQRT(XCRL**2+YCRL**2+ZCRL**2)
C
C.....LOOP SEARCHING FOR A MATCH WITHIN UNPAIRED FACES
C
          DO LR=LL,NBLKS
          DO IOR=IOCBK(LR)+1,IOCBK(LR)+NOCBK(LR)
            IF(FLAGOC(IOR)) THEN
              IJPR=IJLPBM(IOR) +IJKBK(LR)
              IJBR=IJLBBM(IOR) +IJKBK(LR)
              IJ1R=IJOC1BM(IOR)+IJKBK(LR)
              IJ2R=IJOC2BM(IOR)+IJKBK(LR)
              IJ3R=IJOC3BM(IOR)+IJKBK(LR)
              IJ4R=IJOC4BM(IOR)+IJKBK(LR)
              CALL CALCFACE(IJ1R,IJ2R,IJ3R,IJ4R,
     *             XCCR,YCCR,ZCCR,XCRR,YCRR,ZCRR)
              XYZE=SQRT((XCCL-XCCR)**2+(YCCL-YCCR)**2+
     *                  (ZCCL-ZCCR)**2) 
              ERR=XYZE/XYZL
C
C.....CHECK IF THERE IS A MATCH
C
              IF(ERR.LT.EPSREL) THEN
                FLAGOC(IOR)=.FALSE.
                LNOMATCH=.FALSE.
                NEWOC=NEWOC+1
                IJLPBK(NEWOC)=IJPL
                IJRPBK(NEWOC)=IJPR
C               print *, IJPL,IJPR
                IJLBBK(NEWOC)=IJBL
                IJRBBK(NEWOC)=IJBR
                IJOC1BK(NEWOC)=IJ1L
                IJOC2BK(NEWOC)=IJ2L
                IJOC3BK(NEWOC)=IJ3L
                IJOC4BK(NEWOC)=IJ4L
C NEW
C               DL1=SQRT((XCCL-XCGL(IJPL))**2+
C    *                   (YCCL-YCGL(IJPL))**2+
C    *                   (ZCCL-ZCGL(IJPL))**2)
C               DL2=SQRT((XCGL(IJPL)-XCGL(IJPR))**2+
C    *                   (YCGL(IJPL)-YCGL(IJPR))**2+
C    *                   (ZCGL(IJPL)-ZCGL(IJPR))**2)
                CALL CALCFACEM(IJ1L,IJ2L,IJ3L,IJ4L,
     *               IJPL,IJPR,DL1,DL2)
C END NEW
                FOCBK(NEWOC)=DL1/DL2
                ITAGOCBK(NEWOC)=ITAGOCBM(IOL)
                IBLKOCBK(NEWOC)=LL
C
C.....SUPPORT FOR SLIDING INTERFACES (NEEDS USER PROGRAMMING!!)
C
                IF(ITAGOCBM(IOL).GT.99) THEN
                  NEWOC=NEWOC+1
                  IJLPBK(NEWOC)=IJPL
                  IJRPBK(NEWOC)=IJPR
                  IJLBBK(NEWOC)=IJBL
                  IJRBBK(NEWOC)=IJBR
                  IJOC1BK(NEWOC)=IJ1L
                  IJOC2BK(NEWOC)=IJ2L
                  IJOC3BK(NEWOC)=IJ3L
                  IJOC4BK(NEWOC)=IJ4L
                  FOCBK(NEWOC)=DL1/DL2
                  ITAGOCBK(NEWOC)=-ITAGOCBM(IOL)
                  IBLKOCBK(NEWOC)=LL
                ENDIF
C
C.....SHORT-CUT THE LOOP IF MATCH FOUND
C
                GOTO 100
              ELSE
                ERRMIN=MIN(ERRMIN,ERR)
              ENDIF
C
C.....CLOSE BACK ALL LOOPs AND IFs
C
            ENDIF
          END DO
          END DO

  100     CONTINUE
C
C.....PRINT WARNING IF MATCH NOT FOUND FOR THIS (IOL) FACE
C
          IF(LNOMATCH) THEN
            PRINT *,' ...WARNING : INTERFACE PAIR NOT FOUND....'
            PRINT *,' ...SOMETHING WENT WRONG!...'
            PRINT *,' ...OUTPUT FILES WILL NOT BE CREATED!.....'
            LOK=.FALSE.
            PRINT *,LL,IOL,NEWOC,IJBL,XCCL,YCCL,ZCCL,
     *              XCRL,YCRL,ZCRL,ERRMIN
          ENDIF
C
        ENDIF
      END DO
C NEW
      NOTOBK(LL)=NEWOC-IOTBK(LL)
C END NEW
      END DO
C
C.....FINISH THE SETUP OF UNIFIED OC DATA
C
      write(*,*)'NUMBER OF CONNECTIVITIES TOTAL FOUND',NEWOC
      NOCBKAL=NEWOC
C
C
      RETURN 
      END
C
C
C NEW
C#########################################################
      SUBROUTINE MTMFIND
C#########################################################
C     This routine matches CV faces on arbitrary (non-matching)
C     many-to-many interface boundaries. (For performance
C     reasons before matching the interfaces this routine
C     searches blockwise for the adjacent blocks - currently
C     not supported)
C
C=============================================================

      implicit none

#include "param3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
#include "bound3d.inb"
      
      integer NEWFSG,LL,IML,IJPL,IJBL,IJ1L,IJ2L,IJ3L,IJ4L
      integer LR,IMR,IJPR,IJBR,IJ1R,IJ2R,IJ3R,IJ4R
      real*8 DL1,DL2,XCCL,YCCL,ZCCL
      real*8 XYZL(12),XYZR(12),XYZCOM(12),XYZFC(3),AR
      integer NEIGH(NXBK),NNEIGH,LN
      integer P
C NEW
      REAL*8 DX12,DY12,DZ12,DX13,DY13,DZ13,NORM
      REAL*8 XR23,YR23,ZR23
C END NEW
C
      NEWFSG=0
C     FFSGBK=0.0D0
C
C.....LOOP THROUGH ALL INTERFACE FACES
C
C     DO LL=IBPR(P)+1,IBPR(P)+NBPR(P)
      DO LL=1,NBLKS
#ifdef USE_BLOCKING_ASSUMPTION
      CALL FINDNEIGHBOURS(LL,NNEIGH,NEIGH)
#endif
      IFBK(LL)=NEWFSG
      DO IML=IMBK(LL)+1,IMBK(LL)+NMTMBK(LL)
        IJPL=IJMLPBM(IML)+IJKBK(LL)
        IJ1L=IJM1BM(IML) +IJKBK(LL)
        IJ2L=IJM2BM(IML) +IJKBK(LL)
        IJ3L=IJM3BM(IML) +IJKBK(LL)
        IJ4L=IJM4BM(IML) +IJKBK(LL)
        XYZL(1:3)  =[XGL(IJ1L),YGL(IJ1L),ZGL(IJ1L)]
        XYZL(4:6)  =[XGL(IJ2L),YGL(IJ2L),ZGL(IJ2L)]
        XYZL(7:9)  =[XGL(IJ3L),YGL(IJ3L),ZGL(IJ3L)]
        XYZL(10:12)=[XGL(IJ4L),YGL(IJ4L),ZGL(IJ4L)]

#ifndef USE_BLOCKING_ASSUMPTION
        DO LR=LL+1,NBLKSAL
#else
        DO LN=1,NNEIGH
        LR=NEIGH(LN)
#endif
        DO IMR=IMBK(LR)+1,IMBK(LR)+NMTMBK(LR)
          IJPR=IJMLPBM(IMR)+IJKBK(LR)
          IJ1R=IJM1BM(IMR) +IJKBK(LR)
          IJ2R=IJM2BM(IMR) +IJKBK(LR)
          IJ3R=IJM3BM(IMR) +IJKBK(LR)
          IJ4R=IJM4BM(IMR) +IJKBK(LR)
          XYZR(1:3)  =[XGL(IJ1R),YGL(IJ1R),ZGL(IJ1R)]
          XYZR(4:6)  =[XGL(IJ2R),YGL(IJ2R),ZGL(IJ2R)]
          XYZR(7:9)  =[XGL(IJ3R),YGL(IJ3R),ZGL(IJ3R)]
          XYZR(10:12)=[XGL(IJ4R),YGL(IJ4R),ZGL(IJ4R)]
C         XYZCOM=0.0D0
          CALL MATCHFACES(XYZL,XYZR,AR,XYZFC)
C         CALL MATCHFACES(XYZL,XYZR,XYZCOM,AR,XYZFC)
C         IF (AR.GT.0.0001D0) THEN
          IF (AR.GT.0.0D0) THEN
            NEWFSG=NEWFSG+1
            IJFL(NEWFSG)=IJPL
            IJFR(NEWFSG)=IJPR
C NEW
C.....VECTORS TO VERTICES ( FROM IJK1 TO IJ2L, IJ3L AND IJ4L )
C
            DX12=XGL(IJ2L)-XGL(IJ1L)
            DY12=YGL(IJ2L)-YGL(IJ1L)
            DZ12=ZGL(IJ2L)-ZGL(IJ1L)
     
            DX13=XGL(IJ3L)-XGL(IJ1L)
            DY13=YGL(IJ3L)-YGL(IJ1L)
            DZ13=ZGL(IJ3L)-ZGL(IJ1L)
C
C.....CROSS PRODUCT
C
            XR23=DY12*DZ13-DZ12*DY13
            YR23=DZ12*DX13-DX12*DZ13
            ZR23=DX12*DY13-DY12*DX13

            NORM=DSQRT(XR23**2+YR23**2+ZR23**2)

            XFR(NEWFSG)=AR*XR23/NORM
            YFR(NEWFSG)=AR*YR23/NORM
            ZFR(NEWFSG)=AR*ZR23/NORM

            XFC(NEWFSG)=XYZFC(1)
            YFC(NEWFSG)=XYZFC(2)
            ZFC(NEWFSG)=XYZFC(3)
C
C.....CALCULATE INDICES TO INCLUDE FACE COORDINATES INTO EXISTING X,Y,Z 
C.....ARRAYS (CAFFA-GHOSTING) 
C.....SUBTRACTING -1 IS NECESSARY TO TAKE OVERSIZING OF NXYZBK INTO ACCOUNT 
C..... (SEE W_PREPROC)
C           
C           IJF1(NEWFSG)=(NEWFSG-1)*4+1
C           IJF2(NEWFSG)=(NEWFSG-1)*4+2
C           IJF3(NEWFSG)=(NEWFSG-1)*4+3
C           IJF4(NEWFSG)=(NEWFSG-1)*4+4
C           SAVE FACE COORDINATES IN RESPECTIVE ARRAYS
C           XF((NEWFSG-1)*4+1)=XYZCOM( 1)
C           YF((NEWFSG-1)*4+1)=XYZCOM( 2)
C           ZF((NEWFSG-1)*4+1)=XYZCOM( 3)
C           XF((NEWFSG-1)*4+2)=XYZCOM( 4)
C           YF((NEWFSG-1)*4+2)=XYZCOM( 5)
C           ZF((NEWFSG-1)*4+2)=XYZCOM( 6)
C           XF((NEWFSG-1)*4+3)=XYZCOM( 7)
C           YF((NEWFSG-1)*4+3)=XYZCOM( 8)
C           ZF((NEWFSG-1)*4+3)=XYZCOM( 9)
C           XF((NEWFSG-1)*4+4)=XYZCOM(10)
C           YF((NEWFSG-1)*4+4)=XYZCOM(11)
C           ZF((NEWFSG-1)*4+4)=XYZCOM(12)
C END NEW
C           CALCULATE INTERPOLATION FACTOR
            XCCL=XYZFC(1)
            YCCL=XYZFC(2)
            ZCCL=XYZFC(3)
            DL1=SQRT((XCCL-XCGL(IJPL))**2+
     *               (YCCL-YCGL(IJPL))**2+
     *               (ZCCL-ZCGL(IJPL))**2)
            DL2=SQRT((XCGL(IJPL)-XCGL(IJPR))**2+
     *               (YCGL(IJPL)-YCGL(IJPR))**2+
     *               (ZCGL(IJPL)-ZCGL(IJPR))**2)
            FFSGBK(NEWFSG)=DL1/DL2
          END IF
        END DO
        END DO
      END DO
      NFSGBK(LL)=NEWFSG-IFBK(LL)
      PRINT *, LL,NFSGBK(LL),NEWFSG
      END DO
      PRINT *, 'NUMBER OF MTM CONNECTIVITIES FOUND', NEWFSG
C
C
      RETURN 
      END
C
C
C#########################################################
      SUBROUTINE FINDNEIGHBOURS(LL,NNEIGH,NEIGH)
C#########################################################
C     This routine checks if two blocks share boundary faces.
C     If the check result is positive the neighbouring 
C     relation is represented in a list
C
C=============================================================

        IMPLICIT NONE
C
#include "param3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
#include "bound3d.inb"
C
        INTEGER NEIGH(NXBK),NNEIGH
        INTEGER LL,IJ1L,IJ2L,IJ3L,IJ4L,NIML,NJML,NKML
        INTEGER LR,IJ1R,IJ2R,IJ3R,IJ4R,NIMR,NJMR,NKMR
        REAL*8 XYZL(12),XYZR(12),XYZCOM(12),XYZFC(3),AR
C NEW
        INTEGER FL,FR
C END NEW
C
      NNEIGH=0

      AR=-1.0
      DO LR=LL+1,NBLKSAL
        DO FL=1,6
          CALL GETFACE(XYZL,LL,FL)
          DO FR=1,6
            CALL GETFACE(XYZR,LR,FR)
            AR=-1.0
            CALL MATCHFACES(XYZL,XYZR,AR,XYZFC)
            IF (AR.GT.0.0D0) THEN
              NNEIGH=NNEIGH+1
              NEIGH(NNEIGH)=LR
            ENDIF
          END DO
        END DO
      END DO

      RETURN 
      END
C
C
C#########################################################
      SUBROUTINE GETFACE(XYZF,B,F)
C#########################################################
C     This routine returns the coordinates of the
C     edge corners of the current block
C
C=============================================================

      IMPLICIT NONE
C
#include "param3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
#include "bound3d.inb"
C
      INTEGER B,F
      INTEGER IJ1F,IJ2F,IJ3F,IJ4F,NIMF,NJMF,NKMF
      REAL*8 XYZF(12)
C
      NIMF=NIBK(B)-1
      NJMF=NJBK(B)-1
      NKMF=NKBK(B)-1
C
      SELECT CASE (F)
C
C.....CHECK WEST FACE
C
      CASE (1)
        IJ1F=LKBK(NKMF+KBK(B))+LIBK(1   +IBK(B))+1   
        IJ2F=LKBK(NKMF+KBK(B))+LIBK(1   +IBK(B))+NJMF 
        IJ3F=LKBK(1   +KBK(B))+LIBK(1   +IBK(B))+NJMF   
        IJ4F=LKBK(1   +KBK(B))+LIBK(1   +IBK(B))+1   
        XYZF(1:3)  =[XGL(IJ1F),YGL(IJ1F),ZGL(IJ1F)]
        XYZF(4:6)  =[XGL(IJ2F),YGL(IJ2F),ZGL(IJ2F)]
        XYZF(7:9)  =[XGL(IJ3F),YGL(IJ3F),ZGL(IJ3F)]
        XYZF(10:12)=[XGL(IJ4F),YGL(IJ4F),ZGL(IJ4F)]
C
C.....CHECK SOUTH FACE
C
      CASE (2)
        IJ1F=LKBK(NKMF+KBK(B))+LIBK(1   +IBK(B))+1
        IJ2F=LKBK(NKMF+KBK(B))+LIBK(NIMF+IBK(B))+1
        IJ3F=LKBK(1   +KBK(B))+LIBK(NIMF+IBK(B))+1
        IJ4F=LKBK(1   +KBK(B))+LIBK(1   +IBK(B))+1
        XYZF(1:3)  =[XGL(IJ1F),YGL(IJ1F),ZGL(IJ1F)]
        XYZF(4:6)  =[XGL(IJ2F),YGL(IJ2F),ZGL(IJ2F)]
        XYZF(7:9)  =[XGL(IJ3F),YGL(IJ3F),ZGL(IJ3F)]
        XYZF(10:12)=[XGL(IJ4F),YGL(IJ4F),ZGL(IJ4F)]
C
C.....CHECK BOTTOM FACE
C
      CASE (3)
        IJ1F=LKBK(NKMF+KBK(B))+LIBK(   1+IBK(B))+NJMF
        IJ2F=LKBK(NKMF+KBK(B))+LIBK(NIMF+IBK(B))+NJMF
        IJ3F=LKBK(NKMF+KBK(B))+LIBK(NIMF+IBK(B))+1   
        IJ4F=LKBK(NKMF+KBK(B))+LIBK(   1+IBK(B))+1   
        XYZF(1:3)  =[XGL(IJ1F),YGL(IJ1F),ZGL(IJ1F)]
        XYZF(4:6)  =[XGL(IJ2F),YGL(IJ2F),ZGL(IJ2F)]
        XYZF(7:9)  =[XGL(IJ3F),YGL(IJ3F),ZGL(IJ3F)]
        XYZF(10:12)=[XGL(IJ4F),YGL(IJ4F),ZGL(IJ4F)]
C
C.....CHECK TOP FACE
C
      CASE (4)
        IJ1F=LKBK(   1+KBK(B))+LIBK(   1+IBK(B))+NJMF
        IJ2F=LKBK(   1+KBK(B))+LIBK(NIMF+IBK(B))+NJMF
        IJ3F=LKBK(   1+KBK(B))+LIBK(NIMF+IBK(B))+1   
        IJ4F=LKBK(   1+KBK(B))+LIBK(   1+IBK(B))+1   
        XYZF(1:3)  =[XGL(IJ1F),YGL(IJ1F),ZGL(IJ1F)]
        XYZF(4:6)  =[XGL(IJ2F),YGL(IJ2F),ZGL(IJ2F)]
        XYZF(7:9)  =[XGL(IJ3F),YGL(IJ3F),ZGL(IJ3F)]
        XYZF(10:12)=[XGL(IJ4F),YGL(IJ4F),ZGL(IJ4F)]
C
C.....CHECK NORTH FACE
C
      CASE (5)
        IJ1F=LKBK(NKMF+KBK(B))+LIBK(1   +IBK(B))+NJMF
        IJ2F=LKBK(NKMF+KBK(B))+LIBK(NIMF+IBK(B))+NJMF
        IJ3F=LKBK(1   +KBK(B))+LIBK(NIMF+IBK(B))+NJMF
        IJ4F=LKBK(1   +KBK(B))+LIBK(1   +IBK(B))+NJMF
        XYZF(1:3)  =[XGL(IJ1F),YGL(IJ1F),ZGL(IJ1F)]
        XYZF(4:6)  =[XGL(IJ2F),YGL(IJ2F),ZGL(IJ2F)]
        XYZF(7:9)  =[XGL(IJ3F),YGL(IJ3F),ZGL(IJ3F)]
        XYZF(10:12)=[XGL(IJ4F),YGL(IJ4F),ZGL(IJ4F)]
C
C.....CHECK EAST FACE
C
      CASE (6)
        IJ1F=LKBK(NKMF+KBK(B))+LIBK(NIMF+IBK(B))+1   
        IJ2F=LKBK(NKMF+KBK(B))+LIBK(NIMF+IBK(B))+NJMF 
        IJ3F=LKBK(1   +KBK(B))+LIBK(NIMF+IBK(B))+NJMF   
        IJ4F=LKBK(1   +KBK(B))+LIBK(NIMF+IBK(B))+1   
        XYZF(1:3)  =[XGL(IJ1F),YGL(IJ1F),ZGL(IJ1F)]
        XYZF(4:6)  =[XGL(IJ2F),YGL(IJ2F),ZGL(IJ2F)]
        XYZF(7:9)  =[XGL(IJ3F),YGL(IJ3F),ZGL(IJ3F)]
        XYZF(10:12)=[XGL(IJ4F),YGL(IJ4F),ZGL(IJ4F)]
      CASE DEFAULT
        PRINT *, "ERROR, FACE INDEX",F,"MUST BE IN {1,..,6}"
        STOP
      END SELECT
C
      RETURN
      END
C
C
C
C#########################################################
      SUBROUTINE BLOCKPART
C#########################################################
C     This routine partitions the block data for 
C     multiprocessor use.
C
C=============================================================
C
      implicit none

#include "param3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
C
      integer L,P,I,K
C
C.....READ NUMBER OF PROCS
C
      READ(5,*) NPROC
C
C.....DISTRIBUTE BLOCKS EQUALLY ON PROCESSORS
C
      NBLKSAL=NBLKS
      IBPR(1)=0
      DO P=1,NPROC
        IF (P.NE.1) IBPR(P)=IBPR(P-1)+NBPR(P-1)
        IF (MOD(NBLKS,NPROC).GT.P-1) THEN
          NBPR(P)=NBLKS/NPROC+1
        ELSE
          NBPR(P)=NBLKS/NPROC
        END IF
      END DO
C
C
      NIBKAL=0; NJBKAL=0; NKBKAL=0; NIJKBKAL=0
      NINLBKAL=0 ;NOUTBKAL=0 ;NSYMBKAL=0 ; NWALBKAL=0 ;NOCBKAL=0
      NMTMBKAL=0 ;NFSGBKAL=0 
C
      NIPR=0;NJPR=0;NKPR=0;NIJKPR=0;
      NINLPR=0;NOUTPR=0;NSYMPR=0;NWALPR=0;
      NOCPR=0;NMTMPR=0;NFSGPR=0
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE PARTITION
C#########################################################
C     This routine partitions the block data for 
C     multiprocessor use.
C
C=============================================================
C
      implicit none

#include "param3d.inb"
#include "indexb3d.inb"
#include "bound3d.inb"
C
      integer L,P,I,K

C NEW
C
C.....FILL UP IMT ARRAY FOR BLOCK WITHOUT REFINEMENT
C
      DO L=2,NBLKS
        IMTBK(L)=IMTBK(L-1)+NMTOBK(L-1)
      END DO
C END NEW
      DO P=1,NPROC
C
        IPR(P)=IBK(IBPR(P)+1)
        KPR(P)=KBK(IBPR(P)+1)
        IFPR(P)=IFBK(IBPR(P)+1)
        IOTPR(P)=IOTBK(IBPR(P)+1)
        IMTPR(P)=IMTBK(IBPR(P)+1)
        NOTOPR(P)=SUM(NOTOBK(IBPR(P)+1:IBPR(P)+NBPR(P)))
        NMTOPR(P)=SUM(NMTOBK(IBPR(P)+1:IBPR(P)+NBPR(P)))
C
        DO L=IBPR(P)+1,IBPR(P)+NBPR(P)
          IF(L.EQ.IBPR(P)+1) IJKPR(P)=IJKBK(L)
C
C.....SUM ARRAY SIZES PROCESSORWISE
C
          NIPR(P)=NIPR(P)+NIBK(L)
          NJPR(P)=NJPR(P)+NJBK(L)
          NKPR(P)=NKPR(P)+NKBK(L)
          NIJKPR(P)=NIJKPR(P)+NIJKBK(L)
C
          NINLPR(P)=NINLPR(P)+NINLBK(L)
          NOUTPR(P)=NOUTPR(P)+NOUTBK(L)
          NSYMPR(P)=NSYMPR(P)+NSYMBK(L)
          NWALPR(P)=NWALPR(P)+NWALBK(L)
          NOCPR(P) =NOCPR(P)+NMTOBK(L)+NOTOBK(L)
          NMTMPR(P)=NMTMPR(P)+NMTMBK(L)
          NFSGPR(P)=NFSGPR(P)+NFSGBK(L)
C
          IFBK(L)=IFBK(L)-IFPR(P)
        END DO
C
C.....REBUILD INDICES
C
        DO L=IBPR(P)+1,IBPR(P)+NBPR(P)
          DO K=1,NKBK(L)
            LKBK(K+KBK(L))=LKBK(K+KBK(L))-IJKPR(P)
          END DO
          IF(L.EQ.IBPR(P)+1) THEN
            IBK(L)=0
            JBK(L)=0
            KBK(L)=0
            IJKBK(L)=0
          ELSE
            IBK(L)=IBK(L-1)+NIBK(L-1)
            JBK(L)=JBK(L-1)+NJBK(L-1)
            KBK(L)=KBK(L-1)+NKBK(L-1)
            IJKBK(L)=IJKBK(L-1)+NIJKBK(L-1)
          END IF
C
C
C.....GLOBAL INDEXES TO BCs ARRAYS
C
          IF (L.EQ.IBPR(P)+1) THEN
            IIBK(L)=0
            IOBK(L)=0
            IWBK(L)=0
            IWABK(L)=NWALIBK(L)
            ISBK(L)=0
            IOCBK(L)=0
            IMBK(L)=0
          ELSE
            IIBK(L)=IIBK(L-1)+NINLBK(L-1)
            IOBK(L)=IOBK(L-1)+NOUTBK(L-1)
            IWBK(L)=IWBK(L-1)+NWALBK(L-1)
            IWABK(L)=IWBK(L)+NWALIBK(L)
            ISBK(L)=ISBK(L-1)+NSYMBK(L-1)
            IOCBK(L)=IOCBK(L-1)+NOCBK(L-1)
            IMBK(L)=IMBK(L-1)+NMTMBK(L-1)
          ENDIF
        END DO
C
C.....APPEND THESE VALUES TO THE ARRAYS OF X,Y,Z
C
        DO I=IFPR(P)+1,IFPR(P)+NFSGPR(P)
          IJF1(I)=IJF1(I)+NIJKPR(P)-IFPR(P)*4
          IJF2(I)=IJF2(I)+NIJKPR(P)-IFPR(P)*4
          IJF3(I)=IJF3(I)+NIJKPR(P)-IFPR(P)*4
          IJF4(I)=IJF4(I)+NIJKPR(P)-IFPR(P)*4
        END DO
C
C.....CORRECT LOCAL INDECES FOR OTO AND MTO BOUNDARIES
C
        DO I=IOTPR(P)+1,IOTPR(P)+NOTOPR(P)
           IJOC1BK(I)=IJOC1BK(I)-IJKPR(P)
           IJOC2BK(I)=IJOC2BK(I)-IJKPR(P)
           IJOC3BK(I)=IJOC3BK(I)-IJKPR(P)
           IJOC4BK(I)=IJOC4BK(I)-IJKPR(P)
        END DO

        DO I=IMTPR(P)+1,IMTPR(P)+NMTOPR(P)
           IJOC1BK(I)=IJOC1BK(I)-IJKPR(P)
           IJOC2BK(I)=IJOC2BK(I)-IJKPR(P)
           IJOC3BK(I)=IJOC3BK(I)-IJKPR(P)
           IJOC4BK(I)=IJOC4BK(I)-IJKPR(P)
        END DO
C
        IF(NIPR(P).GE.NIBKAL)     NIBKAL=NIPR(P)
        IF(NJPR(P).GE.NJBKAL)     NJBKAL=NJPR(P)
        IF(NKPR(P).GE.NKBKAL)     NKBKAL=NKPR(P)
        IF(NIJKPR(P).GE.NIJKBKAL) NIJKBKAL=NIJKPR(P)
C
        IF(NINLPR(P).GE.NINLBKAL) NINLBKAL=NINLPR(P)
        IF(NOUTPR(P).GE.NOUTBKAL) NOUTBKAL=NOUTPR(P)
        IF(NSYMPR(P).GE.NSYMBKAL) NSYMBKAL=NSYMPR(P)
        IF(NOCPR(P).GE.NOCBKAL)   NOCBKAL=NOCPR(P)
        IF(NWALPR(P).GE.NWALBKAL) NWALBKAL=NWALPR(P)
        IF(NMTMPR(P).GE.NMTMBKAL) NMTMBKAL=NMTMPR(P)
        IF(NFSGPR(P).GE.NFSGBKAL) NFSGBKAL=NFSGPR(P)
      END DO
C
C
      RETURN
      END
C
C
C END NEW
C#########################################################
C NEW
C     SUBROUTINE MASTEROUT(IU)
      SUBROUTINE MASTEROUT(IU,P)
C END NEW
C#########################################################
C     This routine writes out the Master Block Structured
C     grid file.
C
C     This file contains the global index arrays.
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
C NEW
#include "bound3d.inb"
C END NEW
      integer I,IU
C NEW
      integer P,IST,KST,IBST,IMST,IFST,IOTST,IMTST,NOTABK,NMTABK
      CHARACTER*13 FILGRD
      CHARACTER*7  NAME
C NEW
C
C.....PREPARE PROCESSOR INDICES
C
      IST=IPR(P)
      KST=KPR(P)
      IBST=IBPR(P)
      IFST=IFPR(P)
C
      NBLKS=NBPR(P)
      NIBKAL=NIPR(P)
      NJBKAL=NJPR(P)
      NIJKBKAL=NIJKPR(P)
      NKBKAL=NKPR(P)
C
      NINLBKAL=NINLPR(P)
      NOUTBKAL=NOUTPR(P)
      NSYMBKAL=NSYMPR(P)
      NWALBKAL=NWALPR(P)
      NOCBKAL =NOCPR(P)
      NMTMBKAL=NMTMPR(P)
      NFSGBKAL=NFSGPR(P)
C
      NIABK= MAX(NINLBKAL,1)
      NOABK= MAX(NOUTBKAL,1)
      NSABK= MAX(NSYMBKAL,1)
      NWABK= MAX(NWALBKAL,1)
      NOCABK=MAX(NOCBKAL ,1)
      NMABK= MAX(NMTMBKAL,1)
      NFABK= MAX(NFSGBKAL,1)
C
      IOTST=IOTBK(IBST+1)
      IMTST=IMTBK(IBST+1)
      NOTABK=SUM(NOTOBK(IBST+1:IBST+NBLKS))
      NMTABK=SUM(NMTOBK(IBST+1:IBST+NBLKS))
      IF (NMTABK.EQ.0.AND.NOTABK.EQ.0) NOTABK=1
C
C
C END NEW
C
C
C.....WRITE BLOCK-GRID DATA TO FILE
C
      WRITE(IU) NBLKS,NIBKAL,NJBKAL,NKBKAL,NIJKBKAL,
C     WRITE(IU,*) NBLKS,NIBKAL,NJBKAL,NKBKAL,NIJKBKAL,
     *          NINLBKAL,NOUTBKAL,NSYMBKAL,NWALBKAL,
     *          NOCBKAL,NIABK,NOABK,NSABK,NWABK,NOCABK,
C NEW
     *          NFSGBKAL,NFABK
C END NEW
C
      WRITE(IU) (LIBK(I+IST) ,I=1,NIBKAL),( LKBK(I+KST),I=1,NKBKAL),
C     WRITE(IU,*) (LIBK(I+IST) ,I=1,NIBKAL),( LKBK(I+KST),I=1,NKBKAL),
     *          (NIBK(I+IBST) ,I=1,NBLKS) ,( NJBK(I+IBST),I=1,NBLKS),
     *          (NKBK(I+IBST) ,I=1,NBLKS) ,(  IBK(I+IBST),I=1,NBLKS),
     *          ( JBK(I+IBST) ,I=1,NBLKS) ,(  KBK(I+IBST),I=1,NBLKS),
     *          (IJKBK(I+IBST),I=1,NBLKS),(NIJKBK(I+IBST),I=1,NBLKS),
C NEW
     *          IJKPR(P)
C END NEW
C
      WRITE(IU) (NINLBK(I+IBST),I=1,NBLKS),(IIBK(I+IBST),I=1,NBLKS),
C     WRITE(IU,*) (NINLBK(I+IBST),I=1,NBLKS),(IIBK(I+IBST),I=1,NBLKS),
     *          (NOUTBK(I+IBST),I=1,NBLKS),(IOBK(I+IBST),I=1,NBLKS),
     *          (NWALBK(I+IBST),I=1,NBLKS),(IWBK(I+IBST),I=1,NBLKS),
     *          (NSYMBK(I+IBST),I=1,NBLKS),(ISBK(I+IBST),I=1,NBLKS),
     *          (NWALABK(I+IBST),I=1,NBLKS),(IWABK(I+IBST),I=1,NBLKS),
     *          (NWALIBK(I+IBST),I=1,NBLKS),
C NEW
     *          (NMTMBK(I+IBST),I=1,NBLKS),(IMBK(I+IBST),I=1,NBLKS),
     *          (NFSGBK(I+IBST),I=1,NBLKS),(IFBK(I+IBST),I=1,NBLKS)
C END NEW
C
C NEW
C     WRITE(IU) (IJLPBK(I) ,I=1,NOCABK),(IJLBBK(I) ,I=1,NOCABK),
C    *          (IJRPBK(I) ,I=1,NOCABK),(IJRBBK(I) ,I=1,NOCABK),
C    *          (IJOC1BK(I),I=1,NOCABK),(IJOC2BK(I),I=1,NOCABK),
C    *          (IJOC3BK(I),I=1,NOCABK),(IJOC4BK(I),I=1,NOCABK),
C    *          (FOCBK(I)  ,I=1,NOCABK),(ITAGOCBK(I),I=1,NOCABK),
C    *          (IBLKOCBK(I),I=1,NOCABK)
      WRITE(IU) (IJLPBK(I+IMTST) ,I=1,NMTABK),
C     WRITE(IU,*) (IJLPBK(I+IMTST) ,I=1,NMTABK),
     *          (IJLPBK(I+IOTST) ,I=1,NOTABK),
     *          (IJLBBK(I+IMTST) ,I=1,NMTABK),
     *          (IJLBBK(I+IOTST) ,I=1,NOTABK),
     *          (IJRPBK(I+IMTST) ,I=1,NMTABK),
     *          (IJRPBK(I+IOTST) ,I=1,NOTABK),
     *          (IJRBBK(I+IMTST) ,I=1,NMTABK),
     *          (IJRBBK(I+IOTST) ,I=1,NOTABK),
     *          (IJOC1BK(I+IMTST),I=1,NMTABK),
     *          (IJOC1BK(I+IOTST),I=1,NOTABK),
     *          (IJOC2BK(I+IMTST),I=1,NMTABK),
     *          (IJOC2BK(I+IOTST),I=1,NOTABK),
     *          (IJOC3BK(I+IMTST),I=1,NMTABK),
     *          (IJOC3BK(I+IOTST),I=1,NOTABK),
     *          (IJOC4BK(I+IMTST),I=1,NMTABK),
     *          (IJOC4BK(I+IOTST),I=1,NOTABK),
     *          (FOCBK(I+IMTST)  ,I=1,NMTABK),
     *          (FOCBK(I+IOTST)  ,I=1,NOTABK),
     *          (ITAGOCBK(I+IMTST),I=1,NMTABK),
     *          (ITAGOCBK(I+IOTST),I=1,NOTABK),
     *          (IBLKOCBK(I+IMTST),I=1,NMTABK),
     *          (IBLKOCBK(I+IOTST),I=1,NOTABK)
C END NEW
C
C NEW
      WRITE(IU) (IJFL(I+IFST),I=1,NFABK),(IJFR(I+IFST),I=1,NFABK),
C     WRITE(IU,*) (IJFL(I+IFST),I=1,NFABK),(IJFR(I+IFST),I=1,NFABK),
C    *          (IJF1(I+IFST),I=1,NFABK),(IJF2(I+IFST),I=1,NFABK),
C    *          (IJF3(I+IFST),I=1,NFABK),(IJF4(I+IFST),I=1,NFABK),
C    *          (XF(I+IFST*4),I=1,NFABK*4),(YF(I+IFST*4),I=1,NFABK*4),
C    *          (ZF(I+IFST*4),I=1,NFABK*4),
     *          (FFSGBK(I+IFST),I=1,NFABK),
     *          (XFR(I+IFST),I=1,NFABK),(YFR(I+IFST),I=1,NFABK),
     *          (ZFR(I+IFST),I=1,NFABK),
     *          (XFC(I+IFST),I=1,NFABK),(YFC(I+IFST),I=1,NFABK),
     *          (ZFC(I+IFST),I=1,NFABK)
C END NEW
C
C
C NEW
C
C.....WRITE BLOCK NAMES TO PROCESSOR FILE
C
      REWIND 5
      READ(5,*) 
      DO I=1,IBST
        READ(5,*) 
      END DO
      DO I=1+IBST,IBST+NBPR(P)
        READ(5,'(A13)') FILGRD
        WRITE(9,'(A13)') FILGRD
      END DO
C END NEW
      RETURN 
C
      END
C
C
C#########################################################
      SUBROUTINE PARAMOUT
C#########################################################
C     This routine writes out the 'param3d.inc' file
C     with appropiate dimensions for the flow solver
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
C
C.....ALLOW ONE EXTRA ELEMENT FOR DIMENSIONS OF BC. ARRAYS      
C
      NIABK=NIABK+1
      NOABK=NOABK+1
      NSABK=NSABK+1
      NWABK=NWABK+1
      NOCABK=NOCABK+1
C
C.....OPEN FILE     
C
      OPEN(UNIT=9,FILE='param3d.inc')
      REWIND 9
C
      WRITE(9,98)'','integer NBK,NXA,NYA,NZA,NXYZA'
      WRITE(9,98)'','integer NIA,NOA,NSA,NWA,NOCA,NFI'
      WRITE(9,98)'','integer NMA,NFA'
      WRITE(9,99) ' PARAMETER (NBK=  ',NBLKS,    ',NXA= ',NIBKAL,','
      WRITE(9,99) '*           NYA=  ',NJBKAL,   ',NZA= ',NKBKAL,','
      WRITE(9,99) '*           NXYZA=',NIJKBKAL, ',NIA= ',NIABK, ','
      WRITE(9,99) '*           NOA=  ',NOABK,    ',NSA= ',NSABK, ','
      WRITE(9,99) '*           NWA=  ',NWABK,    ',NOCA=',NOCABK,','
C NEW
      WRITE(9,99) '*           NMA=  ',NMABK,    ',NFA=',NFABK,','
C END NEW
      WRITE(9,97) '*           NFI=9)'
 99   FORMAT(A23,I9,A7,I9,A1)
 97   FORMAT(A23)
 98   FORMAT(A9,A)
C
      CLOSE(UNIT=9)
C
C
      RETURN 
      END
C
C
C########################################################
      SUBROUTINE CALCFACE(IJK1,IJK2,IJK3,IJK4,
     *                    XXXC,YYYC,ZZZC,XXCR,YYCR,ZZCR)
C########################################################
C     This routine calculates the surface vector and
C     centre of cell IJK.
C========================================================
      implicit none

#include "param3d.inb"
#include "gridb3d.inb"
      integer IJK1,IJK2,IJK3,IJK4
      real*8 DX12,DY12,DZ12,DX13,DY13,DZ13
      real*8 DX14,DY14,DZ14,XR23,YR23,ZR23
      real*8 XR34,YR34,ZR34,S23,S34
      real*8 XXXC,YYYC,ZZZC,XXCR,YYCR,ZZCR
      
C
C.....Vectors to vertices 
      DX12=XGL(IJK2)-XGL(IJK1)
      DY12=YGL(IJK2)-YGL(IJK1)
      DZ12=ZGL(IJK2)-ZGL(IJK1)
C
      DX13=XGL(IJK3)-XGL(IJK1)
      DY13=YGL(IJK3)-YGL(IJK1)
      DZ13=ZGL(IJK3)-ZGL(IJK1)
C
      DX14=XGL(IJK4)-XGL(IJK1)
      DY14=YGL(IJK4)-YGL(IJK1)
      DZ14=ZGL(IJK4)-ZGL(IJK1)
C
C.....Cross Products for triangle surface vectors
      XR23=DY12*DZ13-DZ12*DY13
      YR23=DZ12*DX13-DX12*DZ13
      ZR23=DX12*DY13-DY12*DX13
C
      XR34=DY13*DZ14-DZ13*DY14
      YR34=DZ13*DX14-DX13*DZ14
      ZR34=DX13*DY14-DY13*DX14
C
C.....Face surface vectors
      XXCR=0.5*(XR23+XR34)
      YYCR=0.5*(YR23+YR34)
      ZZCR=0.5*(ZR23+ZR34)
C
C.....Baricenters of triangles
      DX12=(XGL(IJK1)+XGL(IJK2)+XGL(IJK3))/3
      DY12=(YGL(IJK1)+YGL(IJK2)+YGL(IJK3))/3
      DZ12=(ZGL(IJK1)+ZGL(IJK2)+ZGL(IJK3))/3
C
      DX14=(XGL(IJK1)+XGL(IJK3)+XGL(IJK4))/3
      DY14=(YGL(IJK1)+YGL(IJK3)+YGL(IJK4))/3
      DZ14=(ZGL(IJK1)+ZGL(IJK3)+ZGL(IJK4))/3
C
C.....Area of triangles
      S23=SQRT(XR23**2+YR23**2+ZR23**2)
      S34=SQRT(XR34**2+YR34**2+ZR34**2)
C
C.....Baricenter of face
      XXXC=(DX12*S23+DX14*S34)/(S23+S34+1.E-20)
      YYYC=(DY12*S23+DY14*S34)/(S23+S34+1.E-20)
      ZZZC=(DZ12*S23+DZ14*S34)/(S23+S34+1.E-20)
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE CALCFACEM(IJK1,IJK2,IJK3,IJK4,
     *                     IJP1,IJP2,DL1,DL2)
C########################################################
C     This routine calculates the surface vector and
C     centre of cell IJK.
C========================================================
      implicit none

#include "param3d.inb"
#include "gridb3d.inb"
      integer IJK1,IJK2,IJK3,IJK4,IJP1,IJP2
      real*8 DX12,DY12,DZ12,DX13,DY13,DZ13
      real*8 DX14,DY14,DZ14,XR23,YR23,ZR23
      real*8 XR34,YR34,ZR34,S23,S34
      real*8 DL1,DL2,D,C,E,T
      real*8 XXCR,YYCR,ZZCR,XXXC,YYYC,ZZZC 
C
C.....Vectors to vertices 
      DX12=XGL(IJK2)-XGL(IJK1)
      DY12=YGL(IJK2)-YGL(IJK1)
      DZ12=ZGL(IJK2)-ZGL(IJK1)
C
      DX13=XGL(IJK3)-XGL(IJK1)
      DY13=YGL(IJK3)-YGL(IJK1)
      DZ13=ZGL(IJK3)-ZGL(IJK1)
C
      DX14=XGL(IJK4)-XGL(IJK1)
      DY14=YGL(IJK4)-YGL(IJK1)
      DZ14=ZGL(IJK4)-ZGL(IJK1)
C
C.....Cross Products for triangle surface vectors
      XR23=DY12*DZ13-DZ12*DY13
      YR23=DZ12*DX13-DX12*DZ13
      ZR23=DX12*DY13-DY12*DX13
C
      XR34=DY13*DZ14-DZ13*DY14
      YR34=DZ13*DX14-DX13*DZ14
      ZR34=DX13*DY14-DY13*DX14
C
C.....Face surface vectors
      XXCR=0.5*(XR23+XR34)
      YYCR=0.5*(YR23+YR34)
      ZZCR=0.5*(ZR23+ZR34)

      D=XGL(IJK1)*XXCR+YGL(IJK1)*YYCR+ZGL(IJK1)*ZZCR
      C=XXCR*XCGL(IJP1)+YYCR*YCGL(IJP1)+ZZCR*ZCGL(IJP1)
      E=XXCR*(XCGL(IJP2)-XCGL(IJP1))+YYCR*(YCGL(IJP2)-YCGL(IJP1))
     &     +ZZCR*(ZCGL(IJP2)-ZCGL(IJP1))
      T=(D-C)/(E+1.E-20)

      XXXC=XCGL(IJP1)+T*(XCGL(IJP2)-XCGL(IJP1))
      YYYC=YCGL(IJP1)+T*(YCGL(IJP2)-YCGL(IJP1))
      ZZZC=ZCGL(IJP1)+T*(ZCGL(IJP2)-ZCGL(IJP1))

      DL1=SQRT((XXXC-XCGL(IJP1))**2+(YYYC-YCGL(IJP1))**2+
     &     (ZZZC-ZCGL(IJP1))**2)
      DL2=SQRT((XCGL(IJP1)-XCGL(IJP2))**2+(YCGL(IJP1)-YCGL(IJP2))**2+
     &     (ZCGL(IJP1)-ZCGL(IJP2))**2)
C
      RETURN
      END
C
C
C#########################################################
      SUBROUTINE TECPLOTOUT
C#########################################################
C     THIS ROUTINE CREATES AN TECPLOT OUTPUT.
C     PRUPOSE: CHECK WHETHER APPLIED MATCHING PROCEDURE
C     IS WORKING CORRECTLY!
C
C=============================================================
      implicit none

#include "param3d.inb"
#include "logico3d.inb"
#include "grid3d.inb"
#include "indexb3d.inb"
#include "gridb3d.inb"
      integer L,st,i,j,k,ijk,ijkn,KSTM,ISTM
      real*8, dimension(:), allocatable :: xhh,yhh,zhh,matchhh 
      
      st=0
      DO L=1,NBLKS
         NI=NIBK(L)
         NJ=NJBK(L)
         NK=NKBK(L)
         NIJK=NIJKBK(L)
         KSTM=KBK(L)
         ISTM=IBK(L)
         
         allocate(xhh(nijk),yhh(nijk),zhh(nijk), stat=st)
         allocate(matchhh(nijk), stat=st)
         if (st /= 0) then    
            stop "allocation-Fehler GRID ALLOC xhh,etc!"
         end if
         
         do j=1,nj
            do i=1,ni
               do k=1,nk
                  ijk=LKBK(k+KSTM)+LIBK(i+ISTM)+j
                  ijkn=(k-1)*((ni)*(nj))+(j-1)*(ni)+i
                  xhh(ijkn)=xgl(ijk)
                  yhh(ijkn)=ygl(ijk)
                  zhh(ijkn)=zgl(ijk)
                  matchhh(ijkn)=TECMATCH(ijk)
               enddo
            enddo
         enddo 
      
         if(L.eq.1)then
            open (31, file='w_block.plt', status='replace', iostat=st)
            write (31,fmt ='(A)',iostat=st)'TITLE = "CHECK PREPRO"'
            write (31,fmt ='(A)',iostat=st)'VARIABLES= "X" "Y" "Z" "M"'
            write (31,fmt ='(A,X,A,I4,A,I4,A,I4,A)',iostat=st) 
     &           'ZONE T="Block 1",',
     &           'I=',NI,',J=',NJ,',K=',NK,
     &           ',DATAPACKING=POINT'       
            do i=1,nijk
             write (31,fmt ='(f12.6,A,f12.6,A,f12.6,A,f12.6)',iostat=st) 
     &              xhh(i),'  ',yhh(i),'  ',zhh(i),'  ',matchhh(i)
            enddo
         elseif(L.eq.NBLKS)then
            write (31,fmt ='(A,I3,A,X,A,I4,A,I4,A,I4,A)',iostat=st) 
     &           'ZONE T="Block', L,'",',
     &           'I=',NI,',J=',NJ,',K=',NK,
     &           ',DATAPACKING=POINT'       
            do i=1,nijk
             write (31,fmt ='(f12.6,A,f12.6,A,f12.6,A,f12.6)',iostat=st) 
     &              xhh(i),'  ',yhh(i),'  ',zhh(i),'  ',matchhh(i)
            enddo
            CLOSE(31)
         else
            write (31,fmt ='(A,I3,A,X,A,I4,A,I4,A,I4,A)',iostat=st) 
     &           'ZONE T="Block', L,'",',
     &           'I=',NI,',J=',NJ,',K=',NK,
     &           ',DATAPACKING=POINT'   
            do i=1,nijk
             write (31,fmt ='(f12.6,A,f12.6,A,f12.6,A,f12.6)',iostat=st) 
     &                 xhh(i),'  ',yhh(i),'  ',zhh(i),'  ',matchhh(i)
               enddo
               
            endif
            deallocate(xhh,yhh,zhh, stat=st)
            deallocate(matchhh, stat=st)
      ENDDO



      RETURN
      END
C
